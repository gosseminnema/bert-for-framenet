import re


if __name__ == '__main__':

    with open("data/fn1.7/fn1.7.fulltext.train.syntaxnet.txt", encoding="utf-8") as f:
        prev_line = None
        prev_line_bi_roles = []
        for line in f:
            b_roles = re.findall(r"B:(\w+:\w+)", line)
            bi_roles = re.findall(r"[BI]:(\w+:\w+)", line)
            # b_roles = [m.group(1) for m in b_matches]
            # bi_roles = [m.group(1) for m in bi_matches]

            if any(role in b_roles for role in prev_line_bi_roles):
                print(prev_line)
                print(line)
                print()

            prev_line = line
            prev_line_bi_roles = bi_roles
