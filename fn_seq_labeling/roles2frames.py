import collections
import json
from typing import List, Dict, Tuple, Optional, Set

import pandas as pd
from nltk.corpus import framenet as fn
from sklearn.metrics import accuracy_score


class FrameFinder:

    def __init__(self,
                 role_list: List[str],
                 restrict_frames: Optional[Set[str]] = None,
                 roles_to_possible_frames: Dict[str, List[str]] = None,
                 top_n: int = 1
                 ):
        self.role_list: List[str] = role_list
        self._roles_to_possible_frames: Dict[str, List[str]]
        if roles_to_possible_frames is not None:
            self._roles_to_possible_frames = roles_to_possible_frames
        else:
            self._roles_to_possible_frames = {}
        self._frames2freq: Dict[str, int] = {}
        self._restrict_frames: Optional[Set[str]] = restrict_frames
        self._top_n: int = top_n

    def find_frames(self) -> List[List[str]]:

        # first pass: find possible frame mappings given the roles
        self._update_possible_frames()

        # calculate distribution of frames
        self._frames2freq = self._get_frame_distribution()

        # second pass: find most likely role for each frame
        frames = []
        for role in self.role_list:
            frames.append(self._find_most_likely_frame(role))
        return frames

    def _update_possible_frames(self):
        for role in self.role_list:
            if role in self._roles_to_possible_frames:
                continue
            fe_entries = fn.fes(role)
            possible_frames = [fe["frame"].name for fe in fe_entries]
            self._roles_to_possible_frames[role] = possible_frames

    def _find_most_likely_frame(self, target_role: str) -> List[str]:
        possible_frames = self._roles_to_possible_frames[target_role]
        if self._restrict_frames:
            possible_frames = [frame for frame in possible_frames if frame in self._restrict_frames]

        frame_freqs = [(frame, self._frames2freq[frame]) for frame in possible_frames]

        most_likely_frames = [pair[0] for pair in sorted(frame_freqs, key=lambda pair: pair[1], reverse=True)]
        most_likely_frames = most_likely_frames[:self._top_n]
        return most_likely_frames

    def _get_frame_distribution(self):
        frames_to_freq = collections.defaultdict(int)
        for role in self._roles_to_possible_frames:
            for frame in self._roles_to_possible_frames[role]:
                frames_to_freq[frame] += 1
        return frames_to_freq


def load_role_data(json_file):
    with open(json_file, encoding="utf-8") as f:
        dataset = json.load(f)

    for sentence in dataset:
        frame_list = sentence["frame_list"]
        frames, roles = parse_frame_list(frame_list)
        yield roles, frames


def parse_frame_list(frame_list):
    roles = []
    frames = []
    for token_frames in frame_list:
        for annotation in token_frames:
            role_and_frame = parse_frame_annotation(annotation)
            if role_and_frame:
                role, frame = role_and_frame
                roles.append(role)
                frames.append(frame)
    return frames, roles


def parse_frame_annotation(annotation: str) -> Optional[Tuple[str, str]]:
    if annotation.startswith("T:"):
        return None
    bio_tag, frame, role = annotation.split(":")
    return role, frame


def evaluate_role_reconstruction():
    role_data = load_role_data("data/multilabel/fn1.7.fulltext.train.syntaxnet.json")
    prediction_dicts = []

    # use global mapping for roles to possible frames (to save time)
    roles_to_possible_frames = {}

    for sent_idx, sentence in enumerate(role_data):

        if sent_idx % 10 == 0:
            print(sent_idx)

        roles, frames = sentence

        # find likely frames
        frame_finder = FrameFinder(roles,
                                   restrict_frames=set(frames),
                                   roles_to_possible_frames=roles_to_possible_frames,
                                   top_n=10
                                   )
        predicted_frames = frame_finder.find_frames()

        # save the predictions
        for role, preds, gold in zip(roles, predicted_frames, frames):
            prediction_dict = {
                "sent_idx": sent_idx,
                "role": role,
                "gold": gold
            }
            for i, pred in enumerate(preds):
                prediction_dict[f"pred_{i:02}"] = pred
            prediction_dicts.append(prediction_dict)

    df = pd.DataFrame(prediction_dicts)
    df.to_csv("output/role_predictions/train.csv")
    accuracy = accuracy_score(df["gold"], df["pred_00"])
    print(accuracy)


def process_predicted_roles():
    raise NotImplementedError


if __name__ == '__main__':
    evaluate_role_reconstruction()
