import collections
import dataclasses
import os
from typing import List, Tuple, Dict, Optional

from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation


def main():
    data_dir = "data/hartmann_data/Hartmann-etal_2017/yags-compilation-kit/output/"
    sf_annotations = read_frame_element_file(data_dir + "dev-yags.frame.elements", limit=None)
    ml_annotations = make_multilabel_annotations(sf_annotations, data_dir + "testdev-yags.lemma.tags", limit=None)
    with open("data/hartmann_data/dev.txt", "w", encoding="utf-8") as f:
        for annotation in ml_annotations:
            for line in annotation.to_txt():
                f.write(line + os.linesep)
            f.write(os.linesep)


@dataclasses.dataclass
class SemaforFrameAnnotation:
    sent_idx: int
    pred_idxs: List[int]
    frame: str
    pred_lu: str
    pred_word: str
    roles: List[Tuple[str, Tuple[int, int]]]


def read_frame_element_file(
        filename: str,
        limit: Optional[int] = None
        ) -> Dict[int, List[SemaforFrameAnnotation]]:

    annotations = collections.defaultdict(list)
    with open(filename, encoding="utf-8") as f:
        for i, line in enumerate(f):

            if limit is not None and i >= limit:
                break

            columns = line.split("\t")
            num_annotations = int(columns[1])
            frame = columns[3]
            lu = columns[4]
            pred_idx = [int(idx) for idx in columns[5].split("_")]
            pred_word = columns[6]
            sent_idx = int(columns[7])
            if num_annotations == 1:
                role_names = []
                role_idx = []
            else:
                role_names = columns[8::2]  # e.g. ["Means", "Cook", "Produced_food"]
                role_idx_str = columns[9::2]  # e.g. ["0", "2", "4:5"]
                role_idx_split = [idx_str.split(":") for idx_str in role_idx_str]  # e.g. [["0"], ["2"], ["4", "5"]]
                role_idx = [(int(split[0]), int(split[-1])) for split in role_idx_split]  # e.g. [(0, 0), (2, 2), (4,5)]
            roles = list(zip(role_names, role_idx))
            annotation = SemaforFrameAnnotation(sent_idx, pred_idx, frame, lu, pred_word, roles)
            annotations[sent_idx].append(annotation)

    return annotations


def make_multilabel_annotations(semafor_annos: Dict[int, List[SemaforFrameAnnotation]],
                                sentences_file: str,
                                limit: Optional[int]
                                ) -> List[MultiLabelAnnotation]:
    multilabel_annotations = []

    with open(sentences_file, encoding="utf-8") as f:
        for sent_idx, line in enumerate(f):

            if limit is not None and sent_idx >= limit:
                break

            columns = line.split("\t")
            num_tok = int(columns[0])
            tokens = columns[1:num_tok+1]
            pos_tags = columns[1+num_tok:2*num_tok+1]

            frame_list: List[List[str]] = [list() for _ in range(num_tok)]
            lu_list: List[Optional[str]] = [None for _ in range(num_tok)]

            annotations = semafor_annos.get(sent_idx, [])
            if not annotations:
                continue

            for anno in annotations:

                # add LU/frame target
                for idx in anno.pred_idxs:
                    frame_list[idx].append(f"T:{anno.frame}")
                    lu_list[idx] = anno.pred_lu

                # add role labels
                for role in anno.roles:
                    role_label, (start_idx, end_idx) = role
                    for idx in range(start_idx, end_idx + 1):
                        iob = "B" if idx == start_idx else "I"
                        frame_list[idx].append(f"{iob}:{anno.frame}:{role_label}")

            multilabel_annotations.append(MultiLabelAnnotation(tokens, pos_tags, frame_list, lu_list))

    return multilabel_annotations


if __name__ == '__main__':
    main()
