import os

from fn_seq_labeling.multilabel.frame_annotations import write_multilabel_outputs
from fn_seq_labeling.multilabel.sesame2multilabel import read_txt_corpus


def main(exemplar_file):
    rare_frames = get_rare_frame_set()
    annotations_with_rare_frames = []

    with open(exemplar_file, encoding="utf-8") as f:
        for i, annotation in enumerate(read_txt_corpus(f)):

            if i % 1000 == 0:
                print(f"{i}...", end="", flush=True)

            annotation_frames = {f for frames in annotation.frame_list for f in frames if f.startswith("T:")}
            if any(f in annotation_frames for f in rare_frames):
                annotations_with_rare_frames.append(annotation)

    print()
    write_multilabel_outputs(
        basename=os.path.basename(exemplar_file).replace(".txt", ".only_rare"),
        annotations=annotations_with_rare_frames
    )


def get_rare_frame_set():
    rare_frames = set()
    with open("output/freq_analysis/rare_frames.txt", encoding="utf-8") as f:
        for line in f:
            frame = line.strip()
            rare_frames.add(frame)
    return rare_frames


if __name__ == '__main__':
    # main("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.txt")
    # main("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped2.txt")
    # main("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped_frames2.txt")
    # main("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped2.silver_frames.txt")
    # main("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.silver_frames__emb10_0.5.txt")
    main("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.silver_frames__seqlabel_scalarmix.txt")