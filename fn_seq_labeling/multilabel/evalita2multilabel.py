import dataclasses
import json
import os
import sys

from fn_seq_labeling.multilabel import frame_annotations


def main(input_file):

    annotations, raw_text = read_annotations(input_file)

    basename, _ = os.path.splitext(os.path.basename(input_file))
    output_file_ml = f"data/multilabel/evalita_{basename}.sem.txt"
    output_file_json = f"data/multilabel/evalita_{basename}.sem.json"
    output_file_sents = f"data/multilabel/evalita_{basename}.sents.txt"

    with open(output_file_ml, "w", encoding="utf-8") as f_ml:
        for an in annotations:
            for line in an.to_txt():
                f_ml.write(line + os.linesep)
            f_ml.write(os.linesep)

    with open(output_file_json, "w", encoding="utf-8") as f_json:
        json_data = [dataclasses.asdict(anno) for anno in annotations]
        json.dump(json_data, f_json)

    with open(output_file_sents, "w", encoding="utf-8") as f_sents:
        for line in raw_text:
            f_sents.write(line + os.linesep)


def read_annotations(file_in):

    with open(file_in, encoding="utf-8") as f:

        annotations = []
        raw_text = []

        cur_tokens = []
        cur_pos = []
        cur_frame = None
        cur_role = None
        cur_frame_labels = []

        for line in f:
            line = line.strip()
            if not line:
                if not cur_tokens:
                    continue
                for tok_labels in cur_frame_labels:
                    for i in range(len(tok_labels)):
                        old_label = tok_labels[i]
                        assert cur_frame is not None
                        tok_labels[i] = old_label.replace("::FRAME::", f":{cur_frame}:")

                annotations.append(frame_annotations.MultiLabelAnnotation(cur_tokens, cur_pos,
                                                                          cur_frame_labels, [None for _ in cur_tokens]))
                raw_text.append(" ".join(cur_tokens))

                cur_tokens = []
                cur_pos = []
                cur_frame_labels = []
                cur_frame = None
                cur_role = None
                continue

            columns = line.split("\t")

            token = columns[1]
            pos = columns[2]
            frame = columns[3]
            role = columns[4]

            frame_label = []
            if frame != "-":
                frame_label.append(f"T:{frame}@00")
                cur_frame = frame
            if role != "-":
                role_label = f"B::FRAME::{role}@00"
                if cur_role == role:
                    role_label = f"I::FRAME::{role}@00"
                cur_role = role
                frame_label.append(role_label)
            else:
                cur_role = None

            cur_tokens.append(token)
            cur_pos.append(pos)
            cur_frame_labels.append(frame_label)
    return annotations, raw_text


if __name__ == '__main__':
    main(sys.argv[1])
