import collections
from typing import List, Dict

from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation, write_multilabel_outputs
from fn_seq_labeling.multilabel.sesame2multilabel import read_txt_corpus
from fn_seq_labeling.multilabel.remove_duplicates import get_token_str


def main():
    sentences_to_annotations: Dict[str, List[MultiLabelAnnotation]] = collections.defaultdict(list)

    with open("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.txt", encoding="utf-8") as f:
        for anno in read_txt_corpus(f):
            sentences_to_annotations[get_token_str(anno)].append(anno)

    merged_annotations = []
    for i, sentence in enumerate(sentences_to_annotations):

        if i % 1000 == 0:
            print(f"{i}...", end="", flush=True)

        try:
            merged_annotation = merge_annotations(sentences_to_annotations[sentence])
            merged_annotations.append(merged_annotation)
        except ValueError:
            print(f"\nERR: >= 1 LUs found in #{i}, tokens={sentences_to_annotations[sentence][0].tokens}\n")
        except AssertionError:
            print(f"\nERR: Inconsistent tokens found in #{i}, tokens={sentences_to_annotations[sentence][0].tokens}\n")

    print()
    basename = "fn1.7.exemplar.train.syntaxnet.filtered.merged"
    write_multilabel_outputs(basename, merged_annotations)


def merge_annotations(annotations: List[MultiLabelAnnotation]) -> MultiLabelAnnotation:
    assert len(annotations)
    assert all(a.tokens == annotations[0].tokens for a in annotations), str([" ".join(a.tokens) for a in annotations])

    merged = MultiLabelAnnotation(tokens=annotations[0].tokens,
                                  pos=annotations[0].pos,
                                  frame_list=[list() for _ in annotations[0].tokens],
                                  lu_list=[None for _ in annotations[0].tokens]
                                  )

    for a in annotations:
        for i, _ in enumerate(merged.tokens):
            merged.frame_list[i].extend(a.frame_list[i])
            if a.lu_list[i] is not None:
                if merged.lu_list[i] is not None:
                    raise ValueError("Only <=1 LU is allowed per token!\n", a.tokens)
                merged.lu_list[i] = a.lu_list[i]

    return merged


if __name__ == '__main__':
    main()
