import dataclasses
import json
import os
from typing import List, Optional

from fn_seq_labeling import constants as cns


@dataclasses.dataclass
class FrameAnnotation:
    tokens: List[str] = dataclasses.field(default_factory=list)
    pos: List[str] = dataclasses.field(default_factory=list)


@dataclasses.dataclass
class MultiLabelAnnotation(FrameAnnotation):
    frame_list: List[List[str]] = dataclasses.field(default_factory=list)
    lu_list: List[Optional[str]] = dataclasses.field(default_factory=list)

    def to_txt(self):
        for i, tok in enumerate(self.tokens):
            yield f"{tok} {self.pos[i]} {'|'.join(self.frame_list[i]) or '_'} {self.lu_list[i] or '_'}"

    @staticmethod
    def from_txt(sentence_lines):

        tokens = []
        pos = []
        frame_list = []
        lu_list = []
        for line in sentence_lines:

            # ignore any spaces
            if line.startswith(" "):
                continue

            columns = line.split()
            tokens.append(columns[0])
            pos.append(columns[1])

            # read frame list, handle empty lists
            if columns[2] == "_":
                frame_list.append([])
            else:
                frame_list.append(columns[2].split("|"))

            # read lu list, handle nulls
            if columns[3] == "_":
                lu_list.append(None)
            else:
                lu_list.append(columns[3])
        return MultiLabelAnnotation(tokens, pos, frame_list, lu_list)

    def get_label_set(self):
        label_set = set()
        for tok_labels in self.frame_list:
            for label in tok_labels:
                label_set.add(label)
        return label_set


@dataclasses.dataclass
class OpenSesameAnnotation(FrameAnnotation):
    lu: List[Optional[str]] = dataclasses.field(default_factory=list)
    frame: List[Optional[str]] = dataclasses.field(default_factory=list)
    role_name: List[Optional[str]] = dataclasses.field(default_factory=list)
    role_iob: List[str] = dataclasses.field(default_factory=list)


def write_multilabel_outputs(basename: str, annotations: List[MultiLabelAnnotation]):
    # write json output
    multi_label_json = [dataclasses.asdict(anno) for anno in annotations]
    with open(os.path.join(cns.MULTILABEL_FN_CORPUS, basename + ".json"), "w", encoding="utf-8") as f:
        json.dump(multi_label_json, f)
    # write text output
    with open(os.path.join(cns.MULTILABEL_FN_CORPUS, basename + ".txt"), "w", encoding="utf-8") as f:
        for sentence in annotations:
            for line in sentence.to_txt():
                f.write(line + os.linesep)
            f.write(os.linesep)