import os
import re
import sys
from typing import List

import pandas as pd

from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation, write_multilabel_outputs
from fn_seq_labeling.evaluation.eval_utils import split_sentences


def add_silver_frame_annotations(prediction_file: str = None,
                                 prediction_df: pd.DataFrame = None
                                 ) -> List[MultiLabelAnnotation]:
    annotations = []

    if prediction_file:
        prediction_df = pd.read_csv(prediction_file)

    assert prediction_df is not None

    print("Splitting sentences...")
    sentences = split_sentences(prediction_df)

    print("Adding silver annos...")
    for sentence_df in sentences:

        tokens = []
        pos = []
        frames = []
        lus = []

        for i, (idx, row) in enumerate(sentence_df.iterrows()):

            if i % 1_000 == 0:
                print(f"{i}...", end="", flush=True)

            tokens.append(row["token"])
            pos.append("_")
            gold_frames = [t for t in row["gold"].split("|") if t.startswith("T:")]
            pred_frames = [t for t in row["prediction"].split("|") if t.startswith("T:")]
            if gold_frames:
                frame = [gold_frames[0]]
            elif pred_frames:
                frame = [pred_frames[0]]
            else:
                frame = []
            frames.append(frame)
            lus.append(None)

        annotations.append(MultiLabelAnnotation(
            tokens=tokens,
            pos=pos,
            frame_list=frames,
            lu_list=lus
        ))
    print()
    return annotations


def main():

    prediction_file = sys.argv[1]
    basename = os.path.basename(sys.argv[2])
    postfix = sys.argv[3]
    annotations = add_silver_frame_annotations(prediction_file=prediction_file)
    write_multilabel_outputs(basename.replace(".txt", f".silver_frames_{postfix}"), annotations)


if __name__ == '__main__':
    main()

