import collections
import dataclasses
import json
import os
import sys
from typing import Dict, List

from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation, write_multilabel_outputs
from fn_seq_labeling.multilabel.sesame2multilabel import read_txt_corpus
import fn_seq_labeling.constants as cns


def get_token_str(anno: MultiLabelAnnotation):
    return " ".join(anno.tokens).lower()


def remove_duplicates(target_file: str, *comparison_files: str):

    comparison_sentences: Dict[str, List[str]] = collections.defaultdict(list)

    for file in comparison_files:
        with open(file, encoding="utf-8") as f:
            file_sentences = {get_token_str(anno) for anno in read_txt_corpus(f)}
            for sent in file_sentences:
                comparison_sentences[sent].append(file)

    duplicates = collections.defaultdict(list)
    non_duplicates: List[MultiLabelAnnotation] = []

    with open(target_file, encoding="utf-8") as f:
        for i, anno in enumerate(read_txt_corpus(f)):

            if i % 1000 == 0:
                print(f"{i}...", end="", flush=True)

            token_str = get_token_str(anno)
            if token_str in comparison_sentences:
                for file in comparison_sentences[token_str]:
                    duplicates[file].append(token_str)
            else:
                non_duplicates.append(anno)
        print()

    target_basename = os.path.splitext(os.path.basename(target_file))[0]

    write_multilabel_outputs(target_basename + ".filtered", non_duplicates)

    # write duplicate info
    with open(os.path.join(cns.MULTILABEL_FN_CORPUS, target_basename + ".duplicates.json"), "w", encoding="utf-8") as f:
        json.dump(duplicates, f, indent=4)


if __name__ == '__main__':
    remove_duplicates(sys.argv[1], *sys.argv[2:])
