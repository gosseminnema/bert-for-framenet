import itertools
import sys
from typing import List

from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation, write_multilabel_outputs
from fn_seq_labeling.multilabel.sesame2multilabel import read_txt_corpus


def clip(annotation: MultiLabelAnnotation, window_size=2) -> MultiLabelAnnotation:
    first_token = -1
    last_token = -1

    # find first token
    for i, _ in enumerate(annotation.tokens):
        if annotation.frame_list[i]:
            if first_token < 0:
                first_token = i
            last_token = i

    assert first_token <= last_token

    first_token = max(0, first_token - window_size)
    last_token = min(len(annotation.tokens), last_token + window_size)

    clipped = MultiLabelAnnotation(
        tokens=annotation.tokens[first_token:last_token + 1],
        pos=annotation.pos[first_token:last_token + 1],
        frame_list=annotation.frame_list[first_token:last_token + 1],
        lu_list=annotation.lu_list[first_token:last_token + 1]
    )
    return clipped


def clip_frames(annotation: MultiLabelAnnotation, window_size=2) -> List[MultiLabelAnnotation]:
    # extract frame target annotations
    frame_targets = [[f for f in frames if f.startswith("T:")] for frames in annotation.frame_list]
    frame_targets = [tgt_list[0] if tgt_list else None for tgt_list in frame_targets]

    clips = []

    # group together targets for dealing with multiple-token LUs (e.g. "at least")
    for target, group in itertools.groupby(enumerate(frame_targets), lambda pair: pair[1]):

        if not target:
            continue

        target_indices = [i for i, _ in group]
        first_token = max(target_indices[0] - window_size, 0)
        last_token = min(target_indices[-1] + window_size, len(annotation.tokens))

        clips.append(MultiLabelAnnotation(
            tokens=annotation.tokens[first_token:last_token + 1],
            pos=annotation.pos[first_token:last_token + 1],
            frame_list=[[t] if t else [] for t in frame_targets[first_token:last_token+1]],
            lu_list=annotation.lu_list[first_token:last_token+1]
        ))

    return clips


def main():
    with open("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.merged.txt", encoding="utf-8") as f:
        clipped_annotations = []
        clipped_frame_annotations = []

        window_size = int(sys.argv[1])

        for i, anno in enumerate(read_txt_corpus(f)):
            if i % 1000 == 0:
                print(f"{i}...", end="", flush=True)

            try:
                clipped_annotations.append(clip(anno, window_size=window_size))
                clipped_frame_annotations.extend(clip_frames(anno, window_size=window_size))
            except AssertionError:
                print(f"Skipping {i}")

    basename_clipped_frames = f"fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped_frames{window_size}"
    write_multilabel_outputs(basename_clipped_frames, clipped_frame_annotations)


if __name__ == '__main__':
    main()
