import dataclasses
import itertools
import json
import os
from itertools import zip_longest
from typing import List, Iterator, Tuple, Optional, Dict

import glob

from fn_seq_labeling import constants as cns
from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation, OpenSesameAnnotation


def read_txt_corpus(corpus_lines) -> Iterator[MultiLabelAnnotation]:
    current_sent = []
    for line in corpus_lines:
        # split on empty lines
        if not line.strip():
            yield MultiLabelAnnotation.from_txt(current_sent)
            current_sent = []
            continue
        current_sent.append(line)
    if current_sent:
        yield MultiLabelAnnotation.from_txt(current_sent)


# def restore_gold_tokens_(predictions: List[MultiLabelAnnotation],
#                          gold_references: Dict[int, MultiLabelAnnotation]
#                          ):
#
#     # for pred, gold in zip(predictions, gold_references):
#     #     assert len(pred.tokens) == len(gold.tokens), f"[GOLD]: {gold.tokens}\n[PRED]: {pred.tokens}"
#     #     for i, gold_tok in enumerate(gold.tokens):
#     #         pred.tokens[i] = gold_tok


def convert_framenet_corpus(corpus: str = "framenet",
                            include_exemplars: bool = False
                            ):
    if corpus == "framenet":
        annotation_dir = cns.OPEN_SESAME_TRAINING_DATA
        annos_and_sentences = [
            (conll, conll + ".sents")
            for conll in glob.glob(os.path.join(annotation_dir, "*.conll"))
        ]
    elif corpus == "open_sesame_output":
        annos_and_sentences = [
            (
                # os.path.join(cns.OPEN_SESAME_OUTPUT_DATA, "sesame-predicted-end-to-end.conll"),
                os.path.join(cns.OPEN_SESAME_OUTPUT_DATA, "sesame-predicted-end-to-end_TEST.conll"),
                # os.path.join(cns.OPEN_SESAME_TRAINING_DATA, "fn1.7.dev.syntaxnet.conll.sents")
                os.path.join(cns.OPEN_SESAME_TRAINING_DATA, "fn1.7.test.syntaxnet.conll.sents")
            )
        ]
    elif corpus == "simpleframeid_output":
        annos_and_sentences = None  # TODO add!
    elif corpus == "tatoeba":
        annos_and_sentences = None  # todo add!
    else:
        raise ValueError("Invalid corpus")

    # if gold_reference:
    #     gold_reference_annos: Optional[Dict[int, MultiLabelAnnotation]] = {
    #         sent_idx: anno
    #         for sent_idx, anno in convert_sesame(gold_reference)
    #     }
    # else:
    #     gold_reference_annos = None

    assert annos_and_sentences is not None

    for conll_file, sent_file in annos_and_sentences:

        print(f"Converting {conll_file}")

        # get basename for output files
        cf_basename = os.path.splitext(os.path.split(conll_file)[-1])[0]

        if "exemplar.train" in cf_basename and not include_exemplars:
            print(f"Skipping exemplar file {cf_basename}")
            continue

        # convert to multilabel format
        sent_to_multilabel_anno: Dict[int, MultiLabelAnnotation] = {
            sent_idx: anno
            for sent_idx, anno in convert_sesame(conll_file)
        }

        # add missing sentences, but only for dev/test splits
        if "train" not in cf_basename:
            with open(sent_file, encoding="utf-8") as f:
                sentences = [line.strip() for line in f]
            multilabel_annos = add_missing_sentences(sent_to_multilabel_anno, sentences)
        else:
            multilabel_annos = [anno for sent_idx, anno in sorted(sent_to_multilabel_anno.items(), key=lambda t: t[0])]

        # # for OpenSESAME predictions: use gold file to restore UNK tokens and unannotated sentences
        # if gold_reference_annos:
        #     restore_gold_tokens_(multilabel_annos, gold_reference_annos)

        multi_label_json = [dataclasses.asdict(anno) for anno in multilabel_annos]

        # find set of labels
        label_set = set()
        for sentence in multilabel_annos:
            label_set.update(sentence.get_label_set())
        # write in alphabetical order
        with open(os.path.join(cns.MULTILABEL_FN_CORPUS, cf_basename + ".vocab.txt"), "w", encoding="utf-8") as f:
            for label in sorted(label_set):
                f.write(label + os.linesep)

        # write json output
        with open(os.path.join(cns.MULTILABEL_FN_CORPUS, cf_basename + ".json"), "w", encoding="utf-8") as f:
            json.dump(multi_label_json, f)

        # write text output
        with open(os.path.join(cns.MULTILABEL_FN_CORPUS, cf_basename + ".txt"), "w", encoding="utf-8") as f:
            for sentence in multilabel_annos:
                for line in sentence.to_txt():
                    f.write(line + os.linesep)
                f.write(os.linesep)


def fix_alignment(pred_tokens, gold_tokens):

    PUNCT = ["``", "`", "'", "\"", "''"]

    fixed_tokens = []
    pred_idx = 0
    gold_idx = 0

    while pred_idx < len(pred_tokens) and gold_idx < len(gold_tokens):

        pred = pred_tokens[pred_idx]
        gold = gold_tokens[gold_idx]

        if pred.lower() == gold.lower():
            pred_idx += 1
            gold_idx += 1
            fixed_tokens.append(gold)

        elif pred in PUNCT and gold in PUNCT:
            pred_idx += 1
            gold_idx += 1
            fixed_tokens.append(gold)

        elif pred == "unk":
            pred_idx += 1
            gold_idx += 1
            fixed_tokens.append(gold)

        elif pred.lower().startswith(gold.lower()):
            raise ValueError("Unalignable sequences!")

        elif gold.lower().startswith(pred.lower()):
            pred_idx += 1
            assert pred_idx < len(pred_tokens)

            if pred_tokens[pred_idx] != "unk":
                pred_tokens[pred_idx] = pred + pred_tokens[pred_idx]

        else:
            raise ValueError("Unalignable sequences!")

    return fixed_tokens


def add_missing_sentences(sent_idx_to_anno: Dict[int, MultiLabelAnnotation],
                          sentences: List[str]
                          ) -> List[MultiLabelAnnotation]:

    annotations = []
    for sent_idx, sentence in enumerate(sentences):
        gold_tokens = sentence.split()
        if sent_idx in sent_idx_to_anno:

            annotation = sent_idx_to_anno[sent_idx]

            if len(annotation.tokens) != len(gold_tokens):
                print(f"WARNING: missing tokens in {sent_idx}", set(gold_tokens).difference(annotation.tokens))
                try:
                    annotation.tokens = fix_alignment(annotation.tokens, gold_tokens)
                except ValueError as e:
                    print(f"Can't fix alignment in sentence {sent_idx}")
                    # raise e

            elif [t.lower() for t in annotation.tokens] != [t.lower() for t in gold_tokens]:
                print(f"WARNING: non-matching tokens in {sent_idx}", set(gold_tokens).difference(annotation.tokens))
                annotation.tokens = gold_tokens

            else:
                annotation.tokens = gold_tokens

            annotations.append(annotation)
        else:
            annotations.append(MultiLabelAnnotation(
                tokens=gold_tokens,
                pos=["_" for _ in gold_tokens],
                frame_list=[[] for _ in gold_tokens],
                lu_list=[None for _ in gold_tokens]
            ))
    return annotations


def convert_sesame(sesame_file: str) -> Iterator[Tuple[int, MultiLabelAnnotation]]:
    sesame_annotations = read_conll09(sesame_file)
    grouped_by_sentence = itertools.groupby(sesame_annotations, key=lambda id_and_anno: id_and_anno[0])
    annotation_groups = []
    for _, ids_and_annotations in grouped_by_sentence:
        ids_and_annotations = list(ids_and_annotations)
        sent_idx = ids_and_annotations[0][0]
        annotations = [anno for sent_idx, anno in ids_and_annotations]
        annotation_groups.append((sent_idx, annotations))

    for sentence_id, sent_annotations in annotation_groups:
        yield sentence_id, sesame_to_multilabel(sent_annotations)


def sesame_to_multilabel(sent_annotations: List[OpenSesameAnnotation],
                         use_s_iob: bool = False
                         ) -> MultiLabelAnnotation:

    tokens = sent_annotations[0].tokens
    pos_list = sent_annotations[0].pos
    frame_list = [[] for _ in tokens]
    lu_list = [None for _ in tokens]
    out_annotation = MultiLabelAnnotation(tokens=tokens, pos=pos_list, lu_list=lu_list, frame_list=frame_list)

    for anno_idx, in_annotation in enumerate(sent_annotations):

        frame_name = next(frame for frame in in_annotation.frame if frame)

        # loop over token sequence in input
        for i, _ in enumerate(in_annotation.tokens):

            # target --> write "T:$FRAME@$ANNO_IDX"
            if in_annotation.lu[i]:
                out_annotation.lu_list[i] = in_annotation.lu[i]
                out_annotation.frame_list[i].append(f"T:{frame_name}@{anno_idx:02}")

            # role --> write "$IOB:$FRAME:$ROLE@$ANNO_IDX"
            if in_annotation.role_iob[i] in ["S", "B", "I"]:
                iob_tag = in_annotation.role_iob[i]

                # replace "S" by "B" for reducing label sparsity
                if iob_tag == "S" and not use_s_iob:
                    iob_tag = "B"

                role_string = f"{iob_tag}:{frame_name}:{in_annotation.role_name[i]}@{anno_idx:02}"
                out_annotation.frame_list[i].append(role_string)

    return out_annotation


# based on from framenet-parsing project
# NB assumes empty line at the end of the file!
def read_conll09(file_in: str) -> Iterator[Tuple[int, OpenSesameAnnotation]]:
    current_annotation = None
    current_sent_id = 0

    with open(file_in, encoding="utf-8") as f:
        for i, line in enumerate(f):

            if i % 1000 == 0:
                print(f"{i}...", end="", flush=True)

            # produce frame annotation after every line
            if not line.strip():
                if current_annotation:
                    yield current_sent_id, current_annotation
                    current_annotation = None
                continue

            # remove underscores
            columns = line.split("\t")
            for col_idx, col in enumerate(columns):
                if col == "_":
                    columns[col_idx] = None

            if int(columns[0]) == 1:
                current_annotation = OpenSesameAnnotation()
            current_annotation.tokens.append(columns[1])
            current_annotation.pos.append(columns[5])
            # replace spaces in LU names
            current_annotation.lu.append(columns[12].replace(" ", "_") if columns[12] else None)
            current_annotation.frame.append(columns[13])

            # roles are given as "X-RoleName", with X in [I, O, B, S]
            role = columns[14].strip()
            current_annotation.role_iob.append(role[0])
            current_annotation.role_name.append(role[2:])

            # keep track of sentence IDs
            current_sent_id = int(columns[6])

        # yield last annotation if any
        if current_annotation:
            yield current_sent_id, current_annotation

        print("")


if __name__ == '__main__':
    # convert_framenet_corpus(corpus="framenet", include_exemplars=False)
    # convert_framenet_corpus(corpus="framenet", include_exemplars=True)
    convert_framenet_corpus(corpus="open_sesame_output")
    # convert_framenet_corpus(corpus="tatoeba")
    # convert_framenet_corpus(corpus="simpleframeid_output", include_exemplars=False)