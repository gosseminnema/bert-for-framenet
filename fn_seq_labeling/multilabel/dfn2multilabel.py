"""
Convert SONAR Dutch FrameNet annotations
"""
from collections import defaultdict
from xml.etree import ElementTree as ET
import random
import glob
import os
import math

import spacy
from spacy.tokens import Doc

from fn_seq_labeling.multilabel.frame_annotations import MultiLabelAnnotation, write_multilabel_outputs

ANNOTATORS = ("A1", "A2")

TEST_HOLDOUT_RATIO = 0.2
DEV_HOLDOUT_RATIO = 0.1

TAGS_TO_EXCLUDE = ["None", "MWE"]


def main():
    print("Initializing...")
    random.seed(1996)
    nlp = setup_spacy()

    print("Splitting...")
    annotation_files = [os.path.basename(f) for f in glob.glob("data/dutch-sonar/corpus/A1/*.xml")]
    train_files, dev_files, test_files = split_data_portions(annotation_files)
    write_split_file(dev_files, test_files, train_files)

    print("Extracting annotations...")
    for annotator in ANNOTATORS:
        for split, files in (("train", train_files), ("dev", dev_files), ("test", test_files)):
            print(f"\tannotator: {annotator}, split: {split}")

            split_annotations = []
            split_raw_texts = []

            for anno_file in files:
                print(f"\t\tfile: {anno_file}")
                annotations, raw_text = extract_annotations(f"data/dutch-sonar/corpus/{annotator}/{anno_file}", nlp)
                split_annotations.extend(annotations)
                split_raw_texts.extend(raw_text)

            write_multilabel_outputs(f"dutch-sonar-{split}-{annotator}", split_annotations)
            with open(f"data/multilabel/dutch-sonar-{split}-{annotator}.sents.txt", "w", encoding="utf-8") as f_txt:
                for line in split_raw_texts:
                    f_txt.write(line + os.linesep)


def write_split_file(dev_files, test_files, train_files):
    with open("data/dutch-sonar/train-dev-test-splits.txt", "w", encoding="utf-8") as f:
        f.write("TRAIN:\n\n")
        f.writelines([file + "\n" for file in train_files])
        f.write("\n\nDEV:\n\n")
        f.writelines([file + "\n" for file in dev_files])
        f.write("\n\nTEST:\n\n")
        f.writelines([file + "\n" for file in test_files])


def split_data_portions(annotation_files):
    num_total_files = len(annotation_files)
    num_test_files = math.floor(TEST_HOLDOUT_RATIO * num_total_files)
    num_dev_files = math.floor(DEV_HOLDOUT_RATIO * num_total_files)
    num_train_files = num_total_files - num_test_files - num_dev_files

    shf_annotation_files = sorted(annotation_files)
    random.shuffle(shf_annotation_files)
    train_files = shf_annotation_files[:num_train_files]
    dev_files = shf_annotation_files[num_train_files:num_train_files+num_dev_files]
    test_files = shf_annotation_files[num_train_files+num_dev_files:]
    return train_files, dev_files, test_files


def setup_spacy():
    nlp = spacy.load("nl_core_news_sm")

    def spacy_dont_retokenize(text):
        tokens = text.split()
        return Doc(nlp.vocab, tokens)

    nlp.tokenizer = spacy_dont_retokenize
    return nlp


def extract_annotations(annotation_file, nlp):

    tree = ET.parse(annotation_file)
    root = tree.getroot()

    sent_to_tids, tid_to_token = read_tokens(root)
    mid_to_tids = read_markables(root)
    tid_to_fn = read_fn_annotations(root, mid_to_tids)
    annotations, text = compile_annotations(sent_to_tids, tid_to_token, tid_to_fn, nlp)
    return annotations, text


def read_tokens(root):
    tid_to_token = {}
    sent_to_tid = defaultdict(list)
    tokens = root.findall("token")
    for token in tokens:
        sent = token.attrib["sentence"]
        tid = token.attrib["t_id"]
        tid_to_token[tid] = token.text
        sent_to_tid[sent].append(tid)
    return sent_to_tid, tid_to_token


def read_markables(root):
    mid_to_tids = {}

    event_mentions = root.findall("./Markables/EVENT_MENTION")
    entity_mentions = root.findall("./Markables/ENTITY_MENTION")
    for mention in event_mentions + entity_mentions:
        mid = mention.attrib["m_id"]
        tids = []
        for anchor in mention.findall("token_anchor"):
            tids.append(anchor.attrib["t_id"])
        mid_to_tids[mid] = tids
    return mid_to_tids


def read_fn_annotations(root, mid_to_tids):

    tid_to_fn = defaultdict(set)

    relations = root.findall("./Relations/HAS_PARTICIPANT")
    for rel in relations:
        # parse info for the relation
        frame = rel.attrib["frame"]
        role = rel.attrib["frame_element"]
        event_mid = rel.find("source").attrib["m_id"]
        entity_mid = rel.find("target").attrib["m_id"]

        # skip empty annotations
        if frame in TAGS_TO_EXCLUDE or role in TAGS_TO_EXCLUDE:
            continue

        # retrieve t_id
        event_tids = mid_to_tids[event_mid]
        entity_tids = mid_to_tids[entity_mid]

        for ev_tid in event_tids:
            tid_to_fn[ev_tid].add(f"T:{frame}@{event_mid}")

        first_token = True
        for en_tid in entity_tids:
            bio = "B" if first_token else "I"
            tid_to_fn[en_tid].add(f"{bio}:{frame}:{role}@{event_mid}")
            first_token = False

    return tid_to_fn


def compile_annotations(sent_to_tids, tid_to_token, tid_to_fn, nlp):
    annotations = []
    text = []

    for sent_id in sorted(sent_to_tids):
        tids = sorted(sent_to_tids[sent_id], key=int)
        tokens = [tid_to_token[tid] for tid in tids]
        pos_tags = [tok.pos_ for tok in nlp(" ".join(tokens))]
        lu_tags = [None for _ in tokens]
        frame_annotations = []
        for tid in tids:
            frame_annotations.append(sorted(tid_to_fn.get(tid, [])))
        ml_annotation = MultiLabelAnnotation(tokens, pos_tags, frame_annotations, lu_tags)
        annotations.append(ml_annotation)
        text.append(" ".join(tokens))
    return annotations, text


if __name__ == '__main__':
    main()
