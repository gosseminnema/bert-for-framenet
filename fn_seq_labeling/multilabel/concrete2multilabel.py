import dataclasses
import json
import os
from collections import defaultdict
from typing import List
import sys

import nltk
from concrete import Communication
from concrete.util import read_communication_from_file, lun, get_tokens, get_tagged_tokens

from fn_seq_labeling.multilabel import frame_annotations
import fn_seq_labeling.constants as cns


def convert_file(file, language="english"):

    print("Reading input file...")
    comm = read_communication_from_file(file)

    print("Mapping sentences to situations...")
    tok_uuid_to_situation = map_sent_to_situation(comm)

    print("# sentences with situations:", len(tok_uuid_to_situation))

    for section in lun(comm.sectionList):
        for sentence in lun(section.sentenceList):
            tokens = get_tokens(sentence.tokenization)
            situations = tok_uuid_to_situation[sentence.tokenization.uuid.uuidString]
            tok_to_annos = map_tokens_to_annotations(comm, situations)

            frame_list, tok_list = prepare_ml_lists(language, tok_to_annos, tokens)

            ml_anno = frame_annotations.MultiLabelAnnotation(tok_list, ["_" for _ in tok_list], frame_list,
                                                             [None for _ in tok_list])
            yield ml_anno


def prepare_ml_lists(language, tok_to_annos, tokens):
    tok_list = []
    frame_list = []
    for tok_idx, tok in enumerate(tokens):
        # split tokens that include punctuation
        split_tok = nltk.word_tokenize(tok.text, language=language)
        tok_list.extend(split_tok)
        tok_anno = []
        for anno in tok_to_annos.get(tok_idx, []):
            tok_anno.append(anno)
        frame_list.extend([list(tok_anno) for _ in split_tok])

    # remove annotations from final punctuation
    for idx, (tok, frame_annos) in enumerate(zip(tok_list, frame_list)):
        if tok in ",.:;\"'`«»":
            to_delete = []
            for fa in frame_annos:
                if fa.startswith("T:"):
                    compare_fa = fa
                else:
                    compare_fa = "I" + fa[1:]

                if idx == len(tok_list) - 1:
                    to_delete.append(fa)
                elif compare_fa not in frame_list[idx + 1]:
                    to_delete.append(fa)

            for fa in to_delete:
                frame_annos.remove(fa)

    return frame_list, tok_list


def map_tokens_to_annotations(comm: Communication, situations: List[str]):
    tok_to_annos = defaultdict(list)
    for sit_idx, sit_uuid in enumerate(situations):
        situation = comm.situationMentionForUUID[sit_uuid]
        frame_type = situation.situationKind
        tgt_tokens = situation.tokens.tokenIndexList
        for tok_id in tgt_tokens:
            tok_to_annos[tok_id].append(f"T:{frame_type}@{sit_idx:02}")
        for arg in situation.argumentList:
            fe_type = arg.role
            fe_tokens = arg.entityMention.tokens.tokenIndexList
            for tok_n, tok_id in enumerate(fe_tokens):
                if tok_n == 0:
                    bio = "B"
                else:
                    bio = "I"
                tok_to_annos[tok_id].append(f"{bio}:{frame_type}:{fe_type}@{sit_idx:02}")
    return tok_to_annos


def map_sent_to_situation(comm):
    tok_uuid_to_situation = defaultdict(list)
    for situation in comm.situationMentionSetList:
        for mention in situation.mentionList:
            tok_uuid_to_situation[mention.tokens.tokenizationId.uuidString].append(mention.uuid.uuidString)
    return tok_uuid_to_situation


def main():
    file_in = sys.argv[1]
    language = sys.argv[2]

    file_in_base = os.path.basename(file_in)
    file_out = f"{cns.MULTILABEL_FN_CORPUS}/lome_{file_in_base}"
    multilabel_annos = list(convert_file(file_in, language=language))
    multi_label_json = [dataclasses.asdict(anno) for anno in multilabel_annos]

    with open(f"{file_out}.json", "w", encoding="utf-8") as f_json:
        json.dump(multi_label_json, f_json, indent=4)

    with open(f"{file_out}.txt", "w", encoding="utf-8") as f_txt:
        for anno in multilabel_annos:
            for line in anno.to_txt():
                f_txt.write(line + os.linesep)
            f_txt.write(os.linesep)


if __name__ == '__main__':
    main()
