from typing import Set, Dict

import pandas as pd

from nltk.corpus import framenet as fn

from fn_seq_labeling.evaluation.eval_utils import split_sentences


def main(prediction_file):
    postprocessor = Postprocessor(prediction_file=prediction_file)
    cleaned_df = postprocessor.postprocess_predictions()
    cleaned_df.to_csv(prediction_file.replace(".csv", ".cleaned.csv"))


class Postprocessor:

    def __init__(self,
                 prediction_df: pd.DataFrame = None,
                 prediction_file: str = None
                 ):

        if not (prediction_df or prediction_file):
            raise ValueError("Either a dataframe or a CSV file should be given")

        if prediction_file:
            prediction_df = pd.read_csv(prediction_file)
        else:
            prediction_df = prediction_df

        self.sentences = split_sentences(prediction_df=prediction_df)
        self.frames_to_roles: Dict[str, Set[str]] = {}

    def postprocess_predictions(self):
        cleaned_dfs = []
        for sentence_df in self.sentences:
            without_surplus_frames = self._clean_surplus_frames(sentence_df)
            sentence_cleaned = self._clean_dangling_roles_sent(without_surplus_frames)
            cleaned_dfs.append(sentence_cleaned)
        return pd.concat(cleaned_dfs, ignore_index=True)

    @staticmethod
    def _clean_surplus_frames(sentence_df: pd.DataFrame) -> pd.DataFrame:
        output_rows = []
        for i, row in sentence_df.iterrows():
            predictions = row["prediction"].split("|")
            frame_predictions = [p for p in predictions if p.startswith("T:")]

            # there should be maximally one frame target
            if len(frame_predictions) > 1:
                # keep only the frame with the highest probability
                cleaned_frame_predictions = max(frame_predictions, key=lambda p: float(p.split(":")[2]))
            else:
                cleaned_frame_predictions = frame_predictions
            surplus_frames = [fp for fp in frame_predictions if fp not in cleaned_frame_predictions]
            cleaned_predictions = [p for p in predictions if p not in surplus_frames]
            output_rows.append({
                "token": row["token"],
                "prediction": "|".join(cleaned_predictions),
                "gold": row["gold"],
                "surplus_frames": "|".join(surplus_frames)
            })

        return pd.DataFrame(output_rows)

    def _clean_dangling_roles_sent(self, sentence_df: pd.DataFrame) -> pd.DataFrame:

        frames = self.collect_frames(sentence_df)
        possible_roles = self.find_possible_roles(frames)

        output_rows = []

        for i, row in sentence_df.iterrows():

            prediction_str = row["prediction"]
            if prediction_str == "_":
                output_rows.append(dict(row))
                continue
            predictions = prediction_str.split("|")
            dangling_roles, predictions_cleaned = self._remove_dangling_roles(possible_roles, predictions)

            output_rows.append({
                "token": row["token"],
                "prediction": "|".join(predictions_cleaned) or "_",
                "gold": row["gold"],
                "surplus_frames": row["surplus_frames"] if "surplus_frames" in row else "",
                "dangling_roles": "|".join(dangling_roles) or "_",
            })

        return pd.DataFrame(output_rows)

    @staticmethod
    def _remove_dangling_roles(possible_roles, predictions):
        predictions_cleaned = []
        dangling_roles = []
        for p in predictions:
            # always keep frame targets
            if p.startswith("T:"):
                predictions_cleaned.append(p)
            elif p.startswith("B:") or p.startswith("I:"):
                print(f"[WARNING] Label starts with IOB tag, this seems to be a mistake (p={p})")
            else:
                role_name, _ = p.split(":")
                if role_name in possible_roles:
                    predictions_cleaned.append(p)
                else:
                    dangling_roles.append(p)
        return dangling_roles, predictions_cleaned

    def find_possible_roles(self, frames: Set[str]) -> Set[str]:

        possible_roles = set()

        for f in frames:
            if f not in self.frames_to_roles:
                self.frames_to_roles[f] = set(fn.frame(f).FE.keys())
            possible_roles.update(self.frames_to_roles[f])

        return possible_roles

    @staticmethod
    def collect_frames(sentence_df: pd.DataFrame) -> Set[str]:
        frames = set()
        for i, row in sentence_df.iterrows():
            predictions = row["prediction"].split("|")
            for pred in predictions:
                if pred.startswith("T:"):
                    frame = pred.split(":")[1]
                    frames.add(frame)
        return frames


if __name__ == '__main__':
    main("output/predictions/2020-03-18_mlp_decoder_hidden1000_tatoeba_dev70_0.1.csv")
    main("output/predictions/2020-03-18_mlp_decoder_hidden1000_tatoeba_dev70_0.2.csv")
    main("output/predictions/2020-03-18_mlp_decoder_hidden1000_tatoeba_dev70_0.3.csv")
    main("output/predictions/2020-03-18_mlp_decoder_hidden1000_tatoeba_dev70_0.4.csv")
    main("output/predictions/2020-03-18_mlp_decoder_hidden1000_tatoeba_dev70_0.5.csv")
    main("output/predictions/2020-04-08_use_only_top_bert_layer_tatoeba_dev70_0.1.csv")
    main("output/predictions/2020-04-08_use_only_top_bert_layer_tatoeba_dev70_0.2.csv")
    main("output/predictions/2020-04-08_use_only_top_bert_layer_tatoeba_dev70_0.3.csv")
    main("output/predictions/2020-04-08_use_only_top_bert_layer_tatoeba_dev70_0.4.csv")
    main("output/predictions/2020-04-08_use_only_top_bert_layer_tatoeba_dev70_0.5.csv")

