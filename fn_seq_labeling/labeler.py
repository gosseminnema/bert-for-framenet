import argparse
import csv
import dataclasses
import enum
import json
import os
import random
import shutil
from typing import Iterable, List, ClassVar, Optional, Dict, Any, Tuple

import numpy as np
import pandas as pd
import torch
import torch.optim as optim
from allennlp.data import Instance, Vocabulary
from allennlp.data.iterators import BucketIterator
from allennlp.data.token_indexers import PretrainedTransformerIndexer
from allennlp.models import Model
from allennlp.modules import Embedding
from allennlp.modules.token_embedders import PretrainedBertEmbedder, PretrainedTransformerEmbedder
from allennlp.nn import util as nn_util
from allennlp.training import Trainer
from allennlp.training.checkpointer import Checkpointer
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.preprocessing import MultiLabelBinarizer
from tqdm import tqdm

import fn_seq_labeling.constants as cns
from fn_seq_labeling.datasets import FrameNetDatasetReader, bert_tokenize, init_bert_tokenizer
from fn_seq_labeling.embed_frames import BagOfLexUnitsEmbedder, FrameNameEmbedder
from fn_seq_labeling.evaluation.accuracy_score import fn_accuracy_score
from fn_seq_labeling.evaluation.error_analysis import analyze_errors, ErrorAnalysisLevel
from fn_seq_labeling.evaluation.merge_frames_roles import merge_prediction_files
from fn_seq_labeling.evaluation.merge_split_frames import merge_split_frames
from fn_seq_labeling.fchecks_experiments import evaluate_fchecks_grid
from fn_seq_labeling.infer_frames import LikeliestFramesInferrer, CombinedFrameInferrer
from fn_seq_labeling.label_fields import FrameContextField
from fn_seq_labeling.models import BertFnTagger, FrameNetDecoder, BertFnMultiTaskLabeler
from fn_seq_labeling.utils import evaluate_and_predict, get_lemmas_to_forms

if os.path.exists("/scratch"):
    MODEL_OUTPUT_DIR = "/scratch/p289731"
else:
    MODEL_OUTPUT_DIR = "output"
print(MODEL_OUTPUT_DIR)


@dataclasses.dataclass
class FileNameData:
    basename: str
    model_name: str


@dataclasses.dataclass
class HelperModelData:
    """
    Stores helper models for representing the label space
    """
    # binarizer: represent a series of labels as a binary vector
    # e.g. [Cat, Dog, Camel] -> [0, 1, 0, 1, 1, 0]
    st_binarizer: Optional[MultiLabelBinarizer]  # single-task: frames and/or roles, depending on settings
    mt_frame_binarizer: Optional[MultiLabelBinarizer]  # multi-task: only frames
    mt_role_binarizer: Optional[MultiLabelBinarizer]  # multi-task: only roles

    # pca: map binary vectors to a lower-dimensional space
    # e.g. [0, 1, 0, 1, 1, 0] -> <np.array of shape (N,)>
    pca_model: Optional[PCA]

    # frame embedder (only for frame names): pre-defined representations of FrameNet frames
    # e.g. "Killing" -> <np.array of shape (N,)>
    frame_embedder: Optional[Dict[str, np.ndarray]]


class FnSeqLabelingPipeline:

    def __init__(self,
                 config_file,
                 load_external_dev_predictions: Optional[str] = None,
                 external_prediction_file: Optional[str] = None,
                 load_hartmann_dev: bool = False,
                 load_tatoeba_dev: bool = False,
                 filter_idx: Iterable[int] = None
                 ):
        self.config = self._parse_config(config_file)
        self.external_dev_name = load_external_dev_predictions

        if self.config[cns.RANDOM_SEED] is not None:
            random.seed(self.config[cns.RANDOM_SEED])
            print(f"RANDOM SEED = {self.config[cns.RANDOM_SEED]}")

        assert not (load_hartmann_dev and load_tatoeba_dev)

        if load_hartmann_dev:
            self.dev_set_label = "hartmann"
        elif load_tatoeba_dev:
            self.dev_set_label = "tatoeba"
        else:
            self.dev_set_label = ""

        # define model names
        self.file_names = self._define_file_names(config_file)
        print(f"Model: {self.file_names.model_name}")

        # label representation tools
        self.label_models = self.init_label_models()

        # text embeddings -- BERT tokenizer
        print("Initializing tokenizer ...")
        if self.config[cns.MULTILINGUAL_BERT]:
            self.token_indexer = PretrainedTransformerIndexer("bert-base-multilingual-cased", False, "tokens")
        elif self.config[cns.LANGUAGE] == cns.Languages.DUTCH:
            self.token_indexer = PretrainedTransformerIndexer("GroNLP/bert-base-dutch-cased", False, "tokens")
        else:
            self.token_indexer = PretrainedTransformerIndexer("bert-base-cased", False, "tokens")

        # datasets
        self._load_train_and_dev_data(load_hartmann_dev, load_tatoeba_dev, load_external_dev_predictions,
                                      external_prediction_file)

        # vocabulary
        self.vocab = Vocabulary.from_instances(self.training_set + self.orig_dev_set)
        print("self.vocab=", self.vocab)

        # # todo: remove after debug
        # print("self.dev_set[0][\"sentence\"] =>", self.dev_set[0]["sentence"])
        # print("self.dev_set[0][\"sentence\"].index(self.vocab) =>", self.dev_set[0]["sentence"].index(self.vocab))
        # exit(-1)

        if self.config[cns.MULTITASK]:
            frame_label_size = self.vocab.get_vocab_size("frame_labels")
            role_label_size = self.vocab.get_vocab_size("role_labels")
            print(f"There are {frame_label_size} frame labels and {role_label_size} role labels")
        else:
            label_size = self.vocab.get_vocab_size("labels")
            print("There are", label_size, "labels")

        # keep only certain indices of the dev set (for comparison with open-sesame performance)
        if filter_idx:
            self.dev_set = [self.dev_set[i] for i in filter_idx]
        print(len(self.dev_set))

        # model
        print("Setting up model ...")
        if self.config[cns.MULTITASK]:
            self.model = self._init_multitask_model()
        else:
            self.model = self._init_single_task_model()

        # iterator
        print("Preparing iterator ...")
        self.iterator = BucketIterator(batch_size=2, sorting_keys=[("sentence", "num_tokens")])
        self.iterator.index_with(self.vocab)

        # fit PCA model if necessary
        if self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED_PCA:
            label_namespace = "labels"
            assert not self.config[cns.MULTITASK], "Multitask learning is not compatible with PCA binarization"

            print("Fitting PCA model ...")
            training_output_array = self._get_label_array(self.training_set, label_namespace)
            dev_output_array = self._get_label_array(self.dev_set, label_namespace)
            self.label_models.pca_model.fit(training_output_array)

            # test how well the encoding works
            train_r, train_p = self._evaluate_pca(training_output_array)
            dev_r, dev_p = self._evaluate_pca(dev_output_array)
            print(f"PCA error rate: train_r={train_r}, train_p={train_p}, dev_r={dev_r}, dev_p={dev_p}")

        # CUDA
        self.cuda_device, self.model = self._init_gpu()
        print("Tuning", len([p for p in self.model.parameters() if p.requires_grad]), "parameters")

        if self.config[cns.ADD_FRAME_CONTEXT_VECS]:
            print(f"Killing ID: {FrameContextField.FRAMES_TO_ID['Killing']}")

    def _load_train_and_dev_data(self,
                                 load_hartmann_dev: bool,
                                 load_tatoeba_dev: bool,
                                 load_external_dev_predictions: Optional[str],
                                 external_prediction_file: Optional[str]
                                 ):
        self._load_fulltext_data(load_hartmann_dev, load_external_dev_predictions,
                                 external_prediction_file, load_tatoeba_dev)
        self._load_exemplar_data()

        # use subset of training examples
        assert 0 <= self.config[cns.USE_TRAIN_DATA_PORTION] <= 1.0
        fulltext_train_length = len(self.fulltext_train)
        reduced_length = round(fulltext_train_length * self.config[cns.USE_TRAIN_DATA_PORTION])

        if self.config[cns.SHUFFLE_BEFORE_PORTIONING]:
            print("Shuffling training data for portioning...")
            random.shuffle(self.fulltext_train)

        self.fulltext_train = self.fulltext_train[:reduced_length]
        print(f"fulltext_train: original length = {fulltext_train_length}, reduced length = {reduced_length}")

        if self.config[cns.FULLTEXT_EXEMPLAR_RATIO] > 1:
            self.training_set = (self.fulltext_train * self.config[cns.FULLTEXT_EXEMPLAR_RATIO]) + self.exemplar_train
        else:
            self.training_set = self.fulltext_train + self.exemplar_train

        if self.config[cns.EXTRA_TRAIN_DATA_FILES]:
            for file in self.config[cns.EXTRA_TRAIN_DATA_FILES]:
                print(f"Adding extra data from file {self.config[cns.EXTRA_TRAIN_DATA_FILES]}")
                extra_data, = self._load_datasets([file])
                self.training_set.extend(extra_data)

        print(len(self.training_set))

    def _load_fulltext_data(self,
                            load_hartmann_dev: bool,
                            evaluate_external_model: Optional[str],
                            external_prediction_file: Optional[str],
                            load_tatoeba_dev: bool
                            ):

        if self.config[cns.LANGUAGE] == cns.Languages.ENGLISH:
            train_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, "fn1.7.fulltext.train.syntaxnet.txt")
            self.dev_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, "fn1.7.dev.syntaxnet.txt")
            self.sonar_annotator = None
        elif self.config[cns.LANGUAGE] == cns.Languages.DUTCH:
            annotator = "A1" if self.config[cns.SONAR_FN_ANNOTATOR] == cns.SonarAnnotators.A1 else "A2"
            train_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, f"dutch-sonar-train-{annotator}.txt")
            self.dev_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, f"dutch-sonar-dev-{annotator}.txt")
            self.sonar_annotator = annotator
        else:
            raise ValueError("Invalid language option!")

        assert not (load_hartmann_dev and evaluate_external_model), \
            "Running sesame on hartmann dev not supported yet!"
        if load_hartmann_dev:
            hartmann_dev_set_file = os.path.join(cns.HARTMANN_CORPUS, "dev.txt")
            self.external_dev_predictions = None
            self.fulltext_train, self.orig_dev_set = \
                self._load_datasets([train_set_file, self.dev_set_file])
            self.dev_set, = self._load_datasets([hartmann_dev_set_file], freeze_binarizer=True)

        elif load_tatoeba_dev:
            tatoeba_dev_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, "manual-gosse-tatoeba-dev75.tabs.txt")
            self.external_dev_predictions = None
            self.fulltext_train, self.orig_dev_set = \
                self._load_datasets([train_set_file, self.dev_set_file])
            self.dev_set, = self._load_datasets([tatoeba_dev_set_file], freeze_binarizer=True)

        elif evaluate_external_model:

            assert evaluate_external_model in ["sesame", "simpleframeid", "lome"]

            external_predictions = os.path.join(
                cns.MULTILABEL_FN_CORPUS,
                external_prediction_file
            )
            self.external_prediction_file = external_predictions
            aligned_dev_gold = os.path.join(
                cns.MULTILABEL_FN_CORPUS,
                # f"fn1.7.dev.syntaxnet.aligned_{load_external_dev_predictions}.txt"
                "fn1.7.dev.syntaxnet.txt" if self.config[cns.LANGUAGE] == cns.Languages.ENGLISH
                else f"dutch-sonar-dev-{self.sonar_annotator}.txt"
            )
            self.fulltext_train, = self._load_datasets([train_set_file])
            self.external_dev_predictions, self.dev_set = self._load_datasets([external_predictions, aligned_dev_gold])
            self.orig_dev_set = self.dev_set

        else:
            # self.fulltext_train, self.dev_set = self._load_datasets([train_set_file, self.dev_set_file])
            self.fulltext_train, = self._load_datasets([train_set_file])
            print("Loading DEV set: ", self.dev_set_file)
            self.dev_set, = self._load_datasets([self.dev_set_file])

            self.external_prediction_file = None
            self.external_dev_predictions = None
            self.orig_dev_set = self.dev_set

    def _load_exemplar_data(self):
        if self.config[cns.INCLUDE_EXEMPLARS]:
            if self.config[cns.CLIP_EXEMPLARS]:
                assert self.config[cns.MERGE_EXEMPLARS]
                window = self.config[cns.CLIP_WINDOW]
                if self.config[cns.CLIP_ONLY_FRAMES]:
                    exemplar_file = f"fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped_frames{window}.txt"
                else:
                    exemplar_file = f"fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped{window}.txt"
            elif self.config[cns.MERGE_EXEMPLARS]:
                exemplar_file = "fn1.7.exemplar.train.syntaxnet.filtered.merged.txt"
            else:
                exemplar_file = "fn1.7.exemplar.train.syntaxnet.filtered.txt"

            if self.config[cns.SILVER_EXEMPLARS]:
                postfix = self.config[cns.SILVER_DATA_POSTFIX]
                exemplar_file = exemplar_file.replace(".txt", f".silver_frames{postfix}.txt")

            if self.config[cns.ONLY_RARE_EXEMPLARS]:
                exemplar_file = exemplar_file.replace(".txt", ".only_rare.txt")

            exemplar_sents = os.path.join(cns.MULTILABEL_FN_CORPUS, exemplar_file)
            print(f"Loading exemplars from {exemplar_sents}")

            # use only exemplars, no full text
            if self.config[cns.REMOVE_FULLTEXT_TRAIN]:
                self.fulltext_train = []

            self.exemplar_train = self._load_datasets([exemplar_sents])[0]
        else:
            self.exemplar_train = []

    def init_label_models(self) -> HelperModelData:

        # 1 - binarization and dimensionality reduction
        st_binarizer = mt_frame_binarizer = mt_role_binarizer = pca_model = None

        # do we binarize at all?
        if self.config[cns.MULTILABEL_SETTING_CONFIG] in cns.CONFIGS_WITH_BINARIZATION:

            if self.config[cns.MULTITASK]:
                mt_frame_binarizer = MultiLabelBinarizer()
                mt_role_binarizer = MultiLabelBinarizer()
            else:
                st_binarizer = MultiLabelBinarizer()

            # do we use PCA?
            if self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED_PCA:
                if self.config[cns.PCA_ALGORITHM] == cns.PcaAlgorithm.SVD:
                    pca_algorithm = TruncatedSVD
                else:
                    pca_algorithm = PCA
                pca_model = pca_algorithm(n_components=self.config[cns.LABEL_EMBEDDING_DIMENSIONS], random_state=100)

        elif self.config[cns.ADD_ROLES_CONTEXT_VECS]:
            mt_role_binarizer = MultiLabelBinarizer()

        # 2 - frame embeddings
        frame_embedder = None
        if self.config[cns.EMBED_FRAME_LABELS] == cns.FrameLabelEmbeddingSetting.BAG_OF_LU:
            frame_embedder = BagOfLexUnitsEmbedder().embed_as_dict()
        elif self.config[cns.EMBED_FRAME_LABELS] == cns.FrameLabelEmbeddingSetting.FRAME_NAMES:
            frame_embedder = FrameNameEmbedder().embed_as_dict()
        print(self.config[cns.EMBED_FRAME_LABELS])

        return HelperModelData(st_binarizer, mt_frame_binarizer, mt_role_binarizer, pca_model, frame_embedder)

    @staticmethod
    def _define_file_names(config_file):
        basename = os.path.splitext(os.path.basename(config_file))[0]
        model_name = os.path.join(MODEL_OUTPUT_DIR, basename + ".th")
        file_names = FileNameData(basename, model_name)
        return file_names

    def train(self):
        """
        Train the frame labeler given a specific configuration
        :return:
        """

        # hyperparams
        learning_rate = self.config[cns.LEARNING_RATE]
        epochs = self.config[cns.EPOCHS]
        optimizer = optim.Adam(self.model.parameters(), lr=learning_rate)

        # output directories for tensorboard and checkpointer (only works on Peregrine)
        if os.path.exists("/scratch/p289731"):
            tensorboard_dir = f"/scratch/p289731/tensorboard_{self.file_names.basename}"
            checkpoint_dir = f"/scratch/p289731/checkpoints_{self.file_names.basename}"
            if os.path.isdir(tensorboard_dir):
                shutil.rmtree(tensorboard_dir)
            os.makedirs(tensorboard_dir)
            if os.path.isdir(checkpoint_dir):
                shutil.rmtree(checkpoint_dir)
            os.makedirs(checkpoint_dir)
        else:
            tensorboard_dir = checkpoint_dir = None

        # train the model
        checkpointer = Checkpointer(serialization_dir=checkpoint_dir)

        trainer = Trainer(self.model, optimizer, self.iterator, self.training_set, self.dev_set,
                          num_epochs=epochs, patience=10, cuda_device=self.cuda_device,
                          histogram_interval=None, serialization_dir=tensorboard_dir, checkpointer=checkpointer)
        trainer.train()

        with open(self.file_names.model_name, "wb") as f:
            torch.save(self.model.state_dict(), f)

        if self.config[cns.MULTITASK]:
            print(f"Final loss weight values: role_loss_weight={self.model.role_loss_weight}; "
                  f"frame_loss_weight={self.model.frame_loss_weight}")

    def evaluate(self, dev=True, external_data=None, evaluate_predicate=None, predicate_morph_file=None):
        """
        Load a trained model, produce predictions and calculate accuracy scores (ON DEV)
        :return:
        """

        if evaluate_predicate is not None:
            assert predicate_morph_file is not None
            target_forms = get_lemmas_to_forms(pd.read_excel(predicate_morph_file), True)[evaluate_predicate]
        else:
            target_forms = None

        if external_data:
            eval_set_file = external_data
            eval_set, = self._load_datasets([external_data], freeze_binarizer=True)

        elif dev:
            eval_set_file = self.dev_set_file
            eval_set = self.dev_set

        # test set
        else:
            print("WARNING: loading TEST set, do not do this unless for final evaluation!")

            if self.config[cns.LANGUAGE] == cns.Languages.ENGLISH:
                test_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, "fn1.7.test.syntaxnet.txt")
            elif self.config[cns.LANGUAGE] == cns.Languages.DUTCH:
                annotator = "A1" if self.config[cns.SONAR_FN_ANNOTATOR] == cns.SonarAnnotators.A1 else "A2"
                test_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, f"dutch-sonar-test-{annotator}.txt")
            else:
                raise ValueError("Invalid language option!")
            test_set, = self._load_datasets([test_set_file], freeze_binarizer=True)
            eval_set_file = test_set_file
            eval_set = test_set

        if self.config[cns.INPUT_FRAME_PREDICTIONS] is not None:
            print(f"Loading DEV set with predicted frames from {self.config[cns.INPUT_FRAME_PREDICTIONS]} ...")
            dev_with_pred_frames, = self._load_datasets([eval_set_file],
                                                        use_predicted_frames=True,
                                                        freeze_binarizer=True)
            scores = self.eval_with_pred_frames(dev_with_pred_frames, target_forms, gold_set=eval_set)

        elif self.config[cns.INPUT_ROLES_PREDICTIONS] is not None:
            print(f"Loading DEV set with predicted roles from {self.config[cns.INPUT_ROLES_PREDICTIONS]} ...")
            eval_with_pred_roles, = self._load_datasets([eval_set_file],
                                                        use_predicted_roles=True,
                                                        freeze_binarizer=True)
            scores = self.eval_with_pred_frames(eval_with_pred_roles, target_forms, gold_set=eval_set)

        else:
            scores = self.eval_with_pred_frames(eval_set, target_forms)
        return scores

    def evaluate_exemplars(self, clipped=False):
        self.dev_set_label = "exemplars"
        if clipped:
            exemplar_file = "fn1.7.exemplar.train.syntaxnet.filtered.merged.clipped2.txt"
            self.dev_set_label += "_clipped"
        else:
            exemplar_file = "fn1.7.exemplar.train.syntaxnet.filtered.merged.txt"
        exemplar_sents = os.path.join(cns.MULTILABEL_FN_CORPUS, exemplar_file)
        exemplar_set = self._load_datasets([exemplar_sents])[0]
        self.eval_with_pred_frames(exemplar_set)

    def eval_with_pred_frames(self, evaluation_dataset, target_forms=None, gold_set=None):
        self.load_trained_model()

        # make predictions and calculate losses
        if self.config[cns.SPLIT_BY_FRAMES]:
            split_metrics, split_sentences, split_predictions, split_golds, split_frame_contexts = \
                evaluate_and_predict(self.model,
                                     evaluation_dataset,
                                     self.iterator,
                                     self.cuda_device,
                                     multitask=None)

            split_output_files = self._write_prediction_csv_files(split_sentences,
                                                                  split_predictions,
                                                                  split_golds,
                                                                  target_forms,
                                                                  split_frame_contexts=split_frame_contexts)
            if gold_set is not None:
                _, gold_split_sentences, gold_split_predictions, gold_split_golds, gold_split_frame_contexts = \
                    evaluate_and_predict(self.model, gold_set, self.iterator, self.cuda_device, multitask=None)

                gold_split_output_files = self._write_prediction_csv_files(gold_split_sentences,
                                                                           gold_split_predictions,
                                                                           gold_split_golds,
                                                                           target_forms,
                                                                           output_file_postfix="gold",
                                                                           split_frame_contexts=gold_split_frame_contexts)

            else:
                gold_split_output_files = [None for _ in split_output_files]

            merged_output_files = []
            merged_out_files_w_frames = []
            for split_out_file, gold_split_out_file in zip(split_output_files, gold_split_output_files):
                merged_out_file = split_out_file.replace(".csv", ".merged_frames.csv")
                merged_out_file_w_frames = merged_out_file.replace(".merged_frames.", ".merged_frames_w_names.")
                merge_split_frames(split_out_file,
                                   merged_out_file,
                                   gold_split_file=gold_split_out_file
                                   )
                merge_split_frames(split_out_file,
                                   merged_out_file_w_frames,
                                   gold_split_file=gold_split_out_file,
                                   add_frame_names=True
                                   )
                merged_output_files.append(merged_out_file)
                merged_out_files_w_frames.append(merged_out_file_w_frames)

            # output_files = split_output_files + merged_output_files
            output_files = merged_output_files + merged_out_files_w_frames

        # For multitask: combine predictions from the frame and role models
        elif self.config[cns.MULTITASK]:
            f_metrics, f_sentences, f_predictions, f_golds, _ = evaluate_and_predict(self.model,
                                                                                     evaluation_dataset,
                                                                                     self.iterator,
                                                                                     self.cuda_device,
                                                                                     multitask="frames"
                                                                                     )
            f_output_files = self._write_prediction_csv_files(f_sentences, f_predictions, f_golds, target_forms,
                                                              label_namespace="frame_labels")

            r_metrics, r_sentences, r_predictions, r_golds, _ = evaluate_and_predict(self.model,
                                                                                     evaluation_dataset,
                                                                                     self.iterator,
                                                                                     self.cuda_device,
                                                                                     multitask="roles"
                                                                                     )

            r_output_files = self._write_prediction_csv_files(r_sentences, r_predictions, r_golds, target_forms,
                                                              label_namespace="role_labels")

            print("Merging frame and role predictions ...")
            merged_output_files = []
            for f_out_file, r_out_file in zip(f_output_files, r_output_files):
                merged_out_file = f_out_file.replace(".frame_labels", "")
                merge_prediction_files(f_out_file, r_out_file, merged_out_file)
                merged_output_files.append(merged_out_file)

            output_files = f_output_files + r_output_files + merged_output_files

        else:
            metrics, sentences, predictions, golds, _ = evaluate_and_predict(self.model,
                                                                             evaluation_dataset,
                                                                             self.iterator,
                                                                             self.cuda_device
                                                                             )
            print(metrics)
            output_files = self._write_prediction_csv_files(sentences, predictions, golds, target_forms)
        # calculate accuracy scores
        scores = []
        for output_file in output_files:
            print(output_file)
            scores.append(self.do_score_and_error_analysis(output_file))
        return scores

    def predict_raw_text(self, text, typical_frames=None, inferrers=None):

        if typical_frames is None:
            typical_frames = ["Killing", "Offenses", "Commit_crime", "Operate_vehicle"]

        if inferrers is None:
            inferrers = [LikeliestFramesInferrer()]

        reader = self._init_dataset_reader()
        tokenizer = init_bert_tokenizer()
        instances = []

        for line in text.split(os.linesep):
            tokens, _ = bert_tokenize(line.split(), tokenizer)
            instances.append(reader.text_to_instance(tokens))

        sentences = []
        predictions = []
        with torch.no_grad():
            self.model.eval()
            iterator = self.iterator(instances, num_epochs=1, shuffle=False)
            for batch in iterator:
                nn_util.move_to_device(batch, self.cuda_device)
                output = self.model(**batch)
                sentences.extend(batch["sentence"]["tokens"])
                predictions.extend(output.get("tag_logits"))

        final_output = []
        inferrer = CombinedFrameInferrer(*inferrers)

        for sent, pred in zip(sentences, predictions):
            tokens = [self.vocab.get_token_from_index(idx, "tokens") for idx in sent.tolist()]
            # TODO: change _tensors_to_labels() so that you can run it without gold_tensors
            _, labels, n_best_labels, _, _ = self._tensors_to_labels(pred, pred, multilabel_threshold=0.6,
                                                                     label_namespace="labels")

            inferred_frames = inferrer.infer(tokens, labels, n_best_labels, typical_frames)
            if n_best_labels is None:
                n_best_labels = ["_" for _ in tokens]
            final_output.append(
                [
                    {"token": token, "label": label, "n_best": n_best, "inferred": inferred}
                    for token, label, n_best, inferred in zip(tokens, labels, n_best_labels, inferred_frames)
                ]
            )
        return final_output

    def load_trained_model(self):
        # load parameters to the correct device
        device = "cpu" if self.cuda_device == -1 else f"cuda:{self.cuda_device}"
        model_file = self.file_names.model_name.replace(".test.th", ".th")
        state_dict = torch.load(model_file, map_location=device)
        # dealing with models trained before code changes of 2020-04-08
        self.update_state_dict_keys_(state_dict)
        self.model.load_state_dict(state_dict)

    @staticmethod
    def update_state_dict_keys_(state_dict):
        for key in list(state_dict.keys()):
            if key.startswith("layers"):
                new_key = key.replace("layers", "decoder.layers")
                state_dict[new_key] = state_dict[key]
                del state_dict[key]
            if key.startswith("hidden2tag"):
                new_key = key.replace("hidden2tag", "decoder.layers.0")
                state_dict[new_key] = state_dict[key]
                del state_dict[key]

    def evaluate_external_predictions(self, dev=True):
        """
        Evaluate Open-SESAME or simpleframeid as if it was the model specified in the given configuration
        :param dev:
        :return:
        """

        assert self.external_dev_predictions is not None, "Make sure to set load_sesame_dev=True"

        csv_file = f"output/external_evaluation/{self.external_dev_name}-end-to-end.csv"
        external_predictions = self.external_dev_predictions
        evaluation_set = self.dev_set
        if not dev:
            # assert self.external_dev_name == "sesame", "TEST only supported for SESAME right now"
            csv_file = csv_file.replace(".csv", ".test.csv")
            external_predictions_file = self.external_prediction_file
            external_predictions, = self._load_datasets([external_predictions_file])

            if self.config[cns.LANGUAGE] == cns.Languages.ENGLISH:
                test_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, "fn1.7.test.syntaxnet.txt")
            elif self.config[cns.LANGUAGE] == cns.Languages.DUTCH:
                test_set_file = os.path.join(cns.MULTILABEL_FN_CORPUS, f"dutch-sonar-test-{self.sonar_annotator}.txt")
            else:
                raise ValueError("Invalid language choice!")
            test_set, = self._load_datasets([test_set_file], freeze_binarizer=True)
            evaluation_set = test_set

        with open(csv_file, "w", encoding="utf-8") as f:
            writer = csv.DictWriter(f, fieldnames=["token",
                                                   "prediction_with_idxs",
                                                   "prediction",
                                                   "gold_with_idxs",
                                                   "gold"])
            writer.writeheader()

            for gold_sent, sesame_sent in zip(evaluation_set, external_predictions):

                if len(gold_sent["sentence"]) != len(sesame_sent["sentence"]):
                    print("Can't align:")
                    print(gold_sent)
                    print(sesame_sent)
                    print("-----------")
                    continue

                for i in range(len(sesame_sent["sentence"])):
                    token = sesame_sent["sentence"][i].text
                    prediction_with_idxs = sesame_sent["labels"].labels[i]
                    prediction_with_idxs_str = "|".join(prediction_with_idxs) or "_"
                    prediction = [tag.split("@")[0] for tag in prediction_with_idxs]
                    prediction_str = "|".join(prediction) or "_"

                    gold_with_idxs = gold_sent["labels"].labels[i]
                    gold_with_idxs_str = "|".join(gold_with_idxs) or "_"
                    gold = [tag.split("@")[0] for tag in gold_with_idxs]
                    gold_str = "|".join(gold) or "_"

                    writer.writerow({
                        "token": token,
                        "prediction_with_idxs": prediction_with_idxs_str,
                        "prediction": prediction_str,
                        "gold_with_idxs": gold_with_idxs_str,
                        "gold": gold_str
                    })
            f.write(os.linesep)

        self.do_score_and_error_analysis(csv_file)

    @staticmethod
    def do_score_and_error_analysis(csv_file):
        scores = fn_accuracy_score(prediction_file=csv_file)
        print("Analyzing frame errors (surface level)...")
        surface_error_df = analyze_errors(ErrorAnalysisLevel.FRAMES, prediction_file=csv_file)
        print("Analyzing frame errors (deep level)...")
        deep_error_df = analyze_errors(ErrorAnalysisLevel.DEEP_FRAMES, prediction_file=csv_file)
        print("Analyzing frame errors (token level)...")
        token_error_df = analyze_errors(ErrorAnalysisLevel.TOKENS, prediction_file=csv_file)
        surface_error_df.to_csv(os.path.join("output/error_analysis/surface_frames", os.path.basename(csv_file)))
        deep_error_df.to_csv(os.path.join("output/error_analysis/deep_frames", os.path.basename(csv_file)))
        token_error_df.to_csv(os.path.join("output/error_analysis/tokens", os.path.basename(csv_file)))
        return scores

    def _evaluate_pca(self, label_array):
        transformed = self.label_models.pca_model.transform(label_array)
        reconstructed = (self.label_models.pca_model.inverse_transform(transformed) >= 0.5).astype(int)
        recall = reconstructed[label_array.nonzero()].sum() / label_array.sum()
        precision = label_array[reconstructed.nonzero()].sum() / reconstructed.sum()
        return recall, precision

    def _get_label_array(self, dataset, label_namespace):
        label_tensors = [td[label_namespace] for td in self.iterator(dataset, num_epochs=1, shuffle=False)]
        label_array = np.vstack([t.reshape(-1, t.shape[-1]).numpy() for t in label_tensors])
        return label_array

    def _write_prediction_csv_files(self,
                                    sentences: List[torch.Tensor],
                                    predictions: List[torch.Tensor],
                                    golds: List[torch.Tensor],
                                    target_forms: Optional[List[str]] = None,
                                    label_namespace: str = "labels",
                                    output_file_postfix: Optional[str] = None,
                                    split_frame_contexts=None) -> List[str]:
        print("Writing predictions for dev set ...")

        if self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE or \
                self.config[cns.MULTILABEL_SETTING_CONFIG] in [cns.MultilabelSetting.BINARIZED,
                                                               cns.MultilabelSetting.BINARIZED_PCA]:
            if self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE \
                    and not self.config[cns.BINARY_MARKER_IN_FRAME_EMBEDDING]:
                # thresholds = [0.1, 0.2, 0.3, 0.4, 0.5]
                thresholds = [0.5, 0.6, 0.7, 0.8, 0.9]
            else:
                thresholds = [0.2, 0.3]
                # thresholds = [0.1, 0.2, 0.3, 0.4, 0.5]
                # thresholds = [0.4, 0.5]
        else:
            thresholds = [None]

        output_files = []
        for threshold in thresholds:
            print(f"Threshold={threshold}")
            output_file = self._get_csv_output_name(len(sentences), threshold)
            if output_file_postfix:
                output_file = output_file.replace(".csv", f".{output_file_postfix}.csv")
            if label_namespace != "labels":
                output_file = output_file.replace(".csv", f".{label_namespace}.csv")

            with open(output_file, "w", encoding="utf-8") as f:
                field_names = ["token", "prediction", "prediction_n_best", "prob_score", "gold"]
                if split_frame_contexts is not None:
                    field_names.append("frame_context")

                writer = csv.DictWriter(f, field_names)
                writer.writeheader()

                if split_frame_contexts is None:
                    split_frame_contexts = [None for _ in sentences]

                for i, (sentence, prediction, gold, frame_ct) in enumerate(zip(sentences,
                                                                               predictions,
                                                                               golds,
                                                                               split_frame_contexts)):

                    if i % 10 == 0:
                        print(f"{i}...", end="")

                    # reconstruct tokens and labels from tensors
                    tokens = [self.vocab.get_token_from_index(idx, "tokens") for idx in sentence.tolist()]

                    gold_labels, labels, n_best_labels, _, frame_ct_label = self._tensors_to_labels(gold,
                                                                                                    prediction,
                                                                                                    threshold,
                                                                                                    label_namespace,
                                                                                                    frame_ct=frame_ct)
                    if not n_best_labels:
                        n_best_labels = [None for _ in tokens]

                    # TODO: re-add prob scores (important for some frame embedding settings)
                    word_buffer = ""  # reconstruct word tokens from byte pairs
                    prev_word = ""
                    for token, label, n_best, gold_label in zip(tokens, labels, n_best_labels, gold_labels):

                        if token.startswith("##"):
                            word_buffer += token[2:]
                        else:
                            prev_word = word_buffer
                            word_buffer = token

                        # delete all labels not related to the target word form
                        if target_forms is not None:
                            if not (word_buffer in target_forms or f"{prev_word} {word_buffer}" in target_forms):
                                label = gold_label = "_"

                        row = {
                            "token": token,
                            "prediction": label,
                            "prediction_n_best": n_best if n_best else "_",
                            "gold": gold_label
                        }
                        if frame_ct_label is not None:
                            row["frame_context"] = frame_ct_label[0]
                        writer.writerow(row)
                    f.write("\n")
                print()
            output_files.append(output_file)
        return output_files

    def _get_csv_output_name(self, num_sentences, threshold):
        output_file = f"output/predictions/{self.file_names.basename}"
        if self.dev_set_label:
            output_file += f"_{self.dev_set_label}"
        output_file += f"_dev{num_sentences}"
        if threshold:
            output_file += f"_{threshold}"
        output_file += ".csv"
        return output_file

    def _tensors_to_labels(self,
                           gold_tensors,
                           prediction_tensors,
                           multilabel_threshold,
                           label_namespace,
                           frame_ct=None
                           ):

        # only for decoding embeddings
        n_best_labels = None
        prob_scores = None
        frame_ct_labels = None

        # multi-task, frame part
        if label_namespace == "frame_labels":
            if self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE:
                labels, n_best_labels, prob_scores = self._embeddings_to_labels(prediction_tensors,
                                                                                multilabel_threshold)
                gold_labels, _, _ = self._embeddings_to_labels(gold_tensors)

            else:
                labels = [self.vocab.get_token_from_index(int(np.argmax(pred)), label_namespace)
                          for pred in prediction_tensors.tolist()]
                gold_labels = [self.vocab.get_token_from_index(int(i), label_namespace) for i in gold_tensors]

        # multi-task, role part
        elif label_namespace == "role_labels":
            if self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED_PCA:
                prediction_tensors = torch.tensor(
                    self.label_models.pca_model.inverse_transform(prediction_tensors)
                )

                labels, prob_scores = self._get_multilabel_predictions(prediction_tensors,
                                                                       label_namespace,
                                                                       threshold=multilabel_threshold,
                                                                       sigmoid=False
                                                                       )

                gold_labels, _ = self._get_multilabel_predictions(gold_tensors,
                                                                  label_namespace,
                                                                  threshold=multilabel_threshold,
                                                                  sigmoid=False
                                                                  )

            elif self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED:
                labels, prob_scores = self._get_multilabel_predictions(prediction_tensors,
                                                                       label_namespace,
                                                                       threshold=multilabel_threshold)

                gold_labels, _ = self._get_multilabel_predictions(gold_tensors,
                                                                  label_namespace,
                                                                  threshold=multilabel_threshold,
                                                                  sigmoid=False
                                                                  )
            else:
                raise ValueError("Illegal settings for role label prediction")

        # single-task, could be frames or roles or mixed
        else:
            if self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED_PCA:
                prediction_tensors = torch.tensor(
                    self.label_models.pca_model.inverse_transform(prediction_tensors)
                )

                labels, prob_scores = self._get_multilabel_predictions(prediction_tensors,
                                                                       label_namespace,
                                                                       threshold=multilabel_threshold,
                                                                       sigmoid=False
                                                                       )

                gold_labels, _ = self._get_multilabel_predictions(gold_tensors,
                                                                  label_namespace,
                                                                  threshold=multilabel_threshold,
                                                                  sigmoid=False
                                                                  )

            elif self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED:
                labels, prob_scores = self._get_multilabel_predictions(prediction_tensors,
                                                                       label_namespace,
                                                                       threshold=multilabel_threshold)

                gold_labels, _ = self._get_multilabel_predictions(gold_tensors,
                                                                  label_namespace,
                                                                  threshold=multilabel_threshold,
                                                                  sigmoid=False
                                                                  )

            elif self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE:
                labels, n_best_labels, prob_scores = self._embeddings_to_labels(prediction_tensors,
                                                                                multilabel_threshold)
                gold_labels, _, _ = self._embeddings_to_labels(gold_tensors)

            else:
                labels = [self.vocab.get_token_from_index(int(np.argmax(pred)), label_namespace)
                          for pred in prediction_tensors.tolist()]

                # TODO: make sure works both for webapp and regular script
                # gold_labels = [self.vocab.get_token_from_index(int(np.argmax(pred)), label_namespace)
                #                for pred in gold_tensors.tolist()]
                gold_labels = [self.vocab.get_token_from_index(int(i), label_namespace) for i in gold_tensors]

        if frame_ct is not None:
            if self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE:
                frame_ct_labels, _, _ = self._embeddings_to_labels(frame_ct,
                                                                   threshold=0.1,
                                                                   binary_marker_in_embedding=False)
            else:
                frame_ct_labels = self._get_sparse_frame_context_prediction(frame_ct)

        return gold_labels, labels, n_best_labels, prob_scores, frame_ct_labels

    def _get_multilabel_predictions(self,
                                    prediction,
                                    label_namespace,
                                    threshold=0.2,
                                    sigmoid=True
                                    ) -> Tuple[List[str], Optional[List[float]]]:
        if sigmoid:
            prediction = torch.sigmoid(prediction)

        predicted_labels: List[List[str]] = [[] for _ in range(len(prediction))]
        # probability_scores: List[float] = []

        # get all predictions with a positive probability
        for coord in torch.nonzero(prediction > threshold):
            label = self.vocab.get_token_from_index(int(coord[1]), label_namespace)
            predicted_labels[coord[0]].append(f"{label}:{prediction[coord[0], coord[1]]:.3f}")
            # probability_scores.append(float(prediction[coord[0], coord[1]]))

        str_predictions: List[str] = []
        for label_list in predicted_labels:
            str_predictions.append("|".join(label_list) or "_")

        return str_predictions, None

    @staticmethod
    def _get_sparse_frame_context_prediction(prediction):
        predicted_labels: List[List[str]] = [[] for _ in range(len(prediction))]

        # get all predictions with a positive probability
        for coord in torch.nonzero(prediction > 0):
            label = FrameContextField.ID_TO_FRAME[int(coord[1])]
            predicted_labels[coord[0]].append(f"T:{label}:{prediction[coord[0], coord[1]]:.3f}")
        return predicted_labels[0]

    def _embeddings_to_labels(self,
                              tensors,
                              threshold: float = 0.5,
                              n: int = 10,
                              binary_marker_in_embedding: Optional[bool] = None
                              ) -> Tuple[List[str], List[str], List[float]]:

        assert self.label_models.frame_embedder, "No label embeddings were given!"

        if binary_marker_in_embedding is None:
            binary_marker_in_embedding = self.config[cns.BINARY_MARKER_IN_FRAME_EMBEDDING]

        best_labels = []
        n_best_labels = []
        probability_scores = []

        all_labels = list(self.label_models.frame_embedder.keys())
        all_embeddings = np.array(list(self.label_models.frame_embedder.values()))

        for tensor in tensors:

            if binary_marker_in_embedding:
                embedding = tensor[1:]
            else:
                embedding = tensor

            embedding = embedding.cpu()

            cosines = cosine_similarity(embedding.reshape(1, -1), all_embeddings)[0]
            n_best_labels.append("|".join(reversed(
                [f"{all_labels[i]}:{cosines[i]}" for i in np.argsort(cosines)[-n:]]
            )))

            if binary_marker_in_embedding:
                probability_score = tensor[0]
            else:
                probability_score = np.max(cosines)

            if probability_score < threshold:
                best_labels.append("_")
            else:
                best_idx = int(np.argmax(cosines))
                best_labels.append("T:" + all_labels[best_idx])
            probability_scores.append(probability_score)

        return best_labels, n_best_labels, probability_scores

    def _init_gpu(self) -> Tuple[int, Model]:
        """
        Check if a GPU is available, and if so, move the model to the GPU
        :return: CUDA device ID, GPU version of model
        """
        if torch.cuda.is_available():
            cuda_device = 0
            model = self.model.cuda(cuda_device)
            print("Yay, CUDA is activated!")
        else:
            cuda_device = -1
            model = self.model
        return cuda_device, model

    def _load_datasets(self,
                       datafiles: List[str],
                       freeze_binarizer=False,
                       use_predicted_frames=False,
                       use_predicted_roles=False
                       ) -> List[List[Instance]]:
        """
        Load and represent training and dev data given the current configuration
        :param datafiles: list of data files to load
        :param freeze_binarizer: if True, do not fit the binarizer again
        :return: lists of AllenNLP data instances
        """

        print("Loading datasets")
        reader = self._init_dataset_reader(use_predicted_frames=use_predicted_frames,
                                           use_predicted_roles=use_predicted_roles
                                           )

        # avoid re-fitting binarizer
        if freeze_binarizer:
            reader.freeze_binarizer()

        datasets = [list(reader.read(file)) for file in datafiles]

        return datasets

    def _init_dataset_reader(self,
                             use_predicted_frames: bool = False,
                             use_predicted_roles: bool = False
                             ) -> FrameNetDatasetReader:

        if self.config[cns.MULTILINGUAL_BERT]:
            tokenizer_model = "bert-base-multilingual-cased"
        elif self.config[cns.LANGUAGE] == cns.Languages.DUTCH:
            tokenizer_model = "GroNLP/bert-base-dutch-cased"
        else:
            tokenizer_model = "bert-base-cased"

        reader = FrameNetDatasetReader(
            self.config[cns.MULTILABEL_SETTING_CONFIG],
            self.config[cns.INCLUDE_FRAMES],
            self.config[cns.EMBED_FRAME_LABELS],
            tokenizer_model,
            token_indexers={"tokens": self.token_indexer},
            st_binarizer=self.label_models.st_binarizer,
            mt_frame_binarizer=self.label_models.mt_frame_binarizer,
            mt_role_binarizer=self.label_models.mt_role_binarizer,
            pca_model=self.label_models.pca_model,
            label_embeddings=self.label_models.frame_embedder,
            binary_marker_in_embedding=self.config[cns.BINARY_MARKER_IN_FRAME_EMBEDDING],
            multitask=self.config[cns.MULTITASK],
            add_frame_context_vecs=self.config[cns.ADD_FRAME_CONTEXT_VECS],
            add_roles_context_vecs=self.config[cns.ADD_ROLES_CONTEXT_VECS],
            keep_tag_idxs=True if self.external_dev_name in ["sesame", "lome"] else False,
            split_by_frames=self.config[cns.SPLIT_BY_FRAMES],
            input_frame_predictions=self.config[cns.INPUT_FRAME_PREDICTIONS] if use_predicted_frames else None,
            input_roles_predictions=self.config[cns.INPUT_ROLES_PREDICTIONS] if use_predicted_roles else None,
            combine_roles_context=self.config[cns.COMBINE_ROLES_CONTEXT_VECS],
            infer_extra_frame_predictions=self.config[
                cns.INFER_EXTRA_FRAME_PREDICTIONS] if use_predicted_frames else None,
            use_super_fe=self.config[cns.SUPER_FE]
        )
        return reader

    def _parse_config(self,
                      config_file
                      ) -> Dict[str, Any]:
        """
        Parse a configuration file
        :param config_file: .JSON file; possible key values are specified in constants.py
        :return: dictionary of string keys to values (can be boolean or Enum, specified in constants.py)
        """
        with open(config_file, encoding="utf-8") as f:
            config = json.load(f)

        for setting in cns.SETTING_DEFAULTS:
            if setting in config:
                # convert string values to enum values
                if setting in cns.ENUM_SETTINGS:
                    value = self._get_enum_value(config[setting], cns.ENUM_SETTINGS[setting])
                    config[setting] = value
            # missing settings: use default
            else:
                config[setting] = cns.SETTING_DEFAULTS[setting]

        return config

    @staticmethod
    def _get_enum_value(string: str,
                        enum_class: ClassVar[enum.Enum]
                        ) -> enum.Enum:
        """
        Parse a string value from a config file as an Enum value
        :param string: string value from the config file
        :param enum_class: enum class specified in constants.py
        :return: one of the values from `enum_class`
        """
        for entry in enum_class:
            if string == entry.value:
                return entry
        raise ValueError(f"Invalid value {string} for class {enum_class}")

    def _init_single_task_model(self) -> BertFnTagger:
        """
        Initialize a single-task model given the current configuration
        :return: tagging model
        """
        bert_embedder = self._init_embedder()

        output_size = self._get_decoder_output_size()

        roles_context_vec_size = \
            len(self.label_models.mt_role_binarizer.classes_) \
                if self.label_models.mt_role_binarizer is not None else 0

        if self.label_models.mt_role_binarizer is not None:
            print(f"self.label_models.mt_role_binarizer.classes_={self.label_models.mt_role_binarizer.classes_}")
            print(f"roles_context_vec_size={roles_context_vec_size}")

        decoder = FrameNetDecoder(decoder_output_size=output_size,
                                  multilabel_setting=self.config[cns.MULTILABEL_SETTING_CONFIG],
                                  model_type=self.config[cns.DECODER], hidden_size=self.config[cns.HIDDEN_SIZE],
                                  embed_labels=self.config[cns.EMBED_FRAME_LABELS],
                                  binary_marker_in_embedding=self.config[cns.BINARY_MARKER_IN_FRAME_EMBEDDING],
                                  cosine_loss=self.config[cns.COSINE_LOSS],
                                  add_frame_context_vecs=self.config[cns.ADD_FRAME_CONTEXT_VECS],
                                  frame_context_vec_size=self._get_frame_context_size(),
                                  add_roles_context_vecs=self.config[cns.ADD_ROLES_CONTEXT_VECS],
                                  roles_context_vec_size=roles_context_vec_size,
                                  frame_context_net=self.config[cns.FRAME_CONTEXT_NET])
        return BertFnTagger(bert_embedder, self.vocab, decoder)

    def _init_embedder(self):
        if self.config[cns.RUN_WITHOUT_BERT]:

            print("Vocab size (tokens)", self.vocab.get_vocab_size("tokens"))
            bert_vocab_size = 28996
            bert_embedder = Embedding(num_embeddings=bert_vocab_size, embedding_dim=768)

        else:
            if self.config[cns.MULTILINGUAL_BERT]:
                bert_embedder = PretrainedBertEmbedder("bert-base-multilingual-cased",
                                                       requires_grad=self.config[cns.FINE_TUNE_BERT_CONFIG],
                                                       top_layer_only=self.config[cns.USE_ONLY_TOP_BERT_LAYER],
                                                       scalar_mix_parameters=self.config[cns.BERT_SCALAR_MIX]
                                                       )
            elif self.config[cns.LANGUAGE] == cns.Languages.DUTCH:
                # NB must be on Peregrine!
                bert_embedder = PretrainedTransformerEmbedder("GroNLP/bert-base-dutch-cased")
            else:
                bert_embedder = PretrainedBertEmbedder("bert-base-cased",
                                                       requires_grad=self.config[cns.FINE_TUNE_BERT_CONFIG],
                                                       top_layer_only=self.config[cns.USE_ONLY_TOP_BERT_LAYER],
                                                       scalar_mix_parameters=self.config[cns.BERT_SCALAR_MIX]
                                                       )
            print("Loaded scalar mix", bert_embedder._scalar_mix)
        return bert_embedder

    def _init_multitask_model(self) -> BertFnMultiTaskLabeler:

        bert_embedder = self._init_embedder()

        frame_output_size = self.vocab.get_vocab_size("frame_labels")
        decoder_f = FrameNetDecoder(decoder_output_size=frame_output_size,
                                    multilabel_setting=cns.MultilabelSetting.ONLY_FIRST,
                                    model_type=self.config[cns.DECODER], hidden_size=self.config[cns.HIDDEN_SIZE],
                                    embed_labels=self.config[cns.EMBED_FRAME_LABELS],
                                    binary_marker_in_embedding=self.config[cns.BINARY_MARKER_IN_FRAME_EMBEDDING],
                                    cosine_loss=self.config[cns.COSINE_LOSS], name="frame_decoder", multitask="frames")

        role_output_size = self.vocab.get_vocab_size("role_labels")
        decoder_r = FrameNetDecoder(decoder_output_size=role_output_size,
                                    multilabel_setting=self.config[cns.MULTILABEL_SETTING_CONFIG],
                                    model_type=self.config[cns.DECODER],
                                    hidden_size=self.config[cns.HIDDEN_SIZE],
                                    embed_labels=False,
                                    binary_marker_in_embedding=False,
                                    cosine_loss=False,
                                    name="role_decoder",
                                    multitask="roles",
                                    add_frame_context_vecs=self.config[cns.ADD_FRAME_CONTEXT_VECS],
                                    frame_context_vec_size=self._get_frame_context_size(),
                                    frame_context_net=self.config[cns.FRAME_CONTEXT_NET]
                                    )

        return BertFnMultiTaskLabeler(self.vocab,
                                      bert_embedder,
                                      decoder_f,
                                      decoder_r,
                                      self.config[cns.ROLE_LOSS_WEIGHT],
                                      self.config[cns.FRAME_LOSS_WEIGHT])

    def _get_decoder_output_size(self) -> int:
        """
        Get output size for single-task decoder given the current configuration
        :return: number of output dimensions
        """
        if self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.BINARIZED_PCA:
            output_size = self.config[cns.LABEL_EMBEDDING_DIMENSIONS]
        elif self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE and \
                self.config[cns.MULTILABEL_SETTING_CONFIG] == cns.MultilabelSetting.ONLY_FIRST:
            output_size = self.config[cns.LABEL_EMBEDDING_DIMENSIONS]
            if self.config[cns.BINARY_MARKER_IN_FRAME_EMBEDDING]:
                output_size += 1
        else:
            output_size = self.vocab.get_vocab_size("labels")
        return output_size

    @staticmethod
    def _combine_multitask_labels(frame_labels: Optional[List[str]],
                                  role_labels: Optional[List[str]]
                                  ) -> Optional[List[str]]:
        if not (frame_labels or role_labels):
            return None

        if not frame_labels:
            return role_labels

        if not role_labels:
            return frame_labels

        combined_labels = []
        for fl, rl in zip(frame_labels, role_labels):
            if fl in ["_", "X"]:
                fl = []
            else:
                fl = fl.split("|")
            if rl in ["_", "X"]:
                rl = []
            else:
                rl = rl.split("|")
            combined_labels.append("|".join(fl + rl))

        return combined_labels

    def _get_frame_context_size(self):
        if self.config[cns.EMBED_FRAME_LABELS] != cns.FrameLabelEmbeddingSetting.NONE:
            return self.config[cns.LABEL_EMBEDDING_DIMENSIONS]
        else:
            return len(FrameContextField.FRAMES_TO_ID)


def main():
    argp = argparse.ArgumentParser()
    argp.add_argument("command", choices=["train", "evaluate", "score_fchecks_grid"])
    argp.add_argument("config")
    argp.add_argument("--test", dest="use_dev_mode", action="store_false")
    argp.add_argument("--eval_300", dest="evaluate300", action="store_true")  # TODO: find out what this was meant for!
    argp.add_argument("--eval_external_model", choices=["sesame", "simpleframeid", "lome", "none"], default="none")
    argp.add_argument("--external_predictions", default="none")
    argp.add_argument("--eval_exemplars", dest="evaluate_exemplars", action="store_true")
    argp.add_argument("--eval_hartmann", dest="evaluate_hartmann", action="store_true")
    argp.add_argument("--eval_tatoeba", dest="evaluate_tatoeba", action="store_true")
    argp.add_argument("--clip_exemplars", dest="clip_exemplars", action="store_true")
    argp.add_argument("--external_data", default=None)
    argp.add_argument("--eval_predicate", default=None)
    argp.add_argument("--dev_option", default=None)
    argp.add_argument("--predicate_morph_file", default=None)
    args = argp.parse_args()

    print(args)

    if args.command == "train":
        pipeline = FnSeqLabelingPipeline(args.config)
        pipeline.train()
    elif args.command == "evaluate":
        if args.eval_external_model != "none":
            eval_external_model = args.eval_external_model
        else:
            eval_external_model = None

        if args.external_predictions != "none":
            external_prediction_file = args.external_predictions
        else:
            external_prediction_file = None

        if args.evaluate300:
            with open("data/fn1.7/fn1.7.gold2sesame_mapping.json", encoding="utf-8") as f:
                filter_idx = [int(idx) for idx in json.load(f).keys()]
        else:
            filter_idx = None
        if args.evaluate_hartmann:
            load_hartmann_dev = True
        else:
            load_hartmann_dev = False
        if args.evaluate_tatoeba:
            load_tatoeba_dev = True
        else:
            load_tatoeba_dev = False

        pipeline = FnSeqLabelingPipeline(args.config, eval_external_model, external_prediction_file,
                                         load_hartmann_dev, load_tatoeba_dev, filter_idx)

        if args.external_data:
            external_data_name = os.path.splitext(os.path.basename(args.external_data))[0]
            pipeline.file_names.basename += f"_ext_{external_data_name}"

        if args.eval_predicate:
            pipeline.file_names.basename += f"_pred_{args.eval_predicate.replace('.', '_')}"
            print(pipeline.file_names.basename)

        if eval_external_model is not None:
            print(f"Evaluating external model: {eval_external_model}")
            pipeline.evaluate_external_predictions(args.use_dev_mode)

        elif args.evaluate_exemplars:
            pipeline.evaluate_exemplars(clipped=args.clip_exemplars)

        else:
            pipeline.evaluate(args.use_dev_mode,
                              args.external_data,
                              args.eval_predicate,
                              args.predicate_morph_file
                              )

    elif args.command == "score_fchecks_grid":
        evaluate_fchecks_grid(args.eval_predicate, args.dev_option)


if __name__ == '__main__':
    main()
