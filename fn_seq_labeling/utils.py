import re
from typing import Iterable, Dict, Any, Tuple, List, Optional

import pandas as pd

import torch
from allennlp.common import Tqdm
from allennlp.common.checks import check_for_gpu, logger
from allennlp.data import Instance, DataIterator
from allennlp.models import Model
from allennlp.nn import util as nn_util
from allennlp.training.util import HasBeenWarned


# adapted form `evaluate` in allennlp.training.util.py
def evaluate_and_predict(model: Model,
                         instances: Iterable[Instance],
                         data_iterator: DataIterator,
                         cuda_device: int,
                         batch_weight_key: str = "",
                         multitask: Optional[str] = None
                         ) -> Tuple[Dict[str, Any], List, List, List, List]:

    predictions = []
    golds = []
    sentences = []
    frame_contexts = []

    check_for_gpu(cuda_device)
    with torch.no_grad():
        model.eval()

        iterator = data_iterator(instances,
                                 num_epochs=1,
                                 shuffle=False)
        logger.info("Iterating over dataset")
        generator_tqdm = Tqdm.tqdm(iterator, total=data_iterator.get_num_batches(instances))

        # Number of batches in instances.
        batch_count = 0
        # Number of batches where the model produces a loss.
        loss_count = 0
        # Cumulative weighted loss
        total_loss = 0.0
        # Cumulative weight across all batches.
        total_weight = 0.0

        for batch_idx, batch in enumerate(generator_tqdm):

            batch_count += 1
            batch = nn_util.move_to_device(batch, cuda_device)
            output_dict = model(**batch, use_original_labels=True)
            loss = output_dict.get("loss")

            # keep track of sentences & predicted and gold outputs
            sentences.extend(batch["sentence"]["tokens"])

            if "frame_context_vector" in batch:
                frame_contexts.extend(batch["frame_context_vector"])

            if multitask == "frames":
                tag_logits_key = "frame_tag_logits"
            elif multitask == "roles":
                tag_logits_key = "role_tag_logits"
            # single-task
            else:
                tag_logits_key = "tag_logits"
            batch_predictions = output_dict.get(tag_logits_key)
            predictions.extend(batch_predictions)

            if multitask == "frames":
                labels_key = "original_frame_labels"
            elif multitask == "roles":
                labels_key = "original_role_labels"
            else:
                labels_key = "original_labels"
            golds.extend(batch[labels_key])

            metrics = model.get_metrics()

            if loss is not None:
                loss_count += 1
                if batch_weight_key:
                    weight = output_dict[batch_weight_key].item()
                else:
                    weight = 1.0

                total_weight += weight
                total_loss += loss.item() * weight
                # Report the average loss so far.
                metrics["loss"] = total_loss / total_weight

            if (not HasBeenWarned.tqdm_ignores_underscores and
                    any(metric_name.startswith("_") for metric_name in metrics)):
                logger.warning("Metrics with names beginning with \"_\" will "
                               "not be logged to the tqdm progress bar.")
                HasBeenWarned.tqdm_ignores_underscores = True
            description = ', '.join(["%s: %.2f" % (name, value) for name, value
                                     in metrics.items() if not name.startswith("_")]) + " ||"
            generator_tqdm.set_description(description, refresh=False)

        final_metrics = model.get_metrics(reset=True)
        if loss_count > 0:
            # Sanity check
            if loss_count != batch_count:
                raise RuntimeError("The model you are trying to evaluate only sometimes " +
                                   "produced a loss!")
            final_metrics["loss"] = total_loss / total_weight

        return final_metrics, sentences, predictions, golds, frame_contexts


# copied from utils.py in project frameid-checks
def get_lemmas_to_forms(forms_df, remove_group_names=False):
    lemmas_to_forms = {}
    for _, row in forms_df.iterrows():
        lemma = row["predicate"]
        forms = [row[f"f{col + 1:02}"] for col in range(12)]
        forms = [form for form in forms if not pd.isna(form)]
        if remove_group_names:
            lemma_name = strip_lemma_group_name(lemma)
        else:
            lemma_name = lemma
        lemmas_to_forms[lemma_name] = forms
    return lemmas_to_forms


def strip_lemma_group_name(lemma_with_group):
    lemma = re.sub(r"^(.+\.\w)_\w+$", r"\1", lemma_with_group)
    return lemma
