import abc
import collections
import os
from typing import Set, Tuple, List, Optional, Dict

import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

from nltk.corpus import framenet as fn
from nltk.corpus import stopwords as nltk_stopwords

from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import KeyedVectors
from gensim.models.keyedvectors import Word2VecKeyedVectors

from fn_seq_labeling import constants as cns


def main():
    frame_names = {f.name for f in fn.frames()}
    analyze_frames(frame_names)
    len_to_count = count_frame_name_words(frame_names)
    print(len_to_count)

    glove = load_glove_model(limit=None)
    # glove = None

    embedders = [BagOfLexUnitsEmbedder(frame_names, glove=glove), FrameNameEmbedder(frame_names, glove=glove)]

    for embedder in embedders:
        vectors, vocab, word_lists = embedder.embed()
        print(vectors.shape)
        print(vocab[:10])

        # PCA visualization
        pca = PCA(n_components=2)
        vectors_2d = pca.fit_transform(vectors)
        plt.figure(figsize=(100, 100))
        for i, (x, y) in enumerate(vectors_2d):
            plt.scatter(x, y)
            plt.annotate(vocab[i], (x, y))
        plt.savefig(f"output/figures/{embedder.get_name()}.png")

        # correlation analysis
        correlations = np.corrcoef(vectors)
        sorted_idx = np.argsort(correlations, axis=1)

        with open(f"output/embedding_analysis/{embedder.get_name()}.txt", "w", encoding="utf-8") as f:
            for row in sorted_idx:
                sorted_frames = [vocab[j] for j in row[::-1]]
                f.write(" ".join(sorted_frames) + os.linesep)


class FrameEmbedder(abc.ABC):

    def __init__(self, frames: Set[str], embedding_file: str, glove: Optional[Word2VecKeyedVectors]):
        self.embedding_file = embedding_file
        self.frames = frames or {f.name for f in fn.frames()}
        self.glove = glove

        self.vectors: List[np.ndarray] = []
        self.frame_vocab: List[str] = []
        self.word_vocab: List[Tuple[str, ...]] = []

    def embed(self) -> Tuple[np.ndarray, List[str], List[Tuple[str, ...]]]:

        if os.path.exists(self.embedding_file):
            self._load_embeddings()
        else:
            if not self.glove:
                print("Loading GloVe model ...")
                self.glove = load_glove_model()
            print("Computing embeddings ...")
            self.compute_embeddings()
            print(f"Saving embeddings to {cns.FRAME_NAME_EMBEDDING_FILE}")
            self._save_embeddings()
        return np.array(self.vectors), self.frame_vocab, self.word_vocab

    def embed_as_dict(self) -> Dict[str, np.ndarray]:
        vectors, vocab, _ = self.embed()
        return {frame: vec for vec, frame in zip(vectors, vocab)}

    def compute_embeddings(self):

        for frame in self.frames:
            frame_vectors, frame_words = self.embed_frame(frame)

            if len(frame_vectors) == 0:
                print(f"WARNING: no vectors found for frame {frame}, skipping")
            else:
                self.vectors.append(frame_vectors.mean(axis=0))
                self.frame_vocab.append(frame)
                self.word_vocab.append(tuple(sorted(frame_words)))

    @abc.abstractmethod
    def embed_frame(self, frame: str) -> Tuple[np.ndarray, List[str]]:
        pass

    def _save_embeddings(self):
        # txt version (frame + components + vector)
        # & tsv version (for TensorFlow Projector, separate files for vectors + metadata)
        with \
                open(self.embedding_file, "w", encoding="utf-8") as f_txt, \
                open(self.embedding_file.replace(".txt", ".tsv"), "w", encoding="utf-8") as f_tf, \
                open(self.embedding_file.replace(".txt", ".meta.tsv"), "w", encoding="utf-8") as f_tf_meta:

            # write a row for every frame vector
            for vec, frame, words in zip(self.vectors, self.frame_vocab, self.word_vocab):
                f_txt.write(f"{frame} {'+'.join(words)} {' '.join(str(i) for i in vec)}\n")
                f_tf.write("\t".join(str(i) for i in vec) + os.linesep)
                f_tf_meta.write(frame + os.linesep)

    def _load_embeddings(self):
        # assert len(self.vectors) == 0, "Embeddings are already loaded!"
        # in case vectors are already loaded: don't do anything
        if len(self.vectors) != 0:
            return

        with open(self.embedding_file, encoding="utf-8") as f:
            for line in f:
                columns = line.split()
                frame = columns[0]
                words = tuple(columns[1].split("+"))
                vector = np.array([float(i) for i in columns[2:]])

                self.frame_vocab.append(frame)
                self.word_vocab.append(words)
                self.vectors.append(vector)

    @abc.abstractmethod
    def get_name(self) -> str:
        pass


class BagOfLexUnitsEmbedder(FrameEmbedder):

    def __init__(self, frames: Set[str] = None, glove: Optional[Word2VecKeyedVectors] = None):
        super().__init__(frames, cns.BAG_OF_LU_EMBEDDING_FILE, glove)

    def embed_frame(self, frame: str) -> Tuple[np.ndarray, List[str]]:

        frame_vectors = []
        frame_vocab = []

        frame_object = fn.frame(frame)
        lex_units = frame_object.lexUnit
        for lu in lex_units:
            lu_name, lu_pos = lu.split(".")
            if lu_name not in self.glove:
                print(f"WARNING: LU {lu} in frame {frame} not in GloVe model, skipping")
            else:
                vector = self.glove[lu_name]
                frame_vectors.append(vector)
                frame_vocab.append(lu_name)

        return np.array(frame_vectors), frame_vocab

    def get_name(self) -> str:
        return "bag_of_lus"


class FrameNameEmbedder(FrameEmbedder):

    def __init__(self, frames: Set[str] = None, glove: Optional[Word2VecKeyedVectors] = None):
        super().__init__(frames, cns.FRAME_NAME_EMBEDDING_FILE, glove)

    def embed_frame(self, frame: str) -> Tuple[np.ndarray, List[str]]:

        frame_vectors = []
        frame_words = []
        frame_name_words = [w.lower() for w in frame.split("_")]

        for w in frame_name_words:
            if w in nltk_stopwords.words("english"):
                print(f"INFO: word {w} in frame {frame} is a stopword, skipping")
            elif w not in self.glove:
                print(f"WARNING: word {w} in frame {frame} not in GloVe model, skipping")
            else:
                vector = self.glove[w]
                frame_vectors.append(vector)
                frame_words.append(w)
        return np.array(frame_vectors), frame_words

    def get_name(self) -> str:
        return "frame_names"


def count_frame_name_words(frames):
    len_to_count = collections.defaultdict(int)
    for frame in frames:
        num_words = len(frame.split("_"))
        len_to_count[num_words] += 1
    return len_to_count


def load_glove_model(limit: Optional[int] = None):
    glove_file = "data/embeddings/glove.42B.300d.txt"
    glove_file_w2v_format = "data/embeddings/glove.42B.300d.w2v.txt"

    if not os.path.exists(glove_file_w2v_format):
        print("Converting GloVe file to w2v format for use with gensim ...")
        glove2word2vec(glove_file, glove_file_w2v_format)

    print("Loading GloVe vectors ...")
    return KeyedVectors.load_word2vec_format(glove_file_w2v_format, limit=limit)


def get_frame_names_in_data() -> Set[str]:
    frames = set()
    with open("data/fn1.7/fn1.7.fulltext.train.syntaxnet.vocab.txt", encoding="utf-8") as f:
        for line in f:
            if line.startswith("T:"):
                frames.add(line.strip().split(":")[1])
    return frames


def analyze_frames(frames):
    non_lexical_frames = {f.name for f in fn.frames() if not f.lexUnit}
    lexical_frames = {f for f in frames if f not in non_lexical_frames}
    frames_in_train_data = get_frame_names_in_data()
    print("Frames not in training data", len(frames.difference(frames_in_train_data)))
    print("Non-lexical frames", len(non_lexical_frames))
    missing_frames = lexical_frames.difference(frames_in_train_data)
    print("Lexical frames not in training data", len(missing_frames))
    print(list(missing_frames)[:10])


if __name__ == '__main__':
    main()
