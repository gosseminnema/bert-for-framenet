import json
from typing import Dict, Any, Tuple

import gensim
import pandas as pd
from flask import Flask, render_template, request, jsonify
from nltk.corpus import framenet as fn

from fn_seq_labeling.embed_frames import BagOfLexUnitsEmbedder
from fn_seq_labeling.evaluation.compare_predictions import compare_predictions
from fn_seq_labeling.infer_frames import LikeliestFramesInferrer, Frame2FrameInferrer, FrameEmbeddingInferrer, \
    LexItemToFrameInferrer
from fn_seq_labeling.labeler import FnSeqLabelingPipeline

app = Flask(__name__)


main_page_objects: Dict[str, Any] = {
    "config": "configs/starsem/stripped_framesonly_sparse.rep1.json",
    "labeler": None,
    "embedder": None,
    "glove_model": None
}

comparison_cache: Dict[Tuple[str, str], Dict] = {}


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/model")
def init_model():

    config = request.args.get("config")
    print(f"Changing to config: {config}")
    main_page_objects["config"] = config
    _load_labeler()

    return jsonify({
        "result": "success"
    })


@app.route("/analyze")
def analyze():

    # analyze request
    text = request.args.get("textToAnalyze")
    typical_frames = json.loads(request.args.get("typicalFrames"))
    strategies = json.loads(request.args.get("strategies"))
    print(f"Analyzing: {text}")
    print(typical_frames)
    print(strategies)

    # initialize global objects if needed
    labeler = main_page_objects["labeler"]
    if not labeler:
        labeler = _load_labeler()

    embedder = main_page_objects["embedder"]
    if not embedder:
        embedder = BagOfLexUnitsEmbedder()
        main_page_objects["embedder"] = embedder

    glove_model = main_page_objects["glove_model"]
    if not glove_model:
        print("Loading GloVe model...")
        glove_model = gensim.models.KeyedVectors.load_word2vec_format(
            "data/embeddings/glove.42B.300d.w2v.txt",
            # limit=100_000
            limit=10_000
        )
        main_page_objects["glove_model"] = glove_model
        print("GloVe model loaded!")

    inferrers = []
    for strategy in strategies:
        if strategy == "n_best_frames":
            inferrers.append(LikeliestFramesInferrer())
        elif strategy == "frame_to_frame":
            inferrers.append(Frame2FrameInferrer())
        elif strategy == "frame_embeddings":
            inferrers.append(FrameEmbeddingInferrer(embedder))
        elif strategy == "lex_embeddings":
            inferrers.append(LexItemToFrameInferrer(embedder, glove_model))
        else:
            raise ValueError("Unknown frame inferring strategy")

    print("analyzing...")
    analysis = labeler.predict_raw_text(text, typical_frames=typical_frames, inferrers=inferrers)
    print("done!")
    return jsonify({
        "analysis": analysis
    })


def _load_labeler():
    labeler = FnSeqLabelingPipeline(main_page_objects["config"])
    labeler.load_trained_model()
    main_page_objects["labeler"] = labeler
    return labeler


@app.route("/frames")
def list_frames():
    all_frame_names = []
    frames_to_definitions = {}
    for f in fn.frames():
        all_frame_names.append(f.name)
        frames_to_definitions[f.name] = f.definition
    return jsonify({
        "frames": all_frame_names,
        "framesToDefinitions": frames_to_definitions
    })


@app.route("/errors")
def error_analysis():
    return render_template("errorAnalysis.html")


@app.route("/compare", methods=["POST"])
def compare_files():
    file_1 = request.files.get("csvFile1")
    file_2 = request.files.get("csvFile2")

    cache_key = (file_1.filename, file_2.filename)
    if cache_key in comparison_cache:
        print("Loading comparison from cache!")
        compare_json = comparison_cache[cache_key]

    else:
        pred_df_1 = pd.read_csv(file_1)
        pred_df_2 = pd.read_csv(file_2)
        compare_json, _ = compare_predictions(pred_df_1, pred_df_2)
        # compare_json.sort(key=lambda sent: sent["scores_1"]["f1"])
        compare_json.sort(key=lambda sent: sent["f1_diff"])
        comparison_cache[cache_key] = compare_json
        print("Caching comparison!")

    return jsonify(compare_json)