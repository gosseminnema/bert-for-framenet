(function ($) {

    let possibleFrames;
    let framesToDefinitions;
    let typicalFrames = new Set();

    const strategies = {
        "n_best_frames": "N-best frames",
        "frame_to_frame": "Frame-to-frame relations",
        "frame_embeddings": "Frame embedding similarities",
        "lex_embeddings": "Lexical-unit-to-frame embedding similarities"
    };
    let activeStrategies = new Set();

    function showTypicalFrames() {
        let $typical = $("#typical-frame-list");
        $typical.empty();

        if (typicalFrames.size === 0) {
            $typical.append(`<div class="alert alert-primary">No frames selected!</div>`);
        } else {
            typicalFrames.forEach(function (typicalFrame) {
                $typical.append(
                    $("<li>")
                        .attr("class", "list-group-item list-group-item-secondary")
                        .append(
                            $("<span>")
                                .append($("<h5>").append(typicalFrame))
                                .attr("title", framesToDefinitions[typicalFrame])
                        )
                )
                // $typical.append(`<li class="list-group-item list-group-item-secondary">${typicalFrame}</li>`)
            });

            // for all of the newly added buttons, add an event handler that deletes them when clicked
            $("#typical-frame-list > li").click(function (event) {
                typicalFrames.delete(event.target.textContent);
                showTypicalFrames();
            });
        }
    }

    // when page is ready
    $(function () {

        // Fill possible frame dropdown
        $.get("frames").done(function (data) {

            // Get data from flask app
            possibleFrames = data["frames"];
            framesToDefinitions = data["framesToDefinitions"];
            let $frameOptions = $("#frameOptions");
            possibleFrames.forEach(function (frame) {
                $frameOptions.append(`<option>${frame}</option>`)
            });

            // Set list of typical frames
            ["Killing", "Use_firearm", "Operate_vehicle", "Offenses"].forEach(frame => typicalFrames.add(frame));
            showTypicalFrames();

        });

        // Frame inferring strategies
        Object.keys(strategies).forEach(function (key) {
            const strategyName = strategies[key];
            $("#frame-inferring-strategies").append(
                $("<li>")
                    .attr("class", "list-group-item list-group-item-secondary pl-5")
                    .append($("<input>")
                        .attr("class", "form-check-input")
                        .attr("id", key)
                        .attr("type", "checkbox")
                        .change(function () {
                            if (this.checked) {
                                activeStrategies.add(key);
                            } else {
                                activeStrategies.delete(key);
                            }
                            console.log(activeStrategies);
                        })
                    )
                    .append($("<label>")
                        .attr("class", "form-check-label")
                        .attr("for", key)
                        .append(strategyName)
                    )
            );
        });

    });

    $("#addFrameButton").click(function (event) {
        let newFrame = $("#frameOptions").val();
        typicalFrames.add(newFrame);
        showTypicalFrames();
    });

    $("#clearFrameButton").click(function (event) {
        typicalFrames.clear();
        showTypicalFrames();
    });

    $("#analyzeButton").click(function (event) {

        // show loading button
        $("#analysisSpinner").show();

        // define parameters
        let text = $("#textInput").val();
        let params = {
            textToAnalyze: text,
            typicalFrames: JSON.stringify(Array.from(typicalFrames)),
            strategies: JSON.stringify(Array.from(activeStrategies))
        };

        // call 'server' to analyze text
        $.get("analyze", params).done(function (data) {

            console.log("analysis done!");

            // Add the analysis in the table
            $("#analysis").empty();
            data["analysis"].forEach(function (arr) {
                arr.forEach(function (item) {
                    $("#analysis").append(
                        `<tr>` +
                        `<td>${item["token"]}</td>` +
                        `<td><span title="${item["n_best"]}">${item["label"]}</span></td>` +
                        `<td>${item["inferred"]}</td>` +
                        `</tr>`
                    );
                });
            });

            // Remove the loading button
            $("#analysisSpinner").hide();
        });
    });
})(jQuery);