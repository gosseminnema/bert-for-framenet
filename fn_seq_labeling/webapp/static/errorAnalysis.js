(function ($) {

    // On document load
    $(function () {
        bsCustomFileInput.init();
        $("#analysisSpinner").hide();
        $("#cardTemplate").hide();
    });

    $("#compareButton").click(function (event) {

        let formData = new FormData();
        formData.append("csvFile1", $("#csvFile1Input")[0].files[0]);
        formData.append("csvFile2", $("#csvFile2Input")[0].files[0]);

        $("#analysisSpinner").show();
        let $resultCards = $("#resultCards");
        $resultCards.empty();

        $.ajax({
            url: "compare",
            data: formData,
            type: "POST",
            contentType: false,
            processData: false,
            success: function (data) {

                let f1Diffs = data.map(sentInfo => sentInfo["f1_diff"]);
                Plotly.newPlot("errorDistPlot", [{
                        y: f1Diffs,
                        x: [...f1Diffs.keys()],
                        type: "bar"
                    }], {
                        margin: {t: 0},
                        title: "F1 difference distribution"
                    },
                );

                data.forEach(function (sentInfo) {
                        let $card = $("#cardTemplate").clone().appendTo($resultCards);

                        // Add score summary in header
                        $card.find("p.card-header")
                            .empty()
                            .append($("<code>")
                                .attr("class", "text-primary")
                                .append(`F1_diff=${sentInfo["f1_diff"].toPrecision(2)} `)
                            )
                            .append($("<code>")
                                .attr("class", "text-success")
                                .append(`F1=${sentInfo["scores_1"]["f1"].toPrecision(2)} `)
                                .append(`P=${sentInfo["scores_1"]["prec"].toPrecision(2)} `)
                                .append(`R=${sentInfo["scores_1"]["rec"].toPrecision(2)} `)
                            )

                            .append($("<code>")
                                .attr("class", "text-danger")
                                .append(`F1=${sentInfo["scores_2"]["f1"].toPrecision(2)} `)
                                .append(`P=${sentInfo["scores_2"]["prec"].toPrecision(2)} `)
                                .append(`R=${sentInfo["scores_2"]["rec"].toPrecision(2)} `)
                            );

                        // Show (clipped) sentence string
                        let sentString = sentInfo["annotations"]
                            .map(item => item["token"])
                            .join(" ")
                            .replace("[CLS] ", "")
                            .replace(" [SEP]", "")
                            .replace(" ##", "")
                            .substring(0, 50) + "...";
                        $card.find("p.card-text.text-info")
                            .empty()
                            .append(sentString);

                        // Add (and hide) table
                        let $table = $card.find("table");
                        let $tbody = $table.find("tbody");

                        $table.hide();
                        $tbody.empty();
                        $card.find("a.btn").click(() => $table.toggle());

                        sentInfo["annotations"].forEach(function (item) {

                            let goldLabels = item["gold"].split("|").map(label => label.replace(":1.000", ""));
                            const makeLabelBadges = function (labelsAsString) {
                                let parsedLabels = labelsAsString
                                    .split("|")

                                    // Split label and probability value
                                    .map(labelString => labelString.split(":"))

                                    // Merge "T:" for frame targets
                                    .map(labelArray => labelArray[0] === "T"
                                        ? [labelArray[0] + ":" + labelArray[1], labelArray[3]]
                                        : labelArray
                                    );

                                let badges = parsedLabels
                                    .map(function (labelAndProba) {
                                        const label =
                                            labelAndProba.length > 2
                                                ? labelAndProba[0] + ":" + labelAndProba[1]
                                                : labelAndProba[0];

                                        const proba =
                                            labelAndProba.length >= 2
                                                ? parseFloat(labelAndProba[labelAndProba.length - 1])
                                                : 1.0;
                                        const badgeColor = goldLabels.includes(label)
                                            ? "badge-success"
                                            : "badge-danger";
                                        return $("<span>")
                                            .attr("class", `badge ${badgeColor}`)
                                            .attr("title", `Score: ${proba.toPrecision(2)}`)
                                            .append(label)
                                            ;
                                    });

                                let missingBadges = goldLabels
                                    .filter(label => !(parsedLabels.map(
                                        i => i.length > 2 ? i[0] + ":" + i[1] : i[0]).includes(label))
                                    )
                                    .map(label => $("<span>")
                                        .attr("class", "badge badge-dark")
                                        .append(label)
                                    );

                                return badges.concat(missingBadges);
                            };

                            $tbody
                                .append($("<tr>")
                                    .append($("<td>").append(item["token"]))
                                    .append($("<td>").append(makeLabelBadges(item["pred_1"])))
                                    .append($("<td>").append(makeLabelBadges(item["pred_2"])))
                                    .append($("<td>").append(makeLabelBadges(item["gold"])))
                                )
                        });

                        $card.show();
                    }
                );

                $("#analysisSpinner").hide();
            }
        })

    })

})(jQuery);