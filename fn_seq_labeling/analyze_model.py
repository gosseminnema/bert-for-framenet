import glob
import os

import matplotlib.pyplot as plt
import torch


def main():
    models = glob.glob("output/*.th")

    for m in models:
        try:
            m_name, _ = os.path.splitext(os.path.basename(m))
            print(m_name)

            state_dict = torch.load(m, map_location="cpu")
            scalar_params = torch.cat(
                [state_dict[f"word_embeddings._scalar_mix.scalar_parameters.{i}"] for i in range(12)]
            ).numpy().reshape(1, -1)
            heatmap = plt.matshow(scalar_params)
            plt.colorbar(heatmap)
            plt.savefig(f"output/model_analysis/{m_name}.scalar_params.png")
        except KeyError as e:
            print("KeyError:", e, "skipping this model")


if __name__ == '__main__':
    main()
