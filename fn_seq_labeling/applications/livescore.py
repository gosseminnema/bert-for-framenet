import glob

from fn_seq_labeling.labeler import FnSeqLabelingPipeline

LIVESCORE_DATA_SOURCE = "/mnt/c/Users/Gosse/WorkSyncs/sport-data-scraping/output/positives_texts_frames/"


def main():
    labeler = FnSeqLabelingPipeline("configs/2020-04-15_best_frame_embedding_layer10.json")
    labeler.load_trained_model()

    for raw_file in glob.glob(LIVESCORE_DATA_SOURCE + "*.raw.txt"):

        print(raw_file)
        predictions = []
        with open(raw_file, encoding="utf-8") as f_in:
            for line in f_in:
                predictions.append(labeler.predict_raw_text(line)[1])

        with open(raw_file.replace(".raw.txt", ".frameid.txt"), "w", encoding="utf-8") as f_out:
            for sentence in predictions:
                for tok, pred in sentence:
                    f_out.write(f"{tok}\t{pred}\n")


if __name__ == '__main__':
    main()
