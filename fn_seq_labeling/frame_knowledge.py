import sys
from typing import List

from nltk.corpus import framenet as fn


def find_deepest_inherited_frames(frame: str) -> List[str]:

    fn_entry = fn.frame(frame)
    inherited_frames = []
    for rel in fn_entry["frameRelations"]:
        if rel["type"]["name"] == "Inheritance" and rel["Child"] == fn_entry:
            inherited_frames.append(rel["Parent"]["name"])

    deepest_inherited_frames = []
    if not inherited_frames:
        deepest_inherited_frames.append(frame)

    else:
        for inh_frame in inherited_frames:
            inherited_by_inherited = find_deepest_inherited_frames(inh_frame)
            deepest_inherited_frames.extend(inherited_by_inherited)

    return deepest_inherited_frames


def main():
    print(find_deepest_inherited_frames(sys.argv[1]))
    print(list(fn.frame(sys.argv[1])["lexUnit"]))


if __name__ == '__main__':
    main()
