import collections
import enum

import pandas as pd

from fn_seq_labeling.evaluation.eval_utils import get_prediction_sets
from fn_seq_labeling.frame_knowledge import find_deepest_inherited_frames


class ErrorAnalysisLevel(enum.Enum):
    TOKENS = 0
    FRAMES = 1
    DEEP_FRAMES = 2


def analyze_errors(level: ErrorAnalysisLevel,
                   prediction_df: pd.DataFrame = None,
                   prediction_file: str = None
                   ) -> pd.DataFrame:
    if not (prediction_df or prediction_file):
        raise ValueError("Either a dataframe or a CSV file should be given")

    if prediction_file:
        prediction_df = pd.read_csv(prediction_file)

    labels_to_errors = collections.defaultdict(int)
    labels_to_freqs = collections.defaultdict(int)
    labels_to_frame_splits = collections.defaultdict(lambda: collections.defaultdict(int))

    for i, row in prediction_df.iterrows():

        predictions = row["prediction"].split("|")
        golds = row["gold"].split("|")
        token = row["token"]
        _, predictions_frames, _ = get_prediction_sets(predictions)
        _, gold_frames, _ = get_prediction_sets(golds)

        if len(gold_frames) > 1:
            print("Skipping token with double frame label", repr(gold_frames))
            continue
        elif len(gold_frames) < 1:
            continue
        else:  # exactly one label
            gold_frame = list(gold_frames)[0]
            if level == ErrorAnalysisLevel.FRAMES:
                gold_label_keys = [gold_frame]
            elif level == ErrorAnalysisLevel.DEEP_FRAMES:
                deep_frame_list = find_deepest_inherited_frames(gold_frame.replace("T:", ""))
                gold_label_keys = deep_frame_list
            elif level == ErrorAnalysisLevel.TOKENS:
                gold_label_keys = [token.lower()]
            else:
                raise ValueError

        for key in gold_label_keys:
            labels_to_freqs[key] += 1
        if gold_frame not in predictions_frames:
            for key in gold_label_keys:
                labels_to_errors[key] += 1
                labels_to_frame_splits[key][gold_frame] += 1

    frame_rel_errors = {f: labels_to_errors[f] / labels_to_freqs[f] for f in labels_to_errors}
    sorted_errors = sorted(frame_rel_errors.items(), key=lambda t: t[1], reverse=True)
    data_points = []
    for frame, rel_err in sorted_errors:
        absolute_error = labels_to_errors[frame]
        frequency = labels_to_freqs[frame]
        frame_split = labels_to_frame_splits[frame]
        data_points.append({
            "frame": frame,
            "rel_error": rel_err,
            "abs_error": absolute_error,
            "frame_freq": frequency,
            "frame_split": ", ".join(
                f"{k}={v}" for k, v in sorted(frame_split.items(), key=lambda t: t[1], reverse=True)
            )
        })
    error_df = pd.DataFrame(data_points)
    return error_df


