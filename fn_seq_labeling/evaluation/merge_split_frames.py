import collections
import sys
from typing import Dict, List, Optional, Tuple

import pandas as pd
from nltk.corpus import framenet as fn

from fn_seq_labeling.evaluation.eval_utils import split_sentences


def merge_split_frames(split_file: str,
                       output_file_name: str,
                       gold_split_file: Optional[str] = None,
                       add_frame_names: bool = False
                       ):
    # parse the csv, split by sentence, and then group sentences with the same token strings
    # (= same sentence, different frame)
    split_file_df = pd.read_csv(split_file)
    sentences = split_sentences(split_file_df)
    grouped_by_sentence = group_by_sentence(sentences)

    # if a file with predictions given gold frames is given, do the same for that one
    if gold_split_file is not None:
        gold_split_file_df = pd.read_csv(gold_split_file)
        gold_sentences = split_sentences(gold_split_file_df)
        gold_grouped_by_sentence = group_by_sentence(gold_sentences)
    else:
        gold_grouped_by_sentence = None

    merged_dfs = merge_sentence_dfs(grouped_by_sentence, add_frame_names=add_frame_names)

    # add golds from the dataset with gold frames so that we can evaluate against a complete dev/test set
    print(f"Adding gold-frame-golds to predicted-frame-golds for file {output_file_name}")
    if gold_grouped_by_sentence is not None:
        gold_merged_dfs = merge_sentence_dfs(gold_grouped_by_sentence, add_frame_names=add_frame_names)

        sent_to_merged_dfs: Dict[str, List[Optional[pd.DataFrame]]] = \
            collections.defaultdict(lambda: [None, None])

        for i, df_list in enumerate((merged_dfs, gold_merged_dfs)):
            for sent_df in df_list:
                key = " ".join([str(t) for t in sent_df["token"].tolist()])
                sent_to_merged_dfs[key][i] = sent_df

        missing_sentences = []

        for sent, [df_with_pred_frames, df_with_gold_frames] in sent_to_merged_dfs.items():

            # sentence with gold frames, but no predicted frames
            # --> produce sentence frame with no predictions
            if df_with_pred_frames is None:
                assert df_with_gold_frames is not None
                df_with_pred_frames = df_with_gold_frames.copy()
                df_with_pred_frames["prediction"] = pd.Series(["_" for _ in df_with_gold_frames["prediction"].tolist()])
                missing_sentences.append(df_with_pred_frames)

            # sentences with gold frames, but no predicted frames
            # --> do nothing, "gold" column will already be empty
            elif df_with_gold_frames is None:
                continue

            # sentence has both gold frames and predicted frames
            # --> add the gold annotations
            else:
                df_with_pred_frames["gold"] = df_with_gold_frames["gold"]

        # add any (empty) sentences that were originally missing
        merged_dfs.extend(missing_sentences)

    out_df = pd.concat(merged_dfs, ignore_index=True)
    out_df.to_csv(output_file_name)


def merge_sentence_dfs(grouped_by_sentence, add_frame_names=False):
    merged_dfs = []
    for sent_str, sent_dfs in grouped_by_sentence.items():
        merged_df = merge_sent_dfs(sent_dfs, add_frame_names=add_frame_names)
        merged_dfs.append(merged_df)
    return merged_dfs


def group_by_sentence(sentences: List[pd.DataFrame]
                      ) -> Dict[str, List[pd.DataFrame]]:
    sent_to_annotations = collections.defaultdict(list)
    for sent_df in sentences:
        token_str = " ".join([str(t) for t in sent_df["token"].tolist()])
        sent_to_annotations[token_str].append(sent_df)
    return sent_to_annotations


def merge_sent_dfs(sent_dfs: List[pd.DataFrame],
                   add_frame_names=False
                   ) -> pd.DataFrame:

    assert len(sent_dfs) >= 1
    merged_annos = [{"token": token, "prediction": [], "gold": [], "filtered_pred": []}
                    for token in sent_dfs[0]["token"].tolist()]

    for df in sent_dfs:

        context_frames = {df["frame_context"].tolist()[0].split(":")[1]}
        tok_predictions: List[List[str]] = []
        tok_golds: List[List[str]] = []

        # first pass: get predictions & golds for each token, and define set of gold frames
        for prediction_str, gold_str in zip(df["prediction"].tolist(), df["gold"].tolist()):
            predictions = prediction_str.split("|") if prediction_str != "_" else []
            golds = gold_str.split("|") if gold_str != "_" else []
            tok_predictions.append(predictions)
            tok_golds.append(golds)

        assert len(context_frames) <= 1
        if len(context_frames) == 1:
            context_frame = next(iter(context_frames))
        else:
            context_frame = None

        allowed_roles = {r for cf in context_frames for r in fn.frame(cf).FE}
        for i, (predictions, golds) in enumerate(zip(tok_predictions, tok_golds)):
            for pred in predictions:
                # filter out illegal frame targets
                if pred.startswith("T:"):
                    if pred.split(":")[1] != context_frame:
                        # print(f"Illegal frame target {pred} given frame set {gold_frames}")
                        merged_annos[i]["filtered_pred"].append(f"{context_frame}>>{pred}")
                        continue
                # filter out illegal roles
                elif pred.split(":")[0] not in allowed_roles:
                    # print(f"Illegal role label {pred} given frame set {gold_frames}")
                    merged_annos[i]["filtered_pred"].append(f"{context_frame}>>{pred}")
                    continue
                # legal roles: optionally prepend the frame name
                elif add_frame_names:
                    pred = f"{context_frame}:{pred}"

                merged_annos[i]["prediction"].append(pred)
            for gold in golds:
                if (not gold.startswith("T:")) and add_frame_names:
                    gold = f"{context_frame}:{gold}"
                merged_annos[i]["gold"].append(gold)

    # convert prediction list to "|"-separated string
    for tok_anno in merged_annos:
        for key in ("prediction", "gold", "filtered_pred"):
            tok_anno[key] = "|".join(tok_anno[key]) or "_"
    return pd.DataFrame(merged_annos)


if __name__ == '__main__':
    merge_split_frames(sys.argv[1], sys.argv[3], gold_split_file=sys.argv[2], add_frame_names=False)
    # merge_split_frames(sys.argv[1], sys.argv[2], add_frame_names=True)
