import abc
import collections
import dataclasses
import datetime
import os
import sys
from typing import Dict, List, Tuple, Set, Iterator, Iterable, Optional

import bs4
import lxml.builder
import lxml.etree
import numpy as np
import pandas as pd
from nltk.corpus import framenet as fn

from fn_seq_labeling.evaluation.eval_utils import split_sentences


# -- Data structures for frame-semantic parses --
@dataclasses.dataclass
class FrameTargetInstance:
    frame_name: str
    tokens: List[str]
    token_idx: List[int]
    structure_idx: Optional[int] = None


@dataclasses.dataclass
class FrameElementInstance:
    role_name: str
    tokens: List[str]
    token_idx: List[int]


@dataclasses.dataclass
class FrameInstance:
    target: FrameTargetInstance
    roles: List[FrameElementInstance]


# -- Classes for selecting frames when there are multiple options per token --
class FrameChooser(abc.ABC):

    @abc.abstractmethod
    def choose_frame(self,
                     frame_predictions: Set[Tuple[str, float]],
                     target_idx: int,
                     sentence: pd.DataFrame
                     ) -> str:
        pass


class ProbaFrameChooser(FrameChooser):

    def choose_frame(self,
                     frame_predictions: Set[Tuple[str, float]],
                     target_idx: int,
                     sentence: pd.DataFrame
                     ) -> str:
        if len(frame_predictions) == 1:
            return next(iter(frame_predictions))[0]

        best_frame, highest_proba = max(frame_predictions, key=lambda pred: pred[1])
        return best_frame


class StructureIndexChooser(FrameChooser):

    def choose_frame(self,
                     frame_predictions: Set[Tuple[str, float]],
                     target_idx: int,
                     sentence: pd.DataFrame
                     ) -> str:
        pass


# -- Classes for resolving conflicts where a frame element could 'belong' to multiple different frame instances --
class FrameConflictResolver(abc.ABC):

    @abc.abstractmethod
    def resolve(self,
                frame_element: FrameElementInstance,
                frame_instances: List[FrameInstance]
                ) -> FrameInstance:
        pass


class NearestPredicateResolver(FrameConflictResolver):

    def resolve(self,
                frame_element: FrameElementInstance,
                frame_instances: List[FrameInstance]
                ) -> FrameInstance:

        closest_fi = None
        closest_dist = None

        # take the midpoint (in terms of indices) of the frame element span
        fe_midpoint = np.mean(frame_element.token_idx)

        for fi in frame_instances:

            fi_midpoint = np.mean(fi.target.token_idx)
            distance = np.abs(fi_midpoint - fe_midpoint)

            if closest_dist is None or distance < closest_dist:
                closest_dist = distance
                closest_fi = fi

        return closest_fi


def main(prediction_file,
         discontinuous_disallowed=False,
         predicted_frames_on_roles=False,
         use_structure_idx=False,
         dev=True):
    print(f"predicted_frames_on_roles={predicted_frames_on_roles}")
    print(f"dev={dev}")

    prediction_df = pd.read_csv(prediction_file)
    sentences = [clean_sentence(s) for s in split_sentences(prediction_df)]

    if use_structure_idx and "prediction_with_idxs" in prediction_df:
        frame_structures = [collect_frame_structures_from_indices(s, predicted_frames_on_roles) for s in sentences]

    elif predicted_frames_on_roles:
        frame_structures = [guess_frame_structures(s) for s in sentences]

    else:
        frame_structures = [guess_frames_and_structures(s) for s in sentences]

    if discontinuous_disallowed:
        remove_discontinuous_fe_(frame_structures)
        remove_overlapping_fes_(frame_structures)

    basename = os.path.basename(prediction_file)
    with open(f"output/full_structure_predictions/{basename.replace('.csv', '.conll')}", "w") as f:
        for line in to_sesame_format(frame_structures, sentences):
            f.write(line + os.linesep)
    with open(f"output/full_structure_predictions/{basename.replace('.csv', '.sentences')}", "w") as f:
        for sentence_df in sentences:
            f.write(" ".join(sentence_df["token"].tolist()) + os.linesep)

    if dev:
        mappings = get_offset_mappings_from_gold("data/fn1.7/semeval/dev.gold.xml")
    else:
        mappings = get_offset_mappings_from_gold("data/fn1.7/semeval/test.gold.xml")

    with open(f"output/full_structure_predictions/{basename.replace('.csv', '.semeval')}", "w") as f:
        print(f.name)
        f.write(to_semeval_format(frame_structures, sentences, mappings))


def remove_discontinuous_fe_(frame_structures: List[List[FrameInstance]]):
    for sentence_annos in frame_structures:
        for fi in sentence_annos:
            role_names = []
            fes_to_remove = []
            for fe in fi.roles:
                # discontinuous roles
                if fe.role_name in role_names:
                    print(f"Removing discontinuous role part {fe.role_name}")
                    fes_to_remove.append(fe)
                else:
                    role_names.append(fe.role_name)
            for fe in fes_to_remove:
                fi.roles.remove(fe)


def remove_overlapping_fes_(frame_structures: List[List[FrameInstance]]):
    for sentence_annos in frame_structures:
        for fi in sentence_annos:
            fes_to_remove = []
            for fe_i in fi.roles:
                for fe_j in fi.roles:
                    if fe_i == fe_j:
                        continue
                    if fe_j.tokens[0] < fe_i.tokens[0] < fe_j.tokens[-1]:
                        fes_to_remove.append(fe_i)
            for fe in fes_to_remove:
                # check if we already removed
                if fe not in fi.roles:
                    continue
                fi.roles.remove(fe)


def to_sesame_format(frame_structures: List[List[FrameInstance]],
                     sentences: List[pd.DataFrame]
                     ) -> Iterator[str]:
    assert len(frame_structures) == len(sentences)

    for sent_idx, (sentence_annos, sentence_df) in enumerate(zip(frame_structures, sentences)):
        tokens = sentence_df["token"].tolist()
        for fi in sentence_annos:
            tokens_to_annotations = [{"frame": "_", "FE": "O"} for _ in tokens]
            for target_idx in fi.target.token_idx:
                tokens_to_annotations[target_idx]["frame"] = fi.target.frame_name
            for fe in fi.roles:
                first_tok = True
                for fe_idx in fe.token_idx:
                    if len(fe.token_idx) == 1:
                        iob = "S"
                    elif first_tok:
                        iob = "B"
                        first_tok = False
                    else:
                        iob = "I"
                    tokens_to_annotations[fe_idx]["FE"] = f"{iob}-{fe.role_name}"

            for tok_idx, (token, annotations) in enumerate(zip(tokens, tokens_to_annotations)):
                yield f"{tok_idx + 1}\t{token.lower()}\t_\t{token}\t_\tPOS\t{sent_idx}\t_\t_\t_\t_\t_\t" \
                      f"LU.pos\t{annotations['frame']}\t{annotations['FE']}"
            yield ""


def get_str_for_sorting(tokens: List[str]
                        ) -> str:
    """
    Get a token string that is such that strings will be sorted the same way
    as the sorting in the gold semeval data
    :param tokens: list of tokens in the predicted sentence
    :return: `tokens` as a string with modifications
    """
    chars = " ".join(tokens)
    mod_chars = ""
    for char in chars:

        # replace dashes (ranked before letters in original sorting) by "~" (ranked after letters)
        if char == "-":
            mod_chars += "~"
        else:
            mod_chars += char
    return mod_chars


def to_semeval_format(frame_structures: List[List[FrameInstance]],
                      sentences: List[pd.DataFrame],
                      gold_offset_mappings: Dict[str, List[int]]
                      ) -> str:

    # ID counters
    id_counters = {
        "annotationSet": 0,
        "layer": 0,
        "label": 0,
    }

    # helper for incrementing and returning counters
    def inc_id(tag):
        id_counters[tag] += 1
        return str(id_counters[tag])

    # helper for formatting text
    def format_token_string(sentence_df: pd.DataFrame) -> Optional[str]:

        chars = "".join(sentence_df["token"].tolist())

        if chars.startswith("Allpartieshaveagreed"):
            print()

        # if sentence does not exist in gold set: return tokenization based on spaces
        if chars not in gold_offset_mappings:
            return " ".join(sentence_df["token"].tolist())

        # reconstruct string with original tokenization using offset mapping
        mapping = gold_offset_mappings[chars]

        orig_string = ""

        prev_idx = 0
        for char_idx, char in enumerate(chars):
            orig_idx = mapping[char_idx]
            # add original spaces
            if orig_idx - prev_idx > 1:
                orig_string += " "
            orig_string += char
            prev_idx = orig_idx

        # HACK! fix weird sentence in TEST with leading space
        if orig_string.startswith("Simply put , Stephanopoulos"):
            print("Got ya, Stephanopoulos!")
            orig_string = " " + orig_string

        return orig_string

    # helper for checking if sentence occurs in gold
    def exists_in_gold(sentence_df):
        chars = "".join(sentence_df["token"].tolist())
        return chars in gold_offset_mappings

    # helper for getting char offsets from token offsets
    def token_to_char(sentence_df, token_idx, last_char=False):

        tokens = sentence_df["token"].tolist()

        # find mapping to original characters
        char_sequence = "".join(tokens)
        mapping = gold_offset_mappings[char_sequence]

        # find character position without considering spaces
        char_idx = 0
        for token in tokens[:token_idx]:
            char_idx += len(token)
        if last_char:
            char_idx += len(tokens[token_idx]) - 1

        return str(mapping[char_idx])

    # find any sentences that are missing -> add empty annotation set for those
    annotated_sentence_strs = {"".join(s_df["token"].tolist()) for s_df in sentences}
    for sent_str in gold_offset_mappings:
        if sent_str not in annotated_sentence_strs:
            print(f"Adding empty annotations for sentence\n{sent_str}\n")
            frame_structures.append([])
            sentences.append(pd.DataFrame([{
                "token": sent_str,
                "prediction": "_",
                "gold": "_"
            }]))

    # sort alphabetically
    sorted_annotations = sorted(
        zip(frame_structures, sentences),
        # key=lambda pair: get_str_for_sorting(format_token_string(pair[1]))
        key=lambda pair: format_token_string(pair[1])
    )

    # remove duplicates
    sorted_annotations_without_duplicates = []
    seen_sentences = set()
    for fis, s in sorted_annotations:
        chars = "".join(s["token"].tolist())
        if chars in seen_sentences:
            print("Removing duplicate sentence: ", chars)
            continue
        else:
            seen_sentences.add(chars)
            sorted_annotations_without_duplicates.append((fis, s))
    sorted_annotations = sorted_annotations_without_duplicates

    print(sorted_annotations[0][1]["token"].tolist())

    # set up XML elements
    em = lxml.builder.ElementMaker()
    CORPUS = em.corpus
    DOCUMENTS = em.documents
    DOCUMENT = em.document
    PARAGRAPHS = em.paragraphs
    PARAGRAPH = em.paragraph
    SENTENCES = em.sentences
    SENTENCE = em.sentence
    TEXT = em.text
    ANNOTATION_SETS = em.annotationSets
    ANNOTATION_SET = em.annotationSet
    LAYERS = em.layers
    LAYER = em.layer
    LABELS = em.labels
    LABEL = em.label

    # convert annotations to semeval XML
    xml_annotations = [
        SENTENCE(
            TEXT(format_token_string(sentence_df)),
            ANNOTATION_SETS(
                *[ANNOTATION_SET(
                    LAYERS(
                        LAYER(
                            LABELS(
                                LABEL(ID=inc_id("label"),
                                      name="Target",
                                      start=token_to_char(sentence_df, fi.target.token_idx[0]),
                                      end=token_to_char(sentence_df, fi.target.token_idx[-1], last_char=True)
                                      )
                            ), ID=inc_id("layer"), name="Target"),
                        LAYER(
                            LABELS(
                                *[LABEL(
                                    ID=inc_id("label"),
                                    name=fe.role_name,
                                    start=token_to_char(sentence_df, fe.token_idx[0]),
                                    end=token_to_char(sentence_df, fe.token_idx[-1], last_char=True)
                                ) for fe in fi.roles]
                            ),
                            ID=inc_id("layer"), name="FE")
                    ),
                    ID=inc_id("annotationSet"), frameName=fi.target.frame_name)
                    for fi in sent_anno]
            ),
            ID=str(sent_idx))
        for sent_idx, (sent_anno, sentence_df) in enumerate(sorted_annotations)
        if exists_in_gold(sentence_df)
    ]

    # create overall document structure
    doc = CORPUS(
        DOCUMENTS(
            DOCUMENT(
                PARAGRAPHS(
                    PARAGRAPH(
                        SENTENCES(
                            *xml_annotations
                        )
                    )
                )
            )
        ), XMLCreated=datetime.datetime.utcnow().strftime("%a %b %d %H:%M:%S UTC %Y"))

    return lxml.etree.tostring(doc, encoding="UTF-8", xml_declaration=True, pretty_print=True).decode("utf-8")


def clean_sentence(sentence: pd.DataFrame) -> pd.DataFrame:
    """
    Remove special BERT tokens ([CLS], [SEP], [PAD]) and merge byte-pairs (all + ##egation -> allegation)
    :param sentence: sentence dataframe
    :return: cleaned sentence dataframe
    """

    cleaned_rows = []
    prev_tokens = [""]

    # helper function: merge tokens / annotation rows
    def merge_rows(token_to_add):
        prev_tokens[-1] += token_to_add
        cleaned_rows[-1]["token"] = prev_tokens[-1]

        # keep annotation from the 2nd mark
        for column in sentence.columns[1:]:
            cleaned_rows[-1][column] = row[column]

    for _, row in sentence.iterrows():

        cur_token = str(row["token"])

        if cur_token in ["[SEP]", "[CLS]", "[PAD]"]:
            continue

        # if this is the first (non-skipped) token: just add it
        if not cleaned_rows:
            prev_tokens.append(row["token"])
            cleaned_rows.append(row.copy())

        # merge WordPieces
        elif cur_token.startswith("##"):
            merge_rows(cur_token.replace("##", ""))

        else:
            prev_tokens.append(cur_token)
            cleaned_rows.append(row.copy())

    return pd.DataFrame(cleaned_rows)


def tokens_to_orig_offsets(tokens: List[str],
                           mapping: List[int]
                           ) -> List[Tuple[int, int]]:
    offsets = []
    char_idx = 0
    for tok in tokens:
        tok_start = mapping[char_idx]
        char_idx += len(tok)
        tok_end = mapping[char_idx]
        offsets.append((tok_start, tok_end))
    return offsets


def map_orig_offsets(orig_tokens: List[str]
                     ) -> List[int]:
    """
    map index of characters in sentence without any spaces to indices in original string
    e.g. ["he", "saw", "her"] -> h|e|s|a|w|h|e|r -> [0, 1, 3, 4, 5, 7, 8, 9]
    :param orig_tokens: list of tokens according to original tokenization
    :return: list of original indices for every non-space char in `orig_tokens`
    """
    mapping = []

    orig_idx = 0
    for tok in orig_tokens:
        for _ in tok:
            mapping.append(orig_idx)
            orig_idx += 1

        # skip over spaces
        orig_idx += 1

    return mapping


def create_char_offset_mappings(orig_sentences: Iterable[str]
                                ) -> Dict[str, List[int]]:
    """
    Given a corpus of unique sentences, create an offset mapping (see `map_orig_offsets`) for every sentences
    :param orig_sentences: list of sentences (as strings) with original tokenization)
    :return: dict where keys are sentences in `orig_sentences` with spaces removed and values are mappings
    """

    mappings = {}
    for sentence in orig_sentences:
        tokens = sentence.split(" ")
        mapping = map_orig_offsets(tokens)
        key = "".join(tokens)
        mappings[key] = mapping
    return mappings


def get_offset_mappings_from_gold(gold_eval_file: str
                                  ) -> Dict[str, List[int]]:
    with open(gold_eval_file, encoding="utf-8") as f:
        soup = bs4.BeautifulSoup(f.read(), features="lxml")

    sentences = (text_elem.text for text_elem in soup.find_all("text"))
    return create_char_offset_mappings(sentences)


def collect_frame_structures_from_indices(sentence: pd.DataFrame,
                                          predicted_frames_on_roles: bool
                                          ) -> List[FrameInstance]:
    if "buy" in sentence["token"].to_list():
        print()

    # in case of multiple frames for single target, choose the best one
    # (with OpenSESAME it's rare that you have this, and since we don't have the probabilities, the 'best'
    # one will be the first one in practice)
    chooser = ProbaFrameChooser()
    frames = collect_frames(sentence, chooser, "prediction_with_idxs")
    structures = []
    for fti in frames:
        roles = collect_possible_roles(fti.frame_name, sentence,
                                       frame_struct_idx=fti.structure_idx,
                                       prediction_key="prediction_with_idxs",
                                       predicted_frames_on_roles=predicted_frames_on_roles)
        structures.append(FrameInstance(fti, roles))

    return structures


def guess_frame_structures(sentence: pd.DataFrame
                           ) -> List[FrameInstance]:
    chooser = ProbaFrameChooser()
    frames = collect_frames(sentence, chooser)
    structures = []
    for fti in frames:
        roles = collect_possible_roles(fti.frame_name, sentence,
                                       frame_struct_idx=fti.structure_idx,
                                       predicted_frames_on_roles=True)
        structures.append(FrameInstance(fti, roles))

    return structures


def guess_frames_and_structures(sentence: pd.DataFrame
                                ) -> List[FrameInstance]:
    frame_instances = []

    # # structure indices present?
    # if "prediction_with_idxs" in sentence:
    #     chooser = StructureIndexChooser()
    # else:
    chooser = ProbaFrameChooser()
    resolver = NearestPredicateResolver()
    frames = collect_frames(sentence, chooser)

    frame_type_to_instances: Dict[str, List[FrameInstance]] = collections.defaultdict(list)
    for frame in frames:
        possible_roles = collect_possible_roles(frame.frame_name, sentence, predicted_frames_on_roles=False)
        instance = FrameInstance(
            target=frame,
            roles=possible_roles,
        )
        frame_instances.append(instance)
        frame_type_to_instances[frame.frame_name].append(instance)

    # OLD APPROACH: solve conflicts globally (ban >1 frame instances, regardless of frame type, sharing a FE instance)
    # check for conflicts
    conflicting_roles = find_conflicting_roles(frame_instances)
    if conflicting_roles:
        process_conflicts_(conflicting_roles, resolver)

    # # NEW APPROACH: solve conflicts by frame (disallow >1 instances of the same frame sharing a role)
    # for ft, ft_instances in frame_type_to_instances.items():
    #     frame_conflicts = find_conflicting_roles(ft_instances)
    #     if frame_conflicts:
    #         process_conflicts_(frame_conflicts, resolver)

    return frame_instances


def process_conflicts_(conflicting_roles: List[Tuple[FrameElementInstance, List[FrameInstance]]],
                       resolver: FrameConflictResolver
                       ):
    for conflicting_fe, conflicting_fis in conflicting_roles:
        # find all frame instances matching the conflicting frames
        best_instance = resolver.resolve(conflicting_fe, conflicting_fis)
        for fi in conflicting_fis:
            if fi == best_instance:
                continue
            if conflicting_fe in fi.roles:
                fi.roles.remove(conflicting_fe)


def find_conflicting_roles(frame_instances: List[FrameInstance]
                           ) -> List[Tuple[FrameElementInstance, List[FrameInstance]]]:
    role_names_to_fis = collections.defaultdict(list)
    role_names_to_fes = collections.defaultdict(list)
    for fi in frame_instances:
        for fe in fi.roles:
            if fi not in role_names_to_fis[fe.role_name]:
                role_names_to_fis[fe.role_name].append(fi)
            if fe not in role_names_to_fes[fe.role_name]:
                role_names_to_fes[fe.role_name].append(fe)

    conflicting_roles = []
    for rn, fi_list in role_names_to_fis.items():

        # there is a conflict if a given role (name) could be associated with multiple frame instances
        if len(fi_list) > 1:

            # get all the frame elements with the current role name
            for fe in role_names_to_fes[rn]:
                conflicting_roles.append((fe, fi_list))

    return conflicting_roles


def collect_frames(sentence: pd.DataFrame,
                   chooser: FrameChooser,
                   prediction_key: str = "prediction"
                   ) -> List[FrameTargetInstance]:
    frames = []

    cur_frame_name = None
    cur_frame_tokens = []
    cur_frame_token_idx = []

    for token_idx, (_, row) in enumerate(sentence.iterrows()):

        token = row["token"]

        predictions = row[prediction_key].split("|")

        # map prediction strings like "T:Project:0.989" to ("Project", 0.989) or ("Project",) (depending on model)
        frame_predictions = {tuple(pred.split(":")[1:]) for pred in predictions if pred.startswith("T:")}
        # convert probabilities to floats if needed
        if frame_predictions and len(next(iter(frame_predictions))) == 0:
            frame_predictions = {(pred_tuple[0], float(pred_tuple[1])) for pred_tuple in frame_predictions}

        if not frame_predictions:
            cur_frame_name = None
            continue

        frame_name = chooser.choose_frame(frame_predictions, token_idx, sentence)
        if "@" in frame_name:
            frame_name, structure_idx = frame_name.split("@")[0], int(frame_name.split("@")[1])
        else:
            structure_idx = None

        if frame_name != cur_frame_name:
            cur_frame_name = frame_name
            cur_frame_tokens = []
            cur_frame_token_idx = []
            frames.append(FrameTargetInstance(
                frame_name=cur_frame_name,
                tokens=cur_frame_tokens,
                token_idx=cur_frame_token_idx,
                structure_idx=structure_idx
            ))

        cur_frame_tokens.append(token)
        cur_frame_token_idx.append(token_idx)

    return frames


def collect_possible_roles(target_frame: str,
                           sentence: pd.DataFrame,
                           frame_struct_idx: Optional[int] = None,
                           prediction_key="prediction",
                           predicted_frames_on_roles=True
                           ) -> List[FrameElementInstance]:
    possible_roles = []
    role_set = get_role_set(target_frame)

    # role spans that we're currently processing
    cur_roles_names: Set[str] = set()
    cur_role_tokens: Dict[str, List[str]] = {}
    cur_role_token_idx: Dict[str, List[int]] = {}

    for token_idx, (_, row) in enumerate(sentence.iterrows()):

        token = row["token"]
        predictions = row[prediction_key].split("|")

        if "Non-gradable_proximity" in predictions:
            print()

        # get all the predictions that are roles; parse name and probability
        if predicted_frames_on_roles:
            role_predictions = {
                (p.split(":")[0], p.split(":")[1], float(p.split(":")[2]))
                for p in predictions if p != "_" and not p.startswith("T:")
            }
        else:
            role_predictions = {
                (None, p.split(":")[0], float(p.split(":")[1]))
                if ":" in p else (None, p, 1.0)
                for p in predictions if p != "_" and not p.startswith("T:")
            }
		
        # parse structure idx if present
        role_predictions = {
            (role_frame, role.split("@")[0], int(role.split("@")[1]), proba)
            if "@" in role
            else (role_frame, role, None, proba)
            for role_frame, role, proba in role_predictions
        }

        # filter only the roles for the current frame
        role_predictions = {
            (role, proba) for role_frame, role, role_struct_idx, proba in role_predictions
            if (role in role_set["core"] or role in role_set["non_core"])
               and role_struct_idx == frame_struct_idx
               and (role_frame is None) or (role_frame == target_frame)
        }

        # remove roles whose span has ended
        for role in cur_roles_names.difference({role for role, proba in role_predictions}):
            cur_roles_names.remove(role)
            del cur_role_tokens[role]
            del cur_role_token_idx[role]

        for role, proba in role_predictions:

            if role not in cur_roles_names:
                cur_roles_names.add(role)
                cur_role_tokens[role] = [token]
                cur_role_token_idx[role] = [token_idx]
                possible_roles.append(FrameElementInstance(
                    role_name=role,
                    tokens=cur_role_tokens[role],
                    token_idx=cur_role_token_idx[role]
                ))
            else:
                cur_role_tokens[role].append(token)
                cur_role_token_idx[role].append(token_idx)

    return possible_roles


def get_role_set(frame: str) -> Dict[str, Set[str]]:
    roles = fn.frame(frame).FE
    core = {r for r in roles if roles[r].coreType == "Core"}
    non_core = {r for r in roles if r not in core}
    return {"core": core, "non_core": non_core}


if __name__ == '__main__':
    input_file = sys.argv[1]
    if sys.argv[2] == "dev":
        dev_set = True
    elif sys.argv[2] == "test":
        dev_set = False
    else:
        raise ValueError("2nd cmd argument should be in {dev,test}")
    if sys.argv[3] in ["y", "yes"]:
        use_structure_idx = True
    else:
        use_structure_idx = False

    main(input_file, dev=dev_set, use_structure_idx=use_structure_idx)
