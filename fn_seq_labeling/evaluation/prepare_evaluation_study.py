import sys
import random

import pandas as pd

from fn_seq_labeling.evaluation.eval_utils import split_sentences


def find_sesame_sent(sent_df, sesame_sentences):
    for sesame_sent_df in sesame_sentences:
        if sesame_sent_df["token"].tolist() == sent_df["token"].tolist():
            return sesame_sent_df
    raise ValueError("`sesame_sentences` does not contain the sentence you're looking for!")


def filter_frame_targets_(sesame_sent_df: pd.DataFrame, key="prediction"):
    for i, row in sesame_sent_df.iterrows():
        if row[key] != "_":
            frame_tgt = [p for p in row[key].split("|") if p.startswith("T:")]
            sesame_sent_df.at[i, key] = frame_tgt[0] if frame_tgt else "_"


def main():

    prediction_file = sys.argv[1]
    sesame_file = sys.argv[2]

    prediction_df = pd.read_csv(prediction_file)
    sesame_df = pd.read_csv(sesame_file)

    pred_sentences = split_sentences(prediction_df)
    sesame_sentences = split_sentences(sesame_df)

    pred_sentences_min_10_tokens = [df for df in pred_sentences if len(df) >= 10]
    sample = random.sample(pred_sentences_min_10_tokens, k=20)

    for sent_df in sample:
        sesame_sent_df = find_sesame_sent(sent_df, sesame_sentences)
        filter_frame_targets_(sesame_sent_df, key="prediction")
        filter_frame_targets_(sesame_sent_df, key="gold")
        print(sesame_sent_df)


if __name__ == '__main__':
    main()
