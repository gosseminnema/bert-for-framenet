import os
import sys
from typing import List

import pandas as pd

from fn_seq_labeling.evaluation.eval_utils import split_sentences, align_sentence_dfs


def merge_prediction_files(input_file_frame, input_file_role, output_file):
    frame_df = pd.read_csv(input_file_frame)
    role_df = pd.read_csv(input_file_role)
    merged_df = merge_frame_and_roles(frame_df, role_df)
    merged_df.to_csv(output_file)


def merge_frame_and_roles(frame_df, role_df):

    sentences_frames = split_sentences(frame_df)
    sentences_roles = split_sentences(role_df)
    aligned_sentences = align_sentence_dfs(sentences_frames, sentences_roles)

    combined_dfs = []

    for sentence_str in aligned_sentences:
        sent_df_frame, sent_df_role = aligned_sentences[sentence_str]
        if sent_df_role is None or sent_df_frame is None:
            continue
        combined_dfs.append(merge_sentence_dfs(sent_df_frame, sent_df_role))

    combined_df = pd.concat(combined_dfs, ignore_index=True)
    return combined_df


def merge_token_prediction(frame_pred_str: str,
                           role_pred: str,
                           keep_frames_in_role_pred: bool = True
                           ) -> List[str]:

    if frame_pred_str in ["_", "X"]:
        parsed_frame_pred = []
    else:
        parsed_frame_pred = [frame_pred_str + ":1.000"]

    if role_pred == "_":
        parsed_role_pred = []
    else:
        parsed_role_pred = role_pred.split("|")

    # if we have a frame prediction: remove any frame predictions that might be in the role predictions
    if parsed_frame_pred:
        combined_pred = [pred for pred in parsed_role_pred if not pred.startswith("T:")] + parsed_frame_pred

    # otherwise, keep everything from the role predictions, including any frame
    elif keep_frames_in_role_pred:
        combined_pred = parsed_role_pred

    # remove frame predictions in the role predictions, even if we don't have separate frame predictions
    else:
        combined_pred = [pred for pred in parsed_role_pred if not pred.startswith("T:")]

    return combined_pred or ["_"]


def merge_sentence_dfs(sent_df_frame: pd.DataFrame,
                       sent_df_role: pd.DataFrame
                       ) -> pd.DataFrame:

    assert sent_df_frame["token"].tolist() == sent_df_role["token"].tolist()

    merged_rows = []
    for (_, row_frame), (_, row_role) in zip(sent_df_frame.iterrows(), sent_df_role.iterrows()):

        token = row_frame["token"]
        gold = "|".join(merge_token_prediction(row_frame["gold"], row_role["gold"]))
        prediction = "|".join(merge_token_prediction(row_frame["prediction"], row_role["prediction"]))
        merged_rows.append({
            "token": token,
            "prediction": prediction,
            "prediction_n_best": None,
            "gold": gold
        })

    return pd.DataFrame(merged_rows)


if __name__ == '__main__':
    frame_file = sys.argv[1]
    role_file = sys.argv[2]
    frame_dir, frame_basename = os.path.split(frame_file)
    role_dir, role_basename = os.path.split(role_file)
    assert frame_dir == role_dir
    out_file = os.path.join(frame_dir, f"merge_{frame_basename}__{role_basename}")
    print("Out >>>", out_file)
    merge_prediction_files(frame_file, role_file, out_file)