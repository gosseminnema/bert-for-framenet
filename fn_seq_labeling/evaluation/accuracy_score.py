import sys
from typing import Tuple, Dict

import pandas as pd
from nltk.corpus import framenet as fn

from fn_seq_labeling.evaluation.eval_utils import get_prediction_sets, split_sentences


def fn_accuracy_score(prediction_df: pd.DataFrame = None,
                      prediction_file: str = None
                      ):
    if not (prediction_df or prediction_file):
        raise ValueError("Either a dataframe or a CSV file should be given")

    if prediction_file:
        prediction_df = pd.read_csv(prediction_file)

    scores = count_errors(prediction_df)
    consistent_roles, inconsistent_roles = count_role_consistencies(prediction_df)

    final_scores = {}
    for key in ["all", "roles", "frames"]:
        f1_score, precision, recall = calculate_scores(scores[key])
        final_scores[key] = {
            "f1": f1_score,
            "p": precision,
            "r": recall
        }
        print(f"---{key.upper()}---")
        print("R, P, F1 = \n", recall, "\t", precision, "\t", f1_score)

    # NB this is on role TYPE not TOKEN level!!!
    total_roles = consistent_roles + inconsistent_roles
    print("Total roles: ", total_roles)
    print("%consistent roles:", consistent_roles / total_roles if total_roles else 0.0)
    print("%inconsistent roles:", inconsistent_roles / total_roles if total_roles else 0.0)
    print()
    return final_scores


def count_role_consistencies(prediction_df):

    sentences = split_sentences(prediction_df)

    consistent_roles = 0
    inconsistent_roles = 0

    for sent_df in sentences:
        allowed_roles = set()
        found_roles = set()
        for i, row in sent_df.iterrows():
            predictions = row["prediction"].split("|")
            predictions_all, predictions_frames, predictions_roles = get_prediction_sets(predictions)
            for frame in predictions_frames:
                frame = frame.replace("T:", "")
                frame_roles = set(fn.frame(frame).FE)
                allowed_roles.update(frame_roles)
            for role in predictions_roles:
                found_roles.add(role)

        consistent = found_roles.intersection(allowed_roles)
        inconsistent = found_roles.difference(allowed_roles)
        consistent_roles += len(consistent)
        inconsistent_roles += len(inconsistent)

    return consistent_roles, inconsistent_roles


def count_errors(prediction_df):
    # init counters
    def init_counter_dict():
        return {
            "true_positives": 0,
            "false_positives": 0,
            "false_negatives": 0
        }

    scores = {
        "all": init_counter_dict(),
        "frames": init_counter_dict(),
        "roles": init_counter_dict()
    }

    for i, row in prediction_df.iterrows():
        # print(row)

        # get lists of labels
        predictions = row["prediction"].split("|")
        golds = row["gold"].split("|")

        # convert to sets and remove probabilities
        predictions_all, predictions_frames, predictions_roles = get_prediction_sets(predictions)
        predictions_dict = {"all": predictions_all, "frames": predictions_frames, "roles": predictions_roles}
        golds_all, golds_frames, golds_roles = get_prediction_sets(golds)
        golds_dict = {"all": golds_all, "frames": golds_frames, "roles": golds_roles}

        for key in ["all", "roles", "frames"]:
            scores[key]["true_positives"] += len(predictions_dict[key].intersection(golds_dict[key]))
            scores[key]["false_positives"] += len(predictions_dict[key].difference(golds_dict[key]))
            scores[key]["false_negatives"] += len(golds_dict[key].difference(predictions_dict[key]))

    return scores


def calculate_scores(scores: Dict[str, int]) -> Tuple[float, float, float]:
    try:
        recall = scores["true_positives"] / (scores["true_positives"] + scores["false_negatives"])
    except ZeroDivisionError:
        recall = 0.0
    try:
        precision = scores["true_positives"] / (scores["true_positives"] + scores["false_positives"])
    except ZeroDivisionError:
        precision = 0.0
    try:
        f1_score = 2 * (precision * recall) / (precision + recall)
    except ZeroDivisionError:
        f1_score = 0.0
    return f1_score, precision, recall


if __name__ == '__main__':

    # config = sys.argv[1]
    # threshold_postfix = "_" + sys.argv[2] if sys.argv[2] else ""
    #
    # files = [
    #     f"output/predictions/{config}_dev326{threshold_postfix}.csv",
    #     f"output/predictions/{config}_hartmann_dev712{threshold_postfix}.csv",
    #     f"output/predictions/{config}_tatoeba_dev70{threshold_postfix}.csv"
    # ]
    # for file in files:
    #     if sys.argv[3] == "y":
    #         file = file.replace(".csv", ".cleaned.csv")
    #     print(file)
    #     try:
    #         fn_accuracy_score(prediction_file=file)
    #         print()
    #     except FileNotFoundError:
    #         print("File does not exist! Rerunning evaluation script necessary\n")

    fn_accuracy_score(prediction_file=sys.argv[1])