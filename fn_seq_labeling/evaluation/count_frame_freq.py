from collections import Counter
from typing import List

from nltk.corpus import framenet as fn
import pandas as pd

from fn_seq_labeling.multilabel.sesame2multilabel import read_txt_corpus


FREQ_THRESHOLD = 10


def main():

    freq_counters = {
        "fulltext_train": count_frequencies("data/multilabel/fn1.7.fulltext.train.syntaxnet.txt"),
        "fulltext_dev": count_frequencies("data/multilabel/fn1.7.dev.syntaxnet.txt"),
        "exemplars": count_frequencies("data/multilabel/fn1.7.exemplar.train.syntaxnet.filtered.txt"),
    }

    df = pd.DataFrame(freq_counters.values(), index=list(freq_counters.keys())).T
    df.to_csv("output/freq_analysis/frame_freqs.csv")
    rare_frames = find_rare_frames(df)
    with open("output/freq_analysis/rare_frames.txt", "w", encoding="utf-8") as f:
        for frame in rare_frames:
            f.write(f"{frame}\n")


def find_rare_frames(df: pd.DataFrame) -> List[str]:

    rare_frames = []

    for name, row in df.iterrows():
        if row["fulltext_train"] < FREQ_THRESHOLD:
            rare_frames.append(str(name))

    return rare_frames


def count_frequencies(txt_corpus):

    all_frame_names = [f"T:{f.name}" for f in fn.frames()]
    freq_counter = Counter({f: 0 for f in all_frame_names})
    with open(txt_corpus, encoding="utf-8") as f:
        corpus = read_txt_corpus(f)

        for i, annotation in enumerate(corpus):

            if i % 1000 == 0:
                print(f"{i}...", end="", flush=True)

            for frames in annotation.frame_list:
                for frame_annotation in frames:
                    if frame_annotation.startswith("T:"):
                        freq_counter[frame_annotation] += 1
    return freq_counter


if __name__ == '__main__':
    main()
