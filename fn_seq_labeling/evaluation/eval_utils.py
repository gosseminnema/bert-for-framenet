from collections import defaultdict
from typing import Dict, List, Optional

import pandas as pd


def get_prediction_sets(predictions):
    predictions_all = {
        "T:" + p.split(":")[1] if p.startswith("T:") else p.split(":")[0]
        for p in predictions
    }
    predictions_roles = {
        p.split(":")[0] for p in predictions if not p.startswith("T:") and p != "_"
    }
    predictions_frames = {
        "T:" + p.split(":")[1] for p in predictions if p.startswith("T:")
    }
    return predictions_all, predictions_frames, predictions_roles


def align_sentence_dfs(sentences_1, sentences_2):
    aligned_sentences: Dict[str, List[Optional[pd.DataFrame]]] = defaultdict(lambda: [None, None])
    for i, sentences_x in enumerate([sentences_1, sentences_2]):
        for df in sentences_x:
            aligned_sentences[" ".join([str(t) for t in df["token"].tolist()])][i] = df
    return aligned_sentences


def split_sentences(prediction_df) -> List[pd.DataFrame]:
    # check for leading extra column
    if "Unnamed: 0" in prediction_df.columns:
        del prediction_df["Unnamed: 0"]

    sentence_dfs = []
    sent_start = 0
    for i, row in prediction_df.iterrows():
        if row["token"] == "[CLS]":
            sent_start = i
        if row["token"] == "[SEP]":
            sent_end = i + 1
            sentence_dfs.append(prediction_df.iloc[sent_start:sent_end])
    return sentence_dfs
