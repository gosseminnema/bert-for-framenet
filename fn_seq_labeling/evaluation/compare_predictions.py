import json
import os
import sys
from itertools import zip_longest

import pandas as pd

from fn_seq_labeling.evaluation.accuracy_score import count_errors, calculate_scores
from fn_seq_labeling.evaluation.eval_utils import align_sentence_dfs, split_sentences


def main(file_1, file_2):

    pred_df_1 = pd.read_csv(file_1)
    pred_df_2 = pd.read_csv(file_2)
    compare_json, compare_df = compare_predictions(pred_df_1, pred_df_2)
    compare_json.sort(key=lambda sent: sent["f1_diff"])

    basename_1 = os.path.splitext(os.path.basename(file_1))[0]
    basename_2 = os.path.splitext(os.path.basename(file_2))[0]
    out_name = f"output/prediction_comparisons/{basename_1}__vs__{basename_2}"
    compare_df.to_csv(f"{out_name}.csv")
    with open(f"{out_name}.json", "w", encoding="utf-8") as f:
        json.dump(compare_json, f, indent=4)


def compare_predictions(pred_df_1, pred_df_2):

    sentences_1 = split_sentences(pred_df_1)
    sentences_2 = split_sentences(pred_df_2)

    aligned_sentences = align_sentence_dfs(sentences_1, sentences_2)

    csv_comparisons = []
    json_comparisons = []

    for sentence in aligned_sentences:
        sent_df_1, sent_df_2 = aligned_sentences[sentence]

        if sent_df_1 is None or sent_df_2 is None:
            continue

        if sent_df_1 is not None:
            errors_1 = count_errors(sent_df_1)
            f1_1, prec_1, rec_1 = calculate_scores(errors_1["all"])
        else:
            f1_1, prec_1, rec_1 = 0., 0., 0.
        if sent_df_2 is not None:
            errors_2 = count_errors(sent_df_2)
            f1_2, prec_2, rec_2 = calculate_scores(errors_2["all"])
        else:
            f1_2, prec_2, rec_2 = 0., 0., 0.
        f1_diff = f1_1 - f1_2

        tokens = sent_df_1["token"].tolist() if sent_df_1 is not None else sent_df_2["token"].tolist()

        json_comparisons.append({
            "annotations": [
                {
                    "token": token,
                    "pred_1": pred_1,
                    "pred_2": pred_2,
                    "gold": gold
                }
                for token, pred_1, pred_2, gold in zip(
                    tokens,
                    sent_df_1["prediction"].tolist() if sent_df_1 is not None else [],
                    sent_df_2["prediction"].tolist() if sent_df_2 is not None else [],
                    sent_df_1["gold"].tolist() if sent_df_1 is not None else sent_df_2["gold"].tolist()
                )],
            "scores_1": {"f1": f1_1, "prec": prec_1, "rec": rec_1},
            "scores_2": {"f1": f1_2, "prec": prec_2, "rec": rec_2},
            "f1_diff": f1_diff
        })

        df_1_rows = [row for _, row in sent_df_1.iterrows()] if sent_df_1 is not None else []
        df_2_rows = [row for _, row in sent_df_2.iterrows()] if sent_df_2 is not None else []
        for row_1, row_2 in zip_longest(df_1_rows, df_2_rows, fillvalue=None):
            csv_comparisons.append({
                "token": row_1["token"] if row_1 is not None else row_2["token"],
                "pred_1": row_1["prediction"] if row_1 is not None else "_",
                "pred_2": row_2["prediction"] if row_2 is not None else "_",
                "gold": row_1["gold"] if row_1 is not None else row_2["gold"]
            })

    return json_comparisons, pd.DataFrame(csv_comparisons)


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])