import bs4


def main():

    with open("output/full_structure_predictions/"
              "2020-04-15_frames_and_roles_layer10_dev326_0.3.semeval", encoding="utf-8") as f_pred, \
            open("data/fn1.7/semeval/dev.gold.xml", encoding="utf-8") as f_gold:

        pred_soup = bs4.BeautifulSoup(f_pred.read(), features="lxml")
        gold_soup = bs4.BeautifulSoup(f_gold.read(), features="lxml")

        pred_sentences = pred_soup.find_all("sentence")
        gold_sentences = gold_soup.find_all("sentence")

        error_count = 0

        for ps, gs in zip(pred_sentences, gold_sentences):
            ps_text = ps.find("text").text
            gs_text = gs.find("text").text
            if ps_text != gs_text:
                error_count += 1
                print("Pred_sent: ", ps_text)
                print("Gold_sent: ", gs_text)
                print()

        print("Errors: ", error_count)


if __name__ == '__main__':
    main()
