import collections
import glob
import os
import re

import numpy as np
import pandas as pd

from fn_seq_labeling.evaluation.accuracy_score import fn_accuracy_score


def evaluate_fchecks_grid(predicate_filter=None, dev_options=None):

    if not predicate_filter:
        predicate_filter = "**"
    else:
        predicate_filter = predicate_filter.replace(".", "_").replace(" ", "_")

    if not dev_options:
        dev_options = ["fn_dev", "x_dev", "g_dev"]

    score_grid = build_score_grid(dev_options, predicate_filter)
    score_grid_to_xlsx("+".join(dev_options), predicate_filter, score_grid)


def score_grid_to_xlsx(dev_option_name, predicate_filter, score_grid):
    with pd.ExcelWriter(f"output/fchecks_score_grid_{dev_option_name}_{predicate_filter}.xlsx") as excel:
        for predicate, pred_score_grid in score_grid.items():
            for dev_opt, dev_opt_grid in pred_score_grid.items():
                predicate_rows = []
                for pct, pct_score_grid in dev_opt_grid.items():
                    cond_avg_scores = {}
                    for cond, cond_grid in pct_score_grid.items():
                        if any(i is None for i in cond_grid):
                            rep_mean = np.nan
                        else:
                            rep_mean = np.mean(cond_grid)
                        cond_avg_scores[cond] = rep_mean
                    predicate_rows.append({
                        "ftrain_ratio": pct,
                        "baseline_avg": cond_avg_scores["baseline"],
                        "x_train_avg": cond_avg_scores["x_train"],
                        "g_train_avg": cond_avg_scores["g_train"],
                        "g_x_train_avg": cond_avg_scores["g_x_train"],
                        "baseline_reps": str(pct_score_grid["baseline"]),
                        "x_train_reps": str(pct_score_grid["x_train"]),
                        "g_train_reps": str(pct_score_grid["g_train"]),
                        "g_x_train_reps": str(pct_score_grid["g_x_train"])
                    })
                predicate_df = pd.DataFrame(predicate_rows)
                predicate_df.to_excel(excel_writer=excel, sheet_name=f"{predicate}_{dev_opt}")


def build_score_grid(dev_options, predicate_filter):
    score_grid = collections.defaultdict(
        lambda: collections.defaultdict(
            lambda: collections.defaultdict(
                lambda: {condition: [None] * 3 for condition in FCHECKS_CONDITIONS}
            )
        )
    )

    for config_file in sorted(glob.glob(f"configs/frameid-checks/{predicate_filter}/**/*.json", recursive=True)):
        config_name = os.path.splitext(os.path.split(config_file)[1])[0]

        for dev_option in dev_options:

            if dev_option == "fn_dev":
                prediction_csv_path = config_name + "_dev387.csv"
            else:
                prediction_csv_path = config_name + f"_ext_{predicate_filter}_{dev_option}_*"
            print(prediction_csv_path)
            prediction_csv_files = glob.glob(
                os.path.join("output", "predictions", prediction_csv_path))
            print("prediction_csv_files", prediction_csv_files)

            # todo: why >= 1? not == 1?
            if len(prediction_csv_files) >= 1:
                prediction_csv = prediction_csv_files[0]
            else:
                continue
            print("prediction_csv", prediction_csv)
            if os.path.isfile(prediction_csv):
                scores = fn_accuracy_score(prediction_file=prediction_csv)
                frame_score = scores["frames"]["f1"]
            else:
                print(f"No predictions found for config {config_name}")
                frame_score = np.nan
            m = re.match(r"^(\w+?)_(baseline|x_train|g_x_train|g_train)_(\d+)p_rep(\d)$", config_name)
            predicate_name = m.group(1)
            train_mode = m.group(2)
            train_portion = float(m.group(3)) / 100
            rep = int(m.group(4))

            # check for double entries
            if score_grid[predicate_name][dev_option][train_portion][train_mode][rep - 1] is not None:
                raise ValueError("Score grid entry already filled!")
            score_grid[predicate_name][dev_option][train_portion][train_mode][rep - 1] = frame_score

    return score_grid


FCHECKS_CONDITIONS = ("baseline", "x_train", "g_train", "g_x_train")