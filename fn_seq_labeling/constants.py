import enum

# DATA
OPEN_SESAME_TRAINING_DATA = "data/fn1.7/sesame/"
OPEN_SESAME_OUTPUT_DATA = "data/sesame_output/"
SIMPLEFRAMEID_OUTPUT_DATA = "data/simpleframeid_output"
TATOEBA_ANNOTATION_DATA = "data/tatoeba_annotations/"
MULTILABEL_FN_CORPUS = "data/multilabel"
HARTMANN_CORPUS = "data/hartmann_data"

# EMBEDDING FILE NAMES
FRAME_NAME_EMBEDDING_FILE = "data/embeddings/frame_name_embeddings.txt"
BAG_OF_LU_EMBEDDING_FILE = "data/embeddings/bag_of_lu_embeddings.txt"

# --- CONFIG FILE KEYS ---
# Language (EN/NL)
LANGUAGE = "language"

# Exemplar sentences
INCLUDE_EXEMPLARS = "include_exemplars"
MERGE_EXEMPLARS = "merge_exemplars"
SILVER_EXEMPLARS = "silver_exemplars"
SILVER_DATA_POSTFIX = "silver_data_postfix"
CLIP_EXEMPLARS = "clip_exemplars"
CLIP_ONLY_FRAMES = "clip_only_frames"
CLIP_WINDOW = "clip_window"
REMOVE_FULLTEXT_TRAIN = "remove_fulltext_train"
ONLY_RARE_EXEMPLARS = "only_rare_exemplars"
FULLTEXT_EXEMPLAR_RATIO = "fulltext_exemplar_ratio"

# BERT model settings
FINE_TUNE_BERT_CONFIG = "fine_tune_bert"
USE_ONLY_TOP_BERT_LAYER = "use_only_top_bert_layer"
BERT_SCALAR_MIX = "bert_scalar_mix"
MULTILINGUAL_BERT = "multilingual_bert"

# Label settings
MULTILABEL_SETTING_CONFIG = "multilabel_setting"
INCLUDE_FRAMES = "include_frames"

# Decoder settings
LEARNING_RATE = "learning_rate"
EPOCHS = "epochs"
DECODER = "decoder"
HIDDEN_SIZE = "hidden_size"
LABEL_EMBEDDING_DIMENSIONS = "pca_dimensions"
PCA_ALGORITHM = "pca_algorithm"

# Embedding settings
EMBED_FRAME_LABELS = "embed_labels"
COSINE_LOSS = "cosine_loss"
BINARY_MARKER_IN_FRAME_EMBEDDING = "binary_marker_in_embedding"

# Multitask settings
MULTITASK = "multitask"
ROLE_LOSS_WEIGHT = "role_loss_weight"
FRAME_LOSS_WEIGHT = "frame_loss_weight"

# Frame context vectors as extra input
ADD_FRAME_CONTEXT_VECS = "add_frame_context_vecs"
FRAME_CONTEXT_NET = "frame_context_net"

# Do separate predictions for frames
SPLIT_BY_FRAMES = "split_by_frames"
INPUT_FRAME_PREDICTIONS = "input_frame_predictions"
INFER_EXTRA_FRAME_PREDICTIONS = "infer_extra_frame_predictions"

# Add role context vectors
ADD_ROLES_CONTEXT_VECS = "add_roles_context_vecs"
INPUT_ROLES_PREDICTIONS = "input_roles_predictions"
COMBINE_ROLES_CONTEXT_VECS = "combine_roles_context_vecs"

# Predict "super FEs" (map FEs to superframe FEs)
SUPER_FE = "super_fe"

# Run on non-initialized embeddings
RUN_WITHOUT_BERT = "run_without_bert"

# Use only a subset of available training data
USE_TRAIN_DATA_PORTION = "use_train_data_portion"
SHUFFLE_BEFORE_PORTIONING = "shuffle_before_portioning"
RANDOM_SEED = "random_seed"

# Extra training data
EXTRA_TRAIN_DATA_FILES = "extra_train_data_files"

# For Sonar-NL corpus
SONAR_FN_ANNOTATOR = "sonar_fn_annotator"


# CONFIG FILE VALUES
# Possible values for MULTILABEL_SETTING_CONFIG
class MultilabelSetting(enum.Enum):
    ONLY_FIRST = "only_first"
    CONCATENATE = "concatenate"
    BINARIZED = "binarized"
    BINARIZED_PCA = "binarized_pca"


# Group of MultilabelSetting values with binarization
CONFIGS_WITH_BINARIZATION = [
    MultilabelSetting.BINARIZED,
    MultilabelSetting.BINARIZED_PCA
]


# Possible values for PCA_ALGORITHM
class PcaAlgorithm(enum.Enum):
    PCA = "pca"
    SVD = "truncated_svd"


# Possible values for INCLUDE_FRAMES
class TargetLabelSetting(enum.Enum):
    ROLES_WITHOUT_FRAMES = "none"
    TARGETS_AND_ROLES_WITH_FRAMES = "targets_and_roles"
    TARGETS_AND_ROLES_WITHOUT_FRAMES = "targets_only"
    TARGETS_WITHOUT_ROLES = "frames_only"
    TARGETS_WITHOUT_ROLES_OR_NAMES = "targetid"


# Possible values for DECODER
class DecoderSetting(enum.Enum):
    LINEAR = "linear"
    MLP = "mlp"


# Possible values for EMBED_LABELS
class FrameLabelEmbeddingSetting(enum.Enum):
    NONE = "none"
    BAG_OF_LU = "bag_of_lu"
    FRAME_NAMES = "frame_names"


class Languages(enum.Enum):
    ENGLISH = "en"
    DUTCH = "nl"


class SonarAnnotators(enum.Enum):
    A1 = "a1"
    A2 = "a2"


# Map keys to value enums
ENUM_SETTINGS = {
    MULTILABEL_SETTING_CONFIG: MultilabelSetting,
    PCA_ALGORITHM: PcaAlgorithm,
    INCLUDE_FRAMES: TargetLabelSetting,
    DECODER: DecoderSetting,
    EMBED_FRAME_LABELS: FrameLabelEmbeddingSetting,
    LANGUAGE: Languages,
    SONAR_FN_ANNOTATOR: SonarAnnotators
}

SETTING_DEFAULTS = {
    LANGUAGE: Languages.ENGLISH,

    INCLUDE_EXEMPLARS: False,
    MERGE_EXEMPLARS: False,
    SILVER_EXEMPLARS: False,
    SILVER_DATA_POSTFIX: "_emb10_0.1",
    CLIP_EXEMPLARS: False,
    CLIP_ONLY_FRAMES: False,
    CLIP_WINDOW: 2,
    REMOVE_FULLTEXT_TRAIN: False,
    ONLY_RARE_EXEMPLARS: False,
    FULLTEXT_EXEMPLAR_RATIO: 1,

    FINE_TUNE_BERT_CONFIG: False,
    USE_ONLY_TOP_BERT_LAYER: False,
    BERT_SCALAR_MIX: None,
    MULTILABEL_SETTING_CONFIG: MultilabelSetting.BINARIZED,
    LABEL_EMBEDDING_DIMENSIONS: 300,  # only when dimensionality reduction is done
    PCA_ALGORITHM: PcaAlgorithm.PCA,
    INCLUDE_FRAMES: TargetLabelSetting.TARGETS_AND_ROLES_WITHOUT_FRAMES,
    EMBED_FRAME_LABELS: FrameLabelEmbeddingSetting.NONE,

    # multitask settings
    MULTITASK: False,
    ROLE_LOSS_WEIGHT: 1.0,
    FRAME_LOSS_WEIGHT: 1.0,

    # decoder settings
    LEARNING_RATE: 1e-3,
    EPOCHS: 3,
    DECODER: DecoderSetting.LINEAR,
    HIDDEN_SIZE: 100,  # only for MLP decoder
    COSINE_LOSS: False,  # only when predicting embeddings
    BINARY_MARKER_IN_FRAME_EMBEDDING: True,  # only when predicting embeddings,

    # frame context vectors
    ADD_FRAME_CONTEXT_VECS: False,
    FRAME_CONTEXT_NET: None,

    ADD_ROLES_CONTEXT_VECS: False,
    INPUT_ROLES_PREDICTIONS: None,
    COMBINE_ROLES_CONTEXT_VECS: False,

    # frame-by-frame setup
    SPLIT_BY_FRAMES: False,
    INPUT_FRAME_PREDICTIONS: None,
    INFER_EXTRA_FRAME_PREDICTIONS: False,

    SUPER_FE: False,

    RUN_WITHOUT_BERT: False,

    USE_TRAIN_DATA_PORTION: 1.0,
    SHUFFLE_BEFORE_PORTIONING: False,
    RANDOM_SEED: None,

    EXTRA_TRAIN_DATA_FILES: [],

    MULTILINGUAL_BERT: False,

    SONAR_FN_ANNOTATOR: SonarAnnotators.A1
}

