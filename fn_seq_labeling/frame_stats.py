import collections
from typing import Dict, List

from nltk.corpus import framenet as fn
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams.update({'figure.autolayout': True})

TOP_N = 20


def main():
    roles_to_frames: Dict[str, List[str]] = collections.defaultdict(list)
    for frame in fn.frames():
        for role in frame.FE:
            roles_to_frames[role].append(frame.name)

    roles_and_frames = sorted(roles_to_frames.items(), key=lambda kvp: len(kvp[1]), reverse=True)
    print({r: f[:10] for r, f in roles_and_frames[:10]})

    freq_dist = []
    for role, frames in roles_and_frames:
        freq_dist.append({"role": role, "n_frames": len(frames)})
    freq_df = pd.DataFrame(freq_dist)
    pd.concat([
        freq_df[:TOP_N],
        # pd.DataFrame([{"role": "...", "n_frames": 0}]),
        # freq_df[-TOP_N:]
    ]).iloc[::-1].plot.barh(x="role", figsize=(4, 3))

    plt.savefig("output/figures/roles_to_frames.pdf")


if __name__ == '__main__':
    main()
