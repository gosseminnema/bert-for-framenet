from typing import Dict, Optional

import torch
from allennlp.data import Vocabulary
from allennlp.models import Model
from allennlp.modules import TokenEmbedder
from allennlp.nn.util import get_text_field_mask, sequence_cross_entropy_with_logits
from torch import nn as nn

from fn_seq_labeling import constants as cns


class FrameNetDecoder(nn.Module):
    COSINE_TARGET = torch.tensor([1]).cuda(0) if torch.cuda.is_available() else torch.tensor([1])

    def __init__(self, decoder_output_size: int,
                 multilabel_setting: cns.MultilabelSetting,
                 model_type: cns.DecoderSetting,
                 encoder_output_size: int = 768,
                 hidden_size: int = 100,
                 embed_labels: bool = False,
                 binary_marker_in_embedding: bool = True,
                 cosine_loss: bool = False,
                 name: str = "decoder",
                 multitask: Optional[str] = None,
                 add_frame_context_vecs: bool = False,
                 frame_context_vec_size: int = 300,
                 frame_context_net: Optional[int] = None,
                 add_roles_context_vecs = False,
                 roles_context_vec_size = 1285
                 ):
        """
        :param add_roles_context_vecs:
        :param decoder_output_size: output size of the decoder
        :param multilabel_setting: binarized or single output
        :param model_type: linear model or multi-layer perceptron?
        :param encoder_output_size: output size of the encoder, typically BERT output size (768)
        :param hidden_size: size of the hidden layer, if any
        :param embed_labels: bool, if True, outputs are embeddings
        :param binary_marker_in_embedding: bool, if True, add an extra (binary) dimension that is 0 for null outputs
        :param cosine_loss: bool, use CosineEmbeddingLoss or not
        :param name: string naming the decoder, useful for debugging multitask models
        """
        super().__init__()

        self.multilabel_setting = multilabel_setting
        self.decoder_setting = model_type
        self.hidden_size = hidden_size
        self.embed_labels = embed_labels
        self.binary_marker_in_embedding = binary_marker_in_embedding
        self.cosine_loss = cosine_loss
        self.out_features = decoder_output_size
        self.multitask = multitask
        self.name = name

        input_features = encoder_output_size
        if add_frame_context_vecs:
            if frame_context_net is not None:
                input_features += frame_context_net
            else:
                input_features += frame_context_vec_size
        if add_roles_context_vecs:
            input_features += roles_context_vec_size

        # define the model itself
        if model_type == cns.DecoderSetting.LINEAR:
            self.layers = nn.Sequential(
                nn.Linear(in_features=input_features,
                          out_features=self.out_features)
            )
        else:
            self.layers = nn.Sequential(
                nn.Linear(in_features=input_features,
                          out_features=self.hidden_size),
                nn.ReLU(),
                nn.Linear(in_features=self.hidden_size,
                          out_features=self.out_features)
            )

        if frame_context_net is not None:
            self.frame_context_net = nn.Sequential(
                nn.Linear(in_features=frame_context_vec_size,
                          out_features=frame_context_net),
                nn.ReLU(),
                nn.Linear(in_features=frame_context_net,
                          out_features=frame_context_net)
            )
        else:
            self.frame_context_net = None

        print(self.layers)
        if self.frame_context_net:
            print("Frame context network")
            print(self.frame_context_net)

    def forward(self, embeddings, labels, mask, frame_context_vector, roles_context_vector):

        # compute outputs
        decoder_input = embeddings
        if frame_context_vector is not None:
            if self.frame_context_net is None:
                decoder_input = torch.cat([decoder_input, frame_context_vector], dim=-1)
            else:
                transformed_frame_context_vec = self.frame_context_net(frame_context_vector)
                decoder_input = torch.cat([decoder_input, transformed_frame_context_vec], dim=-1)
        if roles_context_vector is not None:
            decoder_input = torch.cat([decoder_input, roles_context_vector], dim=-1)
        tag_logits = self.layers(decoder_input)

        # reshape mask if necessary
        if self.embed_labels != cns.FrameLabelEmbeddingSetting.NONE \
                or self.multilabel_setting in cns.CONFIGS_WITH_BINARIZATION:
            mask = mask.reshape(mask.shape[0], mask.shape[1], 1).repeat(1, 1, self.out_features)

        # compute loss
        loss = None
        if labels is not None:
            try:
                loss = self._compute_loss(labels, mask, tag_logits)
            except RuntimeError as e:
                print(f"Error computing loss in decoder {self.name}")
                raise e

        return tag_logits, loss

    def _compute_loss(self, labels, mask, tag_logits):

        # print(f"Choosing loss function for mt setting: {self.multitask}")

        # for frame-only prediction in multitask setting
        if self.multitask == "frames":
            # embeddings, cosine loss
            if self.cosine_loss:
                loss = nn.functional.cosine_embedding_loss(tag_logits, labels, self.COSINE_TARGET)

            # embeddings, no cosine loss -> MSE loss
            elif self.embed_labels != cns.FrameLabelEmbeddingSetting.NONE:
                loss = nn.functional.mse_loss(tag_logits, labels)

            # sparse, single labels -> seq x-entropy loss
            else:
                # print(f"tag_logits.shape={tag_logits.shape}")
                # print(f"labels.shape={labels.shape}")
                loss = sequence_cross_entropy_with_logits(tag_logits, labels, mask)

        # for role-only prediction in multitask setting
        elif self.multitask == "roles":
            if self.multilabel_setting == cns.MultilabelSetting.BINARIZED:
                loss = nn.functional.binary_cross_entropy_with_logits(tag_logits, labels, mask)
            elif self.multilabel_setting == cns.MultilabelSetting.BINARIZED_PCA:
                loss = nn.functional.mse_loss(tag_logits, labels)
            else:
                raise ValueError("Illegal multilabel_setting for decoding roles")

        # for single-task setting
        else:
            if self.cosine_loss:
                # assert self.embed_labels, "Cosine embedding loss should only be used when predicting label embeddings"
                loss = nn.functional.cosine_embedding_loss(tag_logits, labels, self.COSINE_TARGET)
            elif self.multilabel_setting == cns.MultilabelSetting.BINARIZED:
                loss = nn.functional.binary_cross_entropy_with_logits(tag_logits, labels, mask)
            elif self.multilabel_setting == cns.MultilabelSetting.BINARIZED_PCA:
                loss = nn.functional.mse_loss(tag_logits, labels)
            elif self.embed_labels != cns.FrameLabelEmbeddingSetting.NONE:
                loss = nn.functional.mse_loss(tag_logits, labels)
            else:
                loss = sequence_cross_entropy_with_logits(tag_logits, labels, mask)

        return loss


class BertFnTagger(Model):
    """
    Model that computes BERT representations for a sequence of (BPE) tokens,
    and then maps these to one of the following, using different `FrameNetDecoder` models:
    - Sequence of single labels (usually frame name labels)
    - Sequence of embeddings representing single labels
    - Sequence of binary vectors representing multiple labels (role labels or combined role + frame name labels)
    - Sequence of binary vectors reduced with PCA
    """

    def __init__(self,
                 word_embeddings: TokenEmbedder,
                 vocab: Vocabulary,
                 decoder: FrameNetDecoder
                 ):
        super().__init__(vocab)
        self.word_embeddings = word_embeddings
        self.decoder = decoder

    def forward(self,
                sentence: Dict[str, torch.FloatTensor],
                labels: Optional[torch.FloatTensor] = None,
                original_labels: Optional[torch.FloatTensor] = None,
                use_original_labels: bool = False,
                frame_context_vector: Optional[torch.FloatTensor] = None,
                roles_context_vector: Optional[torch.FloatTensor] = None
                ) -> Dict[str, torch.Tensor]:
        """
        Embed the sentence and run it through the decoder
        :param sentence: token tensor dict, should have single key "tokens"
        :param labels: label tensor, for single-task setup only
        :param original_labels: unused
        :param use_original_labels: unused
        :param frame_context_vector: TODO add docstring
        :param roles_context_vector: TODO add docstring
        :return:
        """
        embeddings = self.word_embeddings(sentence["tokens"])
        mask = get_text_field_mask(sentence).float()
        tag_logits, loss = self.decoder(embeddings, labels, mask, frame_context_vector, roles_context_vector)

        output = {"tag_logits": tag_logits}
        if loss is not None:
            output["loss"] = loss
        return output

    def get_metrics(self, reset: bool = False) -> Dict[str, float]:
        return {}


class BertFnMultiTaskLabeler(Model):

    def __init__(self,
                 vocab: Vocabulary,
                 word_embeddings: TokenEmbedder,
                 frame_decoder: FrameNetDecoder,
                 role_decoder: FrameNetDecoder,
                 role_loss_weight: float,
                 frame_loss_weight: float,
                 add_frame_context_vecs: bool = False
                 ):
        super().__init__(vocab)
        self.word_embeddings = word_embeddings
        self.frame_decoder = frame_decoder
        self.role_decoder = role_decoder
        if role_loss_weight is None:
            self.role_loss_weight = nn.Parameter(torch.tensor(1.0), True)
        else:
            self.role_loss_weight = role_loss_weight
        if frame_loss_weight is None:
            self.frame_loss_weight = nn.Parameter(torch.tensor(1.0), True)
        else:
            self.frame_loss_weight = frame_loss_weight

    def forward(self,
                sentence: Dict[str, torch.FloatTensor],
                frame_labels: Optional[torch.FloatTensor] = None,
                original_frame_labels: Optional[torch.FloatTensor] = None,
                role_labels: Optional[torch.FloatTensor] = None,
                original_role_labels: Optional[torch.FloatTensor] = None,
                frame_context_vector: Optional[torch.FloatTensor] = None,
                use_original_labels: bool = False
                ) -> Dict[str, torch.Tensor]:
        embeddings = self.word_embeddings(sentence["tokens"])
        mask = get_text_field_mask(sentence).float()
        frame_tag_logits, frame_loss = self.frame_decoder(embeddings, frame_labels, mask, None, None)
        role_tag_logits, role_loss = self.role_decoder(embeddings, role_labels, mask, frame_context_vector, None)

        output = {
            "frame_tag_logits": frame_tag_logits,
            "role_tag_logits": role_tag_logits,
        }

        if frame_loss is not None:
            loss = self.frame_loss_weight * frame_loss + self.role_loss_weight * role_loss
            output["loss"] = loss

        return output
