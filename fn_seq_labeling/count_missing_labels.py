def main():

    missing_tokens = set()

    with open("data/hartmann_data/hartmann_missing_labels.txt", encoding="utf-8") as f:
        for line in f:
            if not line.startswith("Token: "):
                continue
            token = line.replace("Token: ", "")
            missing_tokens.add(token.strip())

    print("Missing tokens:")
    for token in missing_tokens:
        print(token)
    print()
    print("Total missing", len(missing_tokens))
    print("Frames:", len({t for t in missing_tokens if t.startswith("T:")}))


if __name__ == '__main__':
    main()
