import os
import unittest

import pandas as pd
import torch
from allennlp.data import Vocabulary
from allennlp.data.fields import TextField
from allennlp.data.token_indexers import SingleIdTokenIndexer
from allennlp.data.tokenizers import Token
from sklearn.preprocessing import MultiLabelBinarizer

from nltk.corpus import framenet as fn

import fn_seq_labeling.multilabel.frame_annotations
from fn_seq_labeling.multilabel import sesame2multilabel
from fn_seq_labeling.datasets import FrameNetDatasetReader, init_bert_tokenizer, bert_tokenize, retokenize_tags
from fn_seq_labeling.label_fields import SequenceMultiLabelField
from fn_seq_labeling.postprocess import Postprocessor
from fn_seq_labeling.roles2frames import FrameFinder, load_role_data


class LabelConversionTest(unittest.TestCase):
    def test_sesame2multilabel(self):
        mock_sesame_anno = [
            fn_seq_labeling.multilabel.frame_annotations.OpenSesameAnnotation(
                tokens=["hello", "world"],
                pos=["tag1", "tag2"],
                frame=["Greeting", None],
                lu=["hello.ij", None],
                role_name=["Greeting", None],
                role_iob=["B", "O"]
            ),
            fn_seq_labeling.multilabel.frame_annotations.OpenSesameAnnotation(
                tokens=["hello", "world"],
                pos=["tag1", "tag2"],
                frame=[None, "Planet"],
                lu=[None, "world.n"],
                role_name=["Statement", "PlanetName"],
                role_iob=["B", "B"]
            )
        ]
        multilabel = sesame2multilabel.sesame_to_multilabel(mock_sesame_anno)
        expected_multilabel = fn_seq_labeling.multilabel.frame_annotations.MultiLabelAnnotation(
            tokens=["hello", "world"],
            pos=["tag1", "tag2"],
            lu_list=["hello.ij", "world.n"],
            frame_list=[
                ["T:Greeting", "B:Greeting:Greeting", "B:Planet:Statement"],  # hello
                ["T:Planet", "B:Planet:PlanetName"]  # world
            ]
        )

        # check on the object level
        self.assertEqual(expected_multilabel, multilabel)

        # check on the string level
        self.assertEqual("hello tag1 T:Greeting|B:Greeting:Greeting|B:Planet:Statement hello.ij",
                         next(multilabel.to_txt())
                         )

    def test_text_to_data(self):
        text = ("hello tag1 T:Greeting|B:Greeting:Greeting|B:Planet:Statement hello.ij" + os.linesep
                + os.linesep
                + "hello tag1 T:Greeting|B:Greeting:Greeting|B:Planet:Statement hello.ij"
                ).strip().split(os.linesep)
        print(text)

        expected = [
            fn_seq_labeling.multilabel.frame_annotations.MultiLabelAnnotation(
                tokens=["hello"],
                pos=["tag1"],
                lu_list=["hello.ij"],
                frame_list=[
                    ["T:Greeting", "B:Greeting:Greeting", "B:Planet:Statement"]  # hello
                ]
            ),
            fn_seq_labeling.multilabel.frame_annotations.MultiLabelAnnotation(
                tokens=["hello"],
                pos=["tag1"],
                lu_list=["hello.ij"],
                frame_list=[
                    ["T:Greeting", "B:Greeting:Greeting", "B:Planet:Statement"]  # hello
                ]
            )
        ]
        self.assertEqual(expected, list(sesame2multilabel.read_txt_corpus(text)))

    def test_label_set(self):
        annotation = fn_seq_labeling.multilabel.frame_annotations.MultiLabelAnnotation(
            tokens=["hello", "world"],
            pos=["tag1", "tag2"],
            lu_list=["hello.ij", "world.n"],
            frame_list=[
                ["T:Greeting", "S:Greeting:Greeting", "S:Planet:Statement"],  # hello
                ["T:Planet", "S:Planet:PlanetName"]  # world
            ]
        )
        self.assertEqual(
            {"T:Greeting", "S:Greeting:Greeting", "S:Planet:Statement", "T:Planet", "S:Planet:PlanetName"},
            annotation.get_label_set()
        )


class LabelRepresentationTest(unittest.TestCase):

    def test_sequence_multilabel_field(self):
        # setup a mock corpus
        sentence_corpus = [
            ["Hello", "world"],
            ["Hi", "again"]
        ]
        label_corpus = [
            [
                [],
                ["LABEL:1", "LABEL:3"]
            ],
            [
                ["LABEL:2", "LABEL:1", "LABEL:3"],
                ["LABEL:3"]
            ]
        ]
        label_set = ["LABEL:1", "LABEL:2", "LABEL:3"]

        # setup vocabulary
        vocab = Vocabulary(tokens_to_add={
            "labels": label_set
        })

        # setup binarizer
        binarizer = MultiLabelBinarizer()
        binarizer.fit([[vocab.get_token_index(label, "labels") for label in label_set]])

        # get example sentence
        sentence = sentence_corpus[0]
        label = label_corpus[0]
        sentence_field = TextField([Token(word) for word in sentence], {"tokens": SingleIdTokenIndexer()})
        label_field = SequenceMultiLabelField(label, sentence_field, binarizer)

        # test indexing
        label_field.index(vocab)
        self.assertEqual([[], [0, 2]], label_field._indexed_labels)

        # test binarizing
        tensor = label_field.as_tensor({"num_tokens": 3})
        expected_tensor = torch.tensor([
            [0, 0, 0],
            [1, 0, 1],
            [0, 0, 0]
        ])
        self.assertTrue(torch.eq(expected_tensor, tensor).all())


class BertModelTest(unittest.TestCase):

    def test_tokenization(self):
        tokenizer = init_bert_tokenizer()
        word_tokens = "hello , I am Gosse".split()
        wp_tokens, offsets = bert_tokenize(word_tokens, tokenizer)

        expected_wp_tokens = [
            "[CLS]", "hello", ",", "I", "am", "Go", "##sse", "[SEP]"
        ]
        expected_offsets = [
            (1, 1),  # hello
            (2, 2),  # ,
            (3, 3),  # I
            (4, 4),  # am
            (5, 6),  # Go ##sse
        ]

        self.assertEqual(expected_wp_tokens, wp_tokens)
        self.assertEqual(expected_offsets, offsets)

    def test_tag_tokenization(self):
        tokenizer = init_bert_tokenizer()
        word_tokens = "hello , I am Gosse".split()
        tags = [
            ["tag1", "tag2"],  # hello
            [],  # ,
            ["tag3"],  # I
            [],  # am
            ["tag4", "tag5"]  # Gosse
        ]
        _, offsets = bert_tokenize(word_tokens, tokenizer)
        tokenized_tags = retokenize_tags(tags, offsets)
        expected_tags = [
            [],  # [CLS]
            ["tag1", "tag2"],  # hello
            [],  # ,
            ["tag3"],  # I
            [],  # am
            ["tag4", "tag5"],  # Go
            ["tag4", "tag5"],  # ##sse
            [],  # [SEP]
        ]

        self.assertEqual(expected_tags, tokenized_tags)

    def test_role_filtering(self):

        tags = [
            ["T:Eating"],
            ["B:Eating:Eater", "I:Running:Runner"],
            ["B:Eating:Food"]
        ]

        filtered_tags = FrameNetDatasetReader.remove_frame_names(tags)
        expected_tags = [
            [],
            ["Eater", "Runner"],
            ["Food"]
        ]
        self.assertEqual(expected_tags, filtered_tags)


class Roles2FramesTest(unittest.TestCase):

    def test_frame_mapping(self):

        frames_and_roles = [
            ("Emotion_directed", "Experiencer"),
            ("Emotion_directed", "Stimulus"),
            ("Artifact", "Artifact"),
            ("Project", "Name"),
            ("Project", "Project")
        ]

        # reconstruct the frames
        roles = [pair[1] for pair in frames_and_roles]
        frame_finder = FrameFinder(roles)
        reconstructed_frames = frame_finder.find_frames()
        print(reconstructed_frames)

        # check that input size == output size
        self.assertEqual(len(frames_and_roles), len(reconstructed_frames))

        # check that we get a list of valid frames as output
        possible_frames = set(f.name for f in fn.frames())
        for f in reconstructed_frames:
            self.assertIn(f, possible_frames)

    def test_likely_frames(self):
        roles_to_frames = {
            "role1": ["frame1", "frame2", "frame3"],
            "role2": ["frame2", "frame4"],
            "role3": ["frame5"]
        }

        # the most likely frame is the one that is a possible frame for the largest number of roles
        most_likely_frames = {
            "role1": "frame2",
            "role2": "frame2",
            "role3": "frame5"
        }

        frame_finder = FrameFinder(list(roles_to_frames.keys()))
        frame_finder._roles_to_possible_frames = roles_to_frames
        frame_finder._frames2freq = frame_finder._get_frame_distribution()
        for role in most_likely_frames:
            predicted_frame = frame_finder._find_most_likely_frame(role)
            expected_frame = most_likely_frames[role]
            self.assertEqual(expected_frame, predicted_frame)

    def test_load_role_data(self):

        possible_frames = set(f.name for f in fn.frames())
        possible_roles = set(fe["name"] for fe in fn.fes())

        role_data = load_role_data("data/fn1.7/fn1.7.fulltext.train.syntaxnet.json")
        for roles, frames in role_data:
            self.assertTrue(len(roles) == len(frames))
            for r, f in zip(roles, frames):
                self.assertIn(r, possible_roles)
                self.assertIn(f, possible_frames)
                self.assertTrue(r in fn.frame(f).FE.keys())


class DimensionalityReductionTest(unittest.TestCase):
    pass


class PostProcessingTest(unittest.TestCase):

    def test_frame_collection(self):

        sentence_df = pd.DataFrame([
            {"token": "[CLS]", "prediction": "_"},
            {"token": "Nuclear", "prediction": "Weapon:0.786|T:Weapon:0.903"},
            {"token": "[SEP]", "prediction": "_"}
        ])
        pp = Postprocessor(None)
        frames = pp.collect_frames(sentence_df)
        self.assertEqual(frames, {"Weapon"})

    def test_remove_dangling_roles(self):

        sentence_df = pd.DataFrame([
            {"token": "[CLS]", "prediction": "_"},
            {"token": "Nuclear", "prediction": "Weapon:0.786|T:Weapon:0.903"},
            {"token": "weapons", "prediction": "Agent:0.333|Stimulus:0.222"},
            {"token": "[SEP]", "prediction": "_"}
        ])

        without_dangling_roles_df = pd.DataFrame([
            {"token": "[CLS]", "prediction": "_"},
            {"token": "Nuclear", "prediction": "Weapon:0.786|T:Weapon:0.903"},
            {"token": "[SEP]", "prediction": "_"}
        ])

        self.assertEqual(clean_dangling_roles(sentence_df), without_dangling_roles_df)



if __name__ == '__main__':
    unittest.main()
