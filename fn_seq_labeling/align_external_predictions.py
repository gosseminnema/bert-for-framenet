import json
import os
import sys
from typing import List, Dict


def main(sesame_file, source):
    with open("data/multilabel/fn1.7.dev.syntaxnet.json", encoding="utf-8") as f:
        dev_set_gold = json.load(f)

    with open(sesame_file, encoding="utf-8") as f:
        dev_set_sesame = json.load(f)

    mapping = map_tokens(dev_set_gold, dev_set_sesame, sort=source.startswith("simpleframeid"))
    with open(f"data/multilabel/fn1.7.gold2{source}_mapping.json", "w", encoding="utf-8") as f:
        json.dump(mapping, f)

    print(len(mapping))
    gold_aligned, sesame_aligned = align_annotations(dev_set_gold, dev_set_sesame, mapping)

    with open(f"data/multilabel/fn1.7.dev.syntaxnet.aligned_{source}.json", "w", encoding="utf-8") as f:
        json.dump(gold_aligned, f)

    with open(f"data/multilabel/fn1.7.dev.syntaxnet.aligned_{source}.txt", "w", encoding="utf-8") as f:
        write_txt(gold_aligned, f)

    with open(sesame_file.replace(".json", ".aligned.json"), "w", encoding="utf-8") as f:
        json.dump(sesame_aligned, f)

    with open(sesame_file.replace(".json", ".aligned.txt"), "w", encoding="utf-8") as f:
        write_txt(sesame_aligned, f)


def align_annotations(dev_set_gold, dev_set_sesame, mapping):
    gold_aligned = []
    sesame_aligned = []
    for i, gold_anno in enumerate(dev_set_gold):
        if i in mapping:
            gold_aligned.append(gold_anno)
            sesame_anno = dev_set_sesame[mapping[i]]
            sesame_anno["tokens"] = gold_anno["tokens"]
            sesame_aligned.append(sesame_anno)
    return gold_aligned, sesame_aligned


def map_tokens(dev_set_gold: List[Dict],
               dev_set_sesame: [List[Dict]],
               sort: bool = False
               ) -> Dict[int, int]:
    if sort:
        print("Sorting datasets alphabetically ... ")
        for dataset in [dev_set_gold, dev_set_sesame]:
            dataset.sort(key=lambda anno: "".join(anno["tokens"]))

    gold_to_sesame = {}
    last_sesame_idx = 0
    for i, gold_anno in enumerate(dev_set_gold):
        gold_tokens = [t.lower() for t in gold_anno["tokens"]]
        for j, sesame_anno in enumerate(dev_set_sesame):
            if j < last_sesame_idx:
                continue
            sesame_tokens = [t.lower() for t in sesame_anno["tokens"]]
            if are_aligned(gold_tokens, sesame_tokens):
                if gold_tokens != sesame_tokens:
                    pass
                    # print(gold_tokens, sesame_tokens, "\n")
                gold_to_sesame[i] = j
                last_sesame_idx = j
                break
        else:
            print(f"Could not align sentence {i:03} with tokens:\n{gold_tokens}\n")

    return gold_to_sesame


def are_aligned(gold_tokens, sesame_tokens):
    if len(gold_tokens) == len(sesame_tokens):
        num_matching_tokens = 0
        for gold_tok, sesame_tok in zip(gold_tokens, sesame_tokens):
            if gold_tok == sesame_tok:
                num_matching_tokens += 1
        return (num_matching_tokens / len(gold_tokens)) > 0.8

    return False


def write_txt(annotations, f):
    for annotation in annotations:
        for i, tok in enumerate(annotation["tokens"]):
            f.write(f"{tok} {annotation['pos'][i]} {'|'.join(annotation['frame_list'][i]) or '_'} "
                    f"{annotation['lu_list'][i] or '_'}" + os.linesep)
        f.write(os.linesep)


if __name__ == '__main__':
    prediction_source = sys.argv[2]
    print(prediction_source)
    assert prediction_source.startswith("sesame") or prediction_source.startswith("simpleframeid")
    main(sys.argv[1], prediction_source)
