import glob
import json

for file in glob.glob("configs/frameid_checks/*.json"):
    with open(file, encoding="utf-8") as f:
        conf = json.load(f)
        conf["shuffle_before_portioning"] = True
        with open(file.replace("p.json", "p_rand.json"), "w", encoding="utf-8") as fo:
            json.dump(conf, fo, indent=4)
        for i in range(3):
            conf["random_seed"] = (i+1) * 100
            with open(file.replace("p.json", f"p_rand.rep{i+1}.json"), "w", encoding="utf-8") as fo:
                json.dump(conf, fo, indent=4)