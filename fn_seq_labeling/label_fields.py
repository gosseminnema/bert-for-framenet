from typing import List, Dict, Optional, Set

import numpy as np
import torch
from allennlp.common.util import pad_sequence_to_length
from allennlp.data import Field, Vocabulary, DataArray
from allennlp.data.fields import SequenceField, SequenceLabelField
from sklearn.decomposition import PCA
from sklearn.preprocessing import MultiLabelBinarizer
from nltk.corpus import framenet as fn


class OOVSequenceLabelField(SequenceLabelField):
    """
    Slightly modified version of `SequenceLabelField` that replaces any unknown labels with "X"
    """

    def index(self, vocab: Vocabulary):
        if not self._skip_indexing:
            self._indexed_labels = [self._get_token_index(label, vocab)  # type: ignore
                                    for label in self.labels]

    def _get_token_index(self, label: str, vocab: Vocabulary):
        try:
            return vocab.get_token_index(label, self._label_namespace)
        except KeyError:
            return vocab.get_token_index("X")


class SequenceMultiLabelField(Field):

    def __init__(self,
                 labels: List[List[str]],
                 sequence_field: SequenceField,
                 binarizer: MultiLabelBinarizer,
                 label_namespace: str
                 ):
        self.labels = labels
        self._indexed_labels = None
        self._label_namespace = label_namespace
        self.sequence_field = sequence_field
        self.binarizer = binarizer

    def count_vocab_items(self, counter: Dict[str, Dict[str, int]]):
        for label_list in self.labels:
            for label in label_list:
                counter[self._label_namespace][label] += 1

    def get_padding_lengths(self) -> Dict[str, int]:
        return {"num_tokens": self.sequence_field.sequence_length()}

    def index(self, vocab: Vocabulary):

        indexed_labels: List[List[int]] = []
        for sentence_labels in self.labels:
            sentence_indexed_labels = []
            for label in sentence_labels:
                try:
                    sentence_indexed_labels.append(vocab.get_token_index(label, self._label_namespace))
                except KeyError:
                    print(f"[WARNING] Ignore unknown label {label}")
            indexed_labels.append(sentence_indexed_labels)
        self._indexed_labels = indexed_labels

    def as_tensor(self, padding_lengths: Dict[str, int]) -> torch.Tensor:

        # binarize
        binarized_seq = self.binarizer.transform(self._indexed_labels).tolist()

        # padding
        desired_num_tokens = padding_lengths["num_tokens"]
        padded_tags = pad_sequence_to_length(binarized_seq, desired_num_tokens,
                                             default_value=lambda: list(self.binarizer.transform([[]])[0]))

        tensor = torch.tensor(padded_tags, dtype=torch.float)
        return tensor

    def empty_field(self) -> 'Field':

        field = SequenceMultiLabelField([], self.sequence_field.empty_field(), self.binarizer, self._label_namespace)
        field._indexed_labels = []
        return field


class ReducedSequenceMultiLabelField(SequenceMultiLabelField):

    def __init__(self,
                 labels: List[List[str]],
                 sequence_field: SequenceField,
                 binarizer: MultiLabelBinarizer,
                 pca_model: PCA
                 ):
        super().__init__(labels, sequence_field, binarizer, "labels")
        self.pca_model = pca_model

    def as_tensor(self, padding_lengths: Dict[str, int]) -> torch.Tensor:

        # if PCA model is not fitted yet: return unreduced tensor
        # TODO: find a more principled solution
        if not hasattr(self.pca_model, "components_"):
            return self.as_unreduced_tensor(padding_lengths)

        tensor = super().as_tensor(padding_lengths)
        return torch.tensor(self.pca_model.transform(tensor))

    def as_unreduced_tensor(self, padding_lengths: Dict[str, int]) -> torch.Tensor:
        return super().as_tensor(padding_lengths)

    def empty_field(self) -> 'Field':
        field = ReducedSequenceMultiLabelField([], self.sequence_field.empty_field(), self.binarizer,
                                               self.pca_model)
        field._indexed_labels = []
        return field


class EmbeddedSequenceLabelField(Field):

    def __init__(self,
                 labels: List[str],
                 sequence_field: SequenceField,
                 embeddings: Dict[str, np.ndarray],
                 use_binary_marker: bool = True
                 ):
        self.labels = labels
        self.sequence_field = sequence_field
        self.embeddings = embeddings
        self.embedding_size = next(iter(embeddings.values())).shape[0]
        self.use_binary_marker = use_binary_marker

    def get_padding_lengths(self) -> Dict[str, int]:
        return {"num_tokens": self.sequence_field.sequence_length()}

    def as_tensor(self, padding_lengths: Dict[str, int]) -> torch.Tensor:
        tensor_list = self._get_label_tensors()
        padded_size = self.embedding_size + 1 if self.use_binary_marker else self.embedding_size
        padded_sequence = pad_sequence_to_length(tensor_list, padding_lengths["num_tokens"],
                                                 lambda: torch.zeros(padded_size))
        return torch.cat([t.reshape(1, -1) for t in padded_sequence])

    def _get_label_tensors(self):
        tensors = []
        for label in self.labels:
            if label.startswith("T:"):
                label = label.split(":")[1]
            if label in self.embeddings:
                embedding = self.embeddings[label]
                binary_val = 1.0
            else:
                embedding = np.zeros(self.embedding_size)
                binary_val = 0.0
            binary_plus_embedding = np.hstack([binary_val, embedding])

            if self.use_binary_marker:
                tensor = torch.tensor(binary_plus_embedding, dtype=torch.float)
            else:
                tensor = torch.tensor(embedding, dtype=torch.float)
            tensors.append(tensor)
        return tensors

    def empty_field(self) -> Field:
        field = EmbeddedSequenceLabelField([], self.sequence_field.empty_field(), self.embeddings)
        return field


class FrameContextField(Field):

    FRAMES_TO_ID = {
        frame.name: i
        for i, frame in enumerate(fn.frames())
    }

    ID_TO_FRAME = {
        i: frame for frame, i in FRAMES_TO_ID.items()
    }

    def __init__(self,
                 frame_labels: Set[str],
                 embeddings: Optional[Dict[str, np.ndarray]],
                 sequence_field: SequenceField,
                 embedding_size: int = 300
                 ):
        self.frame_labels = frame_labels
        self.embeddings = embeddings
        self.sequence_field = sequence_field

        if self.embeddings is not None:
            self.embedding_size = embedding_size
        else:
            self.embedding_size = len(self.FRAMES_TO_ID)

    def get_padding_lengths(self) -> Dict[str, int]:
        return {"num_tokens": self.sequence_field.sequence_length()}

    def as_tensor(self, padding_lengths: Dict[str, int]) -> DataArray:

        context_vector = self._make_context_vector()
        context_tensor = torch.tensor(context_vector, dtype=torch.float).reshape(1, -1)
        repeated_tensor = context_tensor.repeat(padding_lengths["num_tokens"], 1)
        return repeated_tensor

    def _make_context_vector(self):

        frames = {
            frame_label.split(":")[1]
            for frame_label in self.frame_labels
        }

        # if embeddings are given: retrieve embeddings for each frame and take the mean
        if self.embeddings is not None:

            frames = {f for f in frames if f in self.embeddings}

            if frames:
                context_vector = np.mean([self.embeddings[f] for f in frames], axis=0)
            else:
                context_vector = np.zeros(self.embedding_size)

        # otherwise, make binary vectors
        else:
            context_vector = np.zeros(len(self.FRAMES_TO_ID))
            for frame in frames:
                f_id = self.FRAMES_TO_ID[frame]
                context_vector[f_id] = 1

        return context_vector

    def empty_field(self) -> 'Field':
        return FrameContextField(set(), self.embeddings, self.sequence_field, self.embedding_size)
