"""
Semi-automatic annotation for Inferred Frames
(cf. Remijnse & Minnema, IFNW 2020, https://www.aclweb.org/anthology/2020.framenet-1.3
"""
from typing import List, Optional, Dict
import abc

import gensim
from nltk.corpus import framenet as fn
import numpy as np

from fn_seq_labeling.embed_frames import FrameEmbedder


class FrameInferrer(abc.ABC):

    @abc.abstractmethod
    def infer(self,
              tokens: List[str],
              labels: List[str],
              n_best_labels: Optional[List[str]],
              typical_frames: List[str]
              ) -> List[str]:
        pass


class CombinedFrameInferrer(FrameInferrer):

    def __init__(self, *inferrers: FrameInferrer):
        self.inferrers = inferrers

    def infer(self,
              tokens: List[str],
              labels: List[str],
              n_best_labels: Optional[List[str]],
              typical_frames: List[str]
              ) -> List[str]:

        if not self.inferrers:
            return ["_" for _ in labels]

        inferred_frames_lists = []
        for inf in self.inferrers:
            inferred_frames_lists.append(inf.infer(tokens, labels, n_best_labels, typical_frames))

        inferred_frames = []
        for token_inferred_frames in zip(*inferred_frames_lists):
            combined_token_frames = set()
            for label in token_inferred_frames:
                if label != "_":
                    combined_token_frames.update(label.split("|"))
            inferred_frames.append("|".join(combined_token_frames) or "_")
        return inferred_frames


class Frame2FrameInferrer(FrameInferrer):

    def infer(self,
              tokens: List[str],
              labels: List[str],
              n_best_labels: Optional[List[str]],
              typical_frames: List[str]
              ) -> List[str]:

        inferred_frames = []

        for best_frame in labels:
            best_frame = best_frame.split(":")[1] if best_frame != "_" else None
            if not best_frame:
                inferred_frames.append("_")
                continue

            else:
                related_frames = Frame2FrameInferrer.find_related_frames(best_frame)
                token_inferred_frames = []
                for tf in typical_frames:
                    if tf == best_frame or tf in related_frames:
                        token_inferred_frames.append(tf)
                inferred_frames.append("|".join(token_inferred_frames) or "_")

        return inferred_frames

    @staticmethod
    def find_related_frames(frame_x, num_steps=1, frames_to_ignore=None):

        if not frames_to_ignore:
            frames_to_ignore = {frame_x}

        # find frames directly related to the current one
        related_frames = set()
        for fr in fn.frame(frame_x).frameRelations:
            for frame in (fr.superFrameName, fr.subFrameName):
                if frame != frame_x:
                    related_frames.add(frame)

        if num_steps > 1:
            # recursive step
            for frame in related_frames:
                related_frames = related_frames.union(
                    Frame2FrameInferrer.find_related_frames(frame,
                                                            num_steps=num_steps - 1,
                                                            frames_to_ignore=frames_to_ignore
                                                            ))
        return related_frames


class FrameEmbeddingInferrer(FrameInferrer):

    def __init__(self,
                 embedder: FrameEmbedder,
                 n_neighbours: int = 10
                 ):

        vectors, vocab, word_vocab = embedder.embed()
        model = gensim.models.KeyedVectors(300)
        model.add(vocab, vectors)
        self.model = model
        self.n_neighbours = n_neighbours

    def infer(self,
              tokens: List[str],
              labels: List[str],
              n_best_labels: Optional[List[str]],
              typical_frames: List[str]
              ) -> List[str]:

        inferred_frames = []

        for best_frame in labels:
            best_frame = best_frame.split(":")[1] if best_frame != "_" else None
            if not best_frame:
                inferred_frames.append("_")
                continue

            else:
                token_inferred_frames = []
                neighbours = [frame for frame, sim in self.model.most_similar(best_frame, topn=self.n_neighbours)]
                for frame in typical_frames:
                    if frame == best_frame or frame in neighbours:
                        token_inferred_frames.append(frame)
                inferred_frames.append("|".join(token_inferred_frames))

        return inferred_frames


class LexItemToFrameInferrer(FrameInferrer):

    def __init__(self,
                 embedder: FrameEmbedder,
                 glove_model: gensim.models.KeyedVectors,
                 n_neighbours: int = 10
                 ):

        vectors, vocab, word_vocab = embedder.embed()
        frame_model = gensim.models.KeyedVectors(300)
        frame_model.add(vocab, vectors)
        self.frame_model = frame_model
        self.glove_model = glove_model
        self.n_neighbours = n_neighbours

    def infer(self,
              tokens: List[str],
              labels: List[str],
              n_best_labels: Optional[List[str]],
              typical_frames: List[str]
              ) -> List[str]:

        inferred_frames = []

        for token, best_frame in zip(tokens, labels):
            best_frame = best_frame.split(":")[1] if best_frame != "_" else None
            if not best_frame:
                inferred_frames.append("_")
                continue

            token_key = f"lex:{token}"
            if token_key not in self.frame_model:
                if token in self.glove_model:
                    self.frame_model.add([token_key], [self.glove_model[token]])
                    del self.frame_model.vectors_norm

            most_similar_frames = []
            if token_key in self.frame_model:
                most_similar_frames = [
                    neighbour for neighbour, sim in self.frame_model.most_similar(token_key, topn=self.n_neighbours*10)
                    if not neighbour.startswith("lex:")
                ][:10]

            token_inferred_frames = []
            for frame in typical_frames:
                if frame == best_frame or frame in most_similar_frames:
                    token_inferred_frames.append(frame)
            inferred_frames.append("|".join(token_inferred_frames))

        return inferred_frames


class LikeliestFramesInferrer(FrameInferrer):

    def infer(self,
              tokens: List[str],
              labels: List[str],
              n_best_labels: Optional[List[str]],
              typical_frames: List[str]
              ) -> List[str]:

        if n_best_labels is None:
            n_best_labels = ["" for _ in labels]

        inferred_frames = []
        for best, n_best in zip(labels, n_best_labels):

            best = best.split(":")[1] if best != "_" else None
            n_best = [item.split(":")[0] for item in n_best.split("|")]
            token_inferred_frames = []
            for tf in typical_frames:
                if tf == best or tf in n_best:
                    token_inferred_frames.append(tf)
            inferred_frames.append("|".join(token_inferred_frames) or "_")

        return inferred_frames


def test():
    # frame_name = input("Select a frame: >>> ")
    #
    # for num_steps in range(1, 5):
    #     rel_frames = Frame2FrameInferrer.find_related_frames(frame_name, num_steps=num_steps)
    #     print(f"n_steps={num_steps}")
    #     print(len(rel_frames))
    #     print(list(rel_frames)[:10])
    #     print()

    inf = CombinedFrameInferrer(LikeliestFramesInferrer(), Frame2FrameInferrer())
    inferred = inf.infer(["kill", "him", "slowly", "car"],
                         ["T:Killing", "_", "_", "T:Operate_vehicle"],
                         None,
                         ["Killing", "Execution", "Dying", "Vehicle"])
    print(inferred)


if __name__ == '__main__':
    test()
