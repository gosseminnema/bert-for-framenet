import re
from typing import Dict, Optional, Iterable, List, Tuple, Set

import numpy as np

from allennlp.data import DatasetReader, TokenIndexer, Instance, Token
from allennlp.data.fields import TextField, SequenceLabelField
from allennlp.data.token_indexers import SingleIdTokenIndexer
from pytorch_transformers import AutoTokenizer, BertTokenizer
from sklearn.decomposition import PCA
from sklearn.preprocessing import MultiLabelBinarizer
import pandas as pd
from nltk.corpus import framenet as fn
from tqdm import tqdm

from fn_seq_labeling import constants as cns
from fn_seq_labeling.embed_frames import BagOfLexUnitsEmbedder
from fn_seq_labeling.evaluation.eval_utils import split_sentences
from fn_seq_labeling.infer_frames import FrameEmbeddingInferrer
from fn_seq_labeling.multilabel import sesame2multilabel
from fn_seq_labeling.label_fields import SequenceMultiLabelField, ReducedSequenceMultiLabelField, \
    EmbeddedSequenceLabelField, OOVSequenceLabelField, FrameContextField


class FrameNetDatasetReader(DatasetReader):
    # partially based on the POS dataset in the AllenNLP tutorial

    def __init__(self, multilabel_config: cns.MultilabelSetting, include_frame_names: cns.TargetLabelSetting,
                 embed_frame_labels: cns.FrameLabelEmbeddingSetting, tokenizer_model: str,
                 token_indexers: Dict[str, TokenIndexer] = None, st_binarizer: Optional[MultiLabelBinarizer] = None,
                 mt_frame_binarizer: Optional[MultiLabelBinarizer] = None,
                 mt_role_binarizer: Optional[MultiLabelBinarizer] = None, pca_model: Optional[PCA] = None,
                 label_embeddings: Optional[Dict[str, np.ndarray]] = None, binary_marker_in_embedding: bool = True,
                 multitask: bool = False, add_frame_context_vecs: bool = False, add_roles_context_vecs=False,
                 combine_roles_context: bool = False,
                 keep_tag_idxs: bool = False, split_by_frames: bool = False,
                 input_frame_predictions: Optional[str] = None, infer_extra_frame_predictions: bool = False,
                 use_super_fe: bool = False, input_roles_predictions=False):

        super(FrameNetDatasetReader, self).__init__(lazy=False)
        self.token_indexers = token_indexers or {"tokens": SingleIdTokenIndexer()}
        self.tokenizer = init_bert_tokenizer(model=tokenizer_model)
        self.include_frame_names = include_frame_names

        self.embed_frame_labels = embed_frame_labels
        self.label_embeddings = label_embeddings
        self.binary_marker_in_embedding = binary_marker_in_embedding
        self.multilabel_setting = multilabel_config
        # for now, embedding labels only works with multi_label_setting = "only_first"
        if self.embed_frame_labels != cns.FrameLabelEmbeddingSetting.NONE:
            assert self.multilabel_setting == cns.MultilabelSetting.ONLY_FIRST or add_frame_context_vecs

        # multitask learning
        self.multitask = multitask

        # binarization and dimensionality reduction
        print(f"multitask={multitask}")
        if self.multitask:
            self.mt_frame_binarizer = mt_frame_binarizer
            self.mt_role_binarizer = mt_role_binarizer
            self.st_binarizer = None
        else:
            self.st_binarizer = st_binarizer
            self.mt_frame_binarizer = None
            if add_roles_context_vecs:
                self.mt_role_binarizer = mt_role_binarizer
            else:
                self.mt_role_binarizer = None

        self._binarizer_is_frozen = False
        self.pca_model = pca_model
        if self.multilabel_setting in [cns.MultilabelSetting.BINARIZED, cns.MultilabelSetting.BINARIZED_PCA]:
            if self.multitask:
                assert not (mt_role_binarizer is None or mt_frame_binarizer is None), \
                    "No binarizers were provided for multi-task setup"
            else:
                assert st_binarizer is not None, \
                    "No binarizer was provided for single-task setup"

        # add frame/roles context representation to instances?
        self.add_frame_context_vecs = add_frame_context_vecs
        self.add_roles_context_vecs = add_roles_context_vecs

        # add separate instances for every frame type?
        self.split_by_frames = split_by_frames

        # use predicted frames for context representations?
        self.input_frame_predictions = input_frame_predictions
        self.input_frame_df: Optional[List[pd.DataFrame]]
        if self.input_frame_predictions is not None:
            self.input_frame_df = split_sentences(pd.read_csv(self.input_frame_predictions))
        else:
            self.input_frame_df = None
        self.infer_extra_frame_predictions = infer_extra_frame_predictions

        # use predicted roles?
        self.input_roles_predictions = input_roles_predictions
        self.input_roles_df = Optional[List[pd.DataFrame]]
        if self.input_roles_predictions is not None:
            self.input_roles_df = split_sentences(pd.read_csv(self.input_roles_predictions))
        else:
            self.input_roles_df = None
        self.combine_roles_context = combine_roles_context

        if multitask:
            self.unique_tags = None
            self.unique_frame_tags = set()
            self.unique_role_tags = set()
        else:
            self.unique_tags = set()
            self.unique_frame_tags = None
            if self.add_roles_context_vecs:
                self.unique_role_tags = set()
            else:
                self.unique_role_tags = None

        # for Open-SESAME evaluation
        self.keep_tag_idxs = keep_tag_idxs

        # for FE-relation experiment
        self.use_super_fe = use_super_fe

        # only for printing debug message #TODO remove after debug
        self.first_time_run = True

    def freeze_binarizer(self):
        self._binarizer_is_frozen = True

    def _read(self, file_path: str) -> Iterable[Instance]:
        """
        Read
        :param file_path:
        :return:
        """

        if self.infer_extra_frame_predictions:
            frame_inferrer = FrameEmbeddingInferrer(BagOfLexUnitsEmbedder(), n_neighbours=2)
            frames_to_expand = [fr.name for fr in fn.frames()]
        else:
            frame_inferrer = None
            frames_to_expand = []

        with open(file_path, encoding="utf-8") as f:
            for sent_idx, sentence in enumerate(sesame2multilabel.read_txt_corpus(f)):

                tokens, offsets = bert_tokenize(sentence.tokens, self.tokenizer)
                tags = retokenize_tags(sentence.frame_list, offsets)

                if sent_idx < 10:
                    print("tokens:", tokens)
                    # print("offsets:", offsets)
                    print("tags:", tags)

                if self.use_super_fe:
                    tags = get_super_fe_tags(tags)

                if self.split_by_frames:

                    # load predicted frames (for dev/test)
                    if self.input_frame_predictions is not None:
                        sentence_frames = self._get_predicted_sentence_frames(tokens)

                        # for now - since we use FrameEmbeddingInferrer - we don't care which tokens the sentence
                        # frames are associated to, we can just put them on the first _n_ tokens
                        assert len(tokens) >= len(sentence_frames)
                        best_frame_labels = ["_" for _ in tokens]
                        for sf_idx, sf in enumerate(sentence_frames):
                            best_frame_labels[sf_idx] = f"T:{sf}"

                        if self.infer_extra_frame_predictions:
                            inferred_frames = frame_inferrer.infer(
                                tokens=[t.text for t in tokens],
                                labels=best_frame_labels,
                                typical_frames=frames_to_expand,
                                n_best_labels=None
                            )
                            inferred_frame_set = {inf for tok_inf in inferred_frames
                                                  for inf in tok_inf.split("|")
                                                  if tok_inf != "_"}
                            print(f"Infer: {sentence_frames} => {inferred_frame_set}")
                            sentence_frames.update(inferred_frame_set)

                    # load gold frames (for train or for gold frame eval)
                    else:
                        sentence_frames = self._get_sentence_frame_names(tags)

                    for frame in sentence_frames:
                        yield self.text_to_instance(tokens, tags, frame_filter=frame)

                elif self.input_roles_predictions is not None:
                    roles_predictions = self._get_predicted_roles(tokens)
                    yield self.text_to_instance(tokens, tags, pred_role_tags=roles_predictions)

                else:
                    yield self.text_to_instance(tokens, tags, verbose=sent_idx<10)
        if self._binarizer_is_frozen:
            print(f"Not refitting binarizer for dataset {file_path}")
        else:
            for binarizer, unique_tags in [
                (self.st_binarizer, self.unique_tags),
                (self.mt_frame_binarizer, self.unique_frame_tags),
                (self.mt_role_binarizer, self.unique_role_tags)
            ]:
                if binarizer is not None:
                    print("Fitting binarizer ...")
                    binarizer.fit([
                        list(range(len(unique_tags)))
                    ])

    @staticmethod
    def _get_sentence_frame_names(tags: List[List[str]]) -> Set[str]:
        sentence_frames = set()
        for token_tags in tags:
            for tag in token_tags:
                m = re.match(r"^T:(.*?)(@\d\d)?$", tag)
                if m:
                    frame_name = m.group(1)
                    sentence_frames.add(frame_name)
        return sentence_frames

    def _get_predicted_sentence_frames(self, tokens: List[Token]) -> Set[str]:

        input_frame_df = self.input_frame_df
        try:
            tags = self._get_prediction_tags_for_sentence(input_frame_df, tokens)
        except AssertionError:
            print(f"[WARNING] Could not find frame predictions for sentence {tokens}")
            tags = set()
        return self._get_sentence_frame_names(tags)

    @staticmethod
    def _get_prediction_tags_for_sentence(input_frame_df, tokens):
        token_strings = [t.text for t in tokens]
        matching_prediction_sent = [
            sent_df for sent_df in input_frame_df
            if sent_df["token"].tolist() == token_strings
        ]
        # if len(matching_prediction_sent) > 1:
        #     print("HELP! More than one match found")

        assert len(matching_prediction_sent) >= 1, f"Could not find unique match for tokens {token_strings}"
        sent_df = matching_prediction_sent[0]
        tags = [
            tag_str.split("|") if tag_str not in ("X", "_") else []
            for tag_str in sent_df["prediction"].tolist()
        ]
        return tags

    def text_to_instance(self,
                         tokens: List[Token],
                         tags: List[List[str]] = None,
                         frame_filter: Optional[str] = None,
                         pred_role_tags = None,
                         verbose: bool = False
                         ) -> Instance:

        # field representing the text
        sentence_field = TextField(tokens, self.token_indexers)
        fields = {"sentence": sentence_field}
        if verbose:
            print("sentence_field:", sentence_field)

        if tags:
            if frame_filter:
                filtered_tags = self._filter_tags_by_frame(frame_filter, tags)
                tags = filtered_tags

            if self.multitask:
                frame_label_field, frame_original_field = self._define_label_field(sentence_field, tags, "frames")
                roles_label_field, roles_original_field = self._define_label_field(sentence_field, tags, "roles")
                fields["frame_labels"] = frame_label_field
                fields["original_frame_labels"] = frame_original_field
                fields["role_labels"] = roles_label_field
                fields["original_role_labels"] = roles_original_field

            else:
                label_field, original_label_field = self._define_label_field(sentence_field, tags)
                fields["labels"] = label_field
                fields["original_labels"] = original_label_field

            if self.add_frame_context_vecs:

                if frame_filter is not None:
                    frame_tags = {f"T:{frame_filter}"}
                else:
                    frame_tags = [[t for t in tag_list if t.startswith("T:")]
                                  if tag_list else [] for tag_list in tags]
                    frame_tags = {tag_list[0].split("@")[0] for tag_list in frame_tags if tag_list}

                if self.embed_frame_labels != cns.FrameLabelEmbeddingSetting.NONE:
                    frame_context_field = FrameContextField(frame_tags,
                                                            self.label_embeddings,
                                                            sentence_field
                                                            )

                else:
                    frame_context_field = FrameContextField(frame_tags,
                                                            None,
                                                            sentence_field)

            else:
                frame_context_field = None

            if frame_context_field is not None:
                fields["frame_context_vector"] = frame_context_field

            if self.add_roles_context_vecs:

                if pred_role_tags is not None:
                    role_tags = pred_role_tags
                else:
                    role_tags = tags

                if self.combine_roles_context:
                    role_tags = self._merge_tag_lists(role_tags)

                roles_context_field, _ = self._define_label_field(sentence_field, role_tags, "roles")
                fields["roles_context_vector"] = roles_context_field

                # TODO remove after debug
                if self.first_time_run:
                    tqdm.write(f"\n\n")
                    tqdm.write(f"[DEBUG_ROLE_CXT]: self.add_roles_context_vecs={self.add_roles_context_vecs}")
                    tqdm.write(f"[DEBUG_ROLE_CXT]: self.input_roles_predictions={self.input_roles_predictions}")
                    tqdm.write(f"[DEBUG_ROLE_CXT]: tokens={tokens}")
                    tqdm.write(f"[DEBUG_ROLE_CXT]: role_tags={role_tags}")
                    tqdm.write(f"[DEBUG_ROLE_CXT]: fields['labels']={fields['labels']}")
                    tqdm.write(f"\n\n")
                    self.first_time_run = False

        return Instance(fields)

    @staticmethod
    def _merge_tag_lists(tags):
        merged_roles_context = set()
        for tag_list in tags:
            for tag in tag_list:
                merged_roles_context.add(tag)
        merged_roles_context = [list(merged_roles_context)] * len(tags)
        return merged_roles_context

    @staticmethod
    def _filter_tags_by_frame(frame, tags):
        filtered_tags = []
        for tag_list in tags:
            filtered_tags.append([
                tag for tag in tag_list
                if re.match(r"^\w:" + frame + r"(:.*)?@\d\d$", tag)
            ])
        return filtered_tags

    def _define_label_field(self, sentence_field, tags, multitask=None):
        """
        :param sentence_field:
        :param tags:
        :param multitask: in {None, "frames", "roles")
        :return:
        """

        # multitask: keeps frames and roles separate
        if multitask == "frames":
            tags = [[t for t in tag_list if t.startswith("T:")] if tag_list else [] for tag_list in tags]
        elif multitask == "roles":
            tags = self.remove_frame_names(tags)

        # single task: look at the TargetLabelSetting
        else:
            if self.include_frame_names == cns.TargetLabelSetting.TARGETS_AND_ROLES_WITH_FRAMES:
                # remove BIO tags
                tags = [[re.sub(r"[BI]:", "", t) for t in tag_list] if tag_list else [] for tag_list in tags]
            elif self.include_frame_names == cns.TargetLabelSetting.TARGETS_AND_ROLES_WITHOUT_FRAMES:
                tags = self.remove_frame_names(tags, keep_targets=True)
            elif self.include_frame_names == cns.TargetLabelSetting.TARGETS_WITHOUT_ROLES:
                tags = [[t for t in tag_list if t.startswith("T:")] if tag_list else [] for tag_list in tags]
            elif self.include_frame_names == cns.TargetLabelSetting.TARGETS_WITHOUT_ROLES_OR_NAMES:
                tags = [["TARGET" for t in tag_list if t.startswith("T:")] if tag_list else [] for tag_list in tags]
            else:
                tags = self.remove_frame_names(tags)

        # remove structure indices from labels
        if not self.keep_tag_idxs:
            tags = [[tag.split("@")[0] for tag in tag_list] for tag_list in tags]

        # keep track of unique tags
        for tag_list in tags:
            if multitask == "frames":
                self.unique_frame_tags.update(tag_list)

            elif multitask == "roles":
                self.unique_role_tags.update(tag_list)

            else:
                self.unique_tags.update(tag_list)

        # define binarizer & label namespace
        if multitask == "frames":
            binarizer = self.mt_frame_binarizer
            label_namespace = "frame_labels"
        elif multitask == "roles":
            binarizer = self.mt_role_binarizer
            label_namespace = "role_labels"
        else:
            binarizer = self.st_binarizer
            label_namespace = "labels"

        # for roles context vectors
        if self.add_roles_context_vecs and multitask == "roles":
            label_field = original_label_field = SequenceMultiLabelField(
                tags, sentence_field, binarizer, label_namespace
            )

        # only one label: define a SequenceLabelField
        elif multitask == "frames" or self.multilabel_setting == cns.MultilabelSetting.ONLY_FIRST:
            single_tag = [tag_list[0] if tag_list else "X" for tag_list in tags]
            if self.embed_frame_labels != cns.FrameLabelEmbeddingSetting.NONE:
                label_field = original_label_field = EmbeddedSequenceLabelField(single_tag,
                                                                                sentence_field,
                                                                                self.label_embeddings,
                                                                                self.binary_marker_in_embedding)
            else:
                label_field = original_label_field = OOVSequenceLabelField(single_tag,
                                                                           sentence_field,
                                                                           label_namespace=label_namespace
                                                                           )

        elif self.multilabel_setting == cns.MultilabelSetting.CONCATENATE:
            combined_tags = self.concatenate_tags(tags)
            label_field = original_label_field = SequenceLabelField(combined_tags, sentence_field)

        # multi-label, binarized: use SequenceMultiLabelField
        elif self.multilabel_setting == cns.MultilabelSetting.BINARIZED:
            label_field = original_label_field = \
                SequenceMultiLabelField(tags, sentence_field, binarizer, label_namespace)

        # multi-label, binarized, dim-reduced: use ReducedSequenceMultiLabelField
        elif self.multilabel_setting == cns.MultilabelSetting.BINARIZED_PCA:
            label_field = ReducedSequenceMultiLabelField(tags, sentence_field, binarizer, self.pca_model)
            original_label_field = SequenceMultiLabelField(tags, sentence_field, binarizer, label_namespace)

        else:
            raise ValueError("Unknown label type")
        return label_field, original_label_field

    @staticmethod
    def remove_frame_names(tags, keep_targets=False):
        # remove targets
        if not keep_targets:
            tags = [[t for t in tag_list if not t.startswith("T:")] if tag_list else [] for tag_list in tags]

        tags = [[re.sub(r"[BI]:[\w-]+:", "", t) for t in tag_list] if tag_list else [] for tag_list in tags]
        return tags

    @staticmethod
    def concatenate_tags(tags):
        combined_tags = ["|".join(tag_list) if tag_list else "X" for tag_list in tags]
        return combined_tags

    def _get_predicted_roles(self, tokens: List[Token]) -> List[List[str]]:

        try:
            tags = self._get_prediction_tags_for_sentence(self.input_roles_df, tokens)
            role_tags = [[":".join(t.split(":")[:-1]) for t in tag_list if not t.startswith("T:")] for tag_list in tags]
        except AssertionError as e:
            print(e)
            role_tags = []
        return role_tags


def init_bert_tokenizer(model="bert-base-cased"):
    print(f"Initializing BERT tokenizer: {model}")
    tokenizer = AutoTokenizer.from_pretrained(model, do_lower_case=False)
    return tokenizer


# based on newer version of AllenNLP PretrainedTransformerTokenizer, see https://bit.ly/38zE5ap
def bert_tokenize(tokens: List[str], bert_tokenizer: BertTokenizer) -> Tuple[List[Token], List[Tuple[int, int]]]:
    wordpieces = [bert_tokenizer.cls_token]
    offsets = []
    cumulative_offset = 1

    for token in tokens:
        subtoken_wordpieces = bert_tokenizer.convert_ids_to_tokens(
            bert_tokenizer.encode(token, add_special_tokens=False)
        )
        if len(subtoken_wordpieces) == 0:
            subtoken_wordpieces = [bert_tokenizer.unk_token]
        wordpieces.extend(subtoken_wordpieces)
        start_offset = cumulative_offset
        cumulative_offset += len(subtoken_wordpieces)
        end_offset = cumulative_offset - 1
        offsets.append((start_offset, end_offset))

    wordpieces.append(bert_tokenizer.sep_token)
    wordpieces = [Token(wp) for wp in wordpieces]

    return wordpieces, offsets


def retokenize_tags(tags: List[List[str]],
                    offsets: List[Tuple[int, int]],
                    wp_primary_token: str = "last",
                    wp_secondary_tokens: str = "empty",
                    empty_value=lambda: []
                    ) -> List[List[str]]:
    tags_per_wordpiece = [
        empty_value()  # [CLS]
    ]

    for i, (off_start, off_end) in enumerate(offsets):
        tag = tags[i]

        # put a tag on the first wordpiece corresponding to the word token
        # e.g. "hello" --> "he" + "##ll" + "##o" --> 2 extra tokens
        # TAGS: [..., TAG, None, None, ...]
        num_extra_tokens = off_end - off_start
        if wp_primary_token == "first":
            tags_per_wordpiece.append(tag)
        if wp_secondary_tokens == "repeat":
            tags_per_wordpiece.extend(num_extra_tokens * [tag])
        else:
            tags_per_wordpiece.extend(num_extra_tokens * [empty_value()])
        if wp_primary_token == "last":
            tags_per_wordpiece.append(tag)

    tags_per_wordpiece.append(empty_value())  # [SEP]

    return tags_per_wordpiece


def find_super_fe(frame_name: str,
                  fe_name: str
                  ) -> str:
    frame_obj = fn.frame(frame_name)
    fe_obj = fn.fes(fe_name, frame_name)[0]

    # find parent (inherited) frame
    for fr in frame_obj.frameRelations:
        if fr.type.name == "Inheritance" and fr.subFrame == frame_obj:
            super_frames = [fer.superFE for fer in fr.feRelations if fer.subFE == fe_obj]
            if not super_frames:
                return fe_name
            super_frame = super_frames[0]
            return super_frame.name
    # no parent frame? just use FE name as is
    else:
        return fe_name


def get_super_fe_tags(tags: List[List[str]]) -> List[List[str]]:
    super_fe_tags = []
    for tag_list in tags:
        token_tags = []
        for tag in tag_list:
            if tag.startswith("T:"):
                token_tags.append(tag)
            else:
                tag_, struct_code = tag.split("@")
                parsed_tag = tag_.split(":")
                iob = parsed_tag[0]
                frame_name = parsed_tag[1]
                fe_name = parsed_tag[2]
                super_fe_name = find_super_fe(frame_name, fe_name)
                tag_with_super_fe = f"{iob}:{frame_name}:{super_fe_name}@{struct_code}"
                token_tags.append(tag_with_super_fe)
        super_fe_tags.append(token_tags)
    return super_fe_tags
