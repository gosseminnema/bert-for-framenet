# `bert-for-framenet`

This is the official GitLab repository for the paper:

> Gosse Minnema & Malvina Nissim (2021). *Breeding Fillmore's Chickens and Hatching the Eggs: Recombinding Frames and Roles in Frame-Semantic Parsing*. _Proceedings of the 14th Conference on Computational Semantics (IWCS'21)_, Groningen, The Netherlands (online)

Our paper will be published in the ACL Anthology soon; for now, you can find our camera-ready version [here](https://drive.google.com/file/d/1h3Fwn-00dk41SQNcOJEci3zIvATtMHowI/view?usp=sharing).

If you would like to get in touch with us, you can find our contact details at [gossminn.eu](https://gossminn.eu/) and [malvinanissim.github.io](https://malvinanissim.github.io/).

## How to use this repo? 

### (1) Get data
This repo as-is contains only our code and configuration files. In order to have a look at the outputs of our system (incl. prediction and score files) or to reproduce our experiments, you need to download [this data folder (*.tar.gz)](https://drive.google.com/file/d/1UP3uSg0IXGv6Q4P2K0vS_AKxJw_KZO7W/view?usp=sharing) and extract it into the root of the repository. The resulting folder structure should look like this: 

```
├── configs
│   ├── ...
├── data
│   ├── embeddings
│   ├── fn1.7
│   │   ├── bios
│   │   ├── fn_raw_sentences
│   │   ├── semeval
│   │   └── sesame
│   ├── lome_output
│   ├── multilabel
│   ├── sesame_output
├── fn_seq_labeling
│   ├── ...
├── lib
│   └── semeval
├── output
│   ├── external_evaluation
│   ├── full_structure_predictions
│   ├── predictions
├── scripts
```

Some important notes on these files:

* `./data/fn1.7` contains the official FrameNet corpus (release v1.7), converted to different formats. Conversion to `sesame` was done using Swayamdipta et al.'s [Open-SESAME](https://github.com/swabhs/open-sesame) package, conversion to `bios` and `semeval` was done using Alexandre Kabbach's [PyFN](https://github.com/akb89/pyfn) package. The FrameNet corpus is licenced under Creative Commons (CC-BY); we strongly encourage you to register as an official user [on the official FrameNet website](https://framenet.icsi.berkeley.edu/fndrupal/framenet_request_data) if you are planning to use the data.
* `./data/multilabel` contains annotations converted to our own `multilabel` format, and which can be used as input to our models
* `./data/*_output` contain predictions from two other recent frame semantic parsing models, Open-SESAME (discussed in the paper; ran with standard settings as described in the GitHub repo), and LOME (too new to be covered in our paper; ran with standard settings using the scripts in [the official Docker](https://hub.docker.com/r/hltcoe/lome))
* `./lib/semeval` contains the official evaluation scripts from [SemEval'07 Task #19: Frame Semantic Structure Extraction](https://www.aclweb.org/anthology/S07-1018/). We used the version of the scripts available in the PyFN package, and added a modified version that takes into account partially correct spans (see our paper). We also provide a script (under `./scripts`) as a BASH wrapper around the original Perl scripts.
* `./output/predictions` contains the outputs (*.csv) from all models discussed in our paper, in sequence-label format; `./output/full_structure_predictions` contains the outputs from the same models but with (reconstructed) full frame structures in SemEval format. 
* `./output/external_evaluation` contains predictions from Open-SESAME and LOME converted to match with the files in `./output/predictions` so that they can be evaluated in the same way as our own models' predictions.

In addition to the files above, you will need GloVe embeddings for running some of the experiments; for this, download the file [glove.42B.300d](http://nlp.stanford.edu/data/glove.42B.300d.zip) from the official website, unzip, and add it under `./data/embeddings`.

We do *not* provide pretrained models (since there are too many of them and they are large files); re-training our models shouldn't take much time (less than an hour on a GPU), but if you're interested in our original model files, we can provide them on request.

### (2) Software environment
We recommend using [Conda](https://docs.conda.io/) for reproducing our exact software environment. You can use our YML file `./conda-env.yml` for this (see [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file) for instructions). Please get in touch with us if you have any issues installing the software. 

### (3) Running models
The main entry point for our system is `./fn_seq_labeling/labeler.py`, which can train a model from a configuration file, or evaluate an existing model. 

Under `./configs` you can find all of our configuration files (the configurations under `./configs/iwcs_*` are the ones reported on in the paper; the others were used for internal development purposes only but are provided for the sake of completeness).

#### Training
For training a model, simply do:

```shell
cd bert-for-framenet/
python -m fn_seq_labeling.labeler train configs/YOUR_CONFIG_NAME.json
```

#### Sequence label evaluation
Once the model is trained, the model parameters will be saved under `./output/YOUR_CONFIG_NAME.th`. Then, to run the models on an evaluation set, run: 

```shell
python -m fn_seq_labeling.labeler evaluate configs/YOUR_CONFIG_NAME.json 
```

Options:
* `--test` will make the system evaluate on FrameNet/test (if unspecified, it will evaluate on FrameNet/dev)
* `--eval_external_model MODEL` (where `MODEL` should be `sesame` or `lome`) will evaluate an external model (Open-SESAME or LOME), using the same task and formatting settings as given in your config file. In this case, `--external_predictions FILE` should also be specified, where `FILE` is a file in `./data/multilabel/*.txt`.

There are several other, more experimental options, but these are not covered by the paper; use them at your own risk! (Or get in touch with us if  you want to know more.)

The evaluation script will produce predictions for the specified development set and print scores. These scores correspond to the sequence-labeling scores discussed in the paper. 


#### SemEval evaluation
In order to do SemEval evaluation, do the following:

* Optionally, for combining predictions from separate models for frame and role prediction, run `python -m fn_seq_labeling.evaluation.merge_frames_roles FRAME_PREDICTIONS.csv ROLE_PREDICTIONS.csv` 
* Convert sequence label predictions to SemEval format: `python -m fn_seq_labeling.evaluation.reconstruct_frame_structures PREDICTIONS.csv SPLIT USE_STRUCTURE_INDICES`, where `SPLIT` is `dev` or `test`, and `USE_STRUCTURE_INDICES` specifies whether to infer frame structures or not: 
    * `yes` or `y`: the prediction file already includes indices indicating which frame and role labels belong to the same structure; this is the case for Open-SESAME/LOME predictions, _not_ for our own models;
    * `no` or `n`: automatically infer frame structures from bare sequence labels.
* Finally, run the SemEval scripts:
  
   ```bash
  cd ./lib/semeval
  bash ../../scripts/semeval_score.sh PREDICTIONS SPLIT
  ```
  where `PREDICTIONS` is a prediction file in `./output/full_structure_predictions` (N.B.: specify only the basename, not the path or the `.semeval` extension).
