#export SEMEVAL_FILE=merge_2020-04-15_only_frames_seqlabel_layer8.retrain_2020-06-22_dev387.semeval__2020-06-16_multitask_role_loss_x100_dev387_0.2
SEMEVAL_FILE=$1
EVAL_SET=$2

./score.semeval.pl \
  -c /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/output/semeval_scores \
  -l -n -e -v \
  /mnt/c/Users/Gosse/WorkSyncs/ClonedRepos/pyfn/data/fndata-1.7-with-dev/frames.xml \
  /mnt/c/Users/Gosse/WorkSyncs/ClonedRepos/pyfn/data/fndata-1.7-with-dev/frRelations.xml \
  /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/data/fn1.7/semeval/$EVAL_SET.gold.xml \
  /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/output/full_structure_predictions/$SEMEVAL_FILE.semeval \
  > /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/output/semeval_scores/$SEMEVAL_FILE.score

#./score.semeval.pl \
#  -c /mnt/c/Users/Gosse/WorkSyncs/bert-for-framenet/output/semeval_scores \
#  -l -n -v \
#  ../../../pyfn/data/fndata-1.7-with-dev/frames.xml \
#  ../../../pyfn/data/fndata-1.7-with-dev/frRelations.xml \
#  /mnt/c/Users/Gosse/WorkSyncs/bert-for-framenet/data/fn1.7/semeval/$EVAL_SET.gold.xml \
#  /mnt/c/Users/Gosse/WorkSyncs/bert-for-framenet/output/full_structure_predictions/$SEMEVAL_FILE.semeval \
#  > /mnt/c/Users/Gosse/WorkSyncs/bert-for-framenet/output/semeval_scores/$SEMEVAL_FILE.score.nonstrict

./score.semeval.partial_span.pl \
  -c /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/output/semeval_scores \
  -l -n -e -v \
  /mnt/c/Users/Gosse/WorkSyncs/ClonedRepos/pyfn/data/fndata-1.7-with-dev/frames.xml \
  /mnt/c/Users/Gosse/WorkSyncs/ClonedRepos/pyfn/data/fndata-1.7-with-dev/frRelations.xml \
  /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/data/fn1.7/semeval/$EVAL_SET.gold.xml \
  /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/output/full_structure_predictions/$SEMEVAL_FILE.semeval \
  > /mnt/c/Users/Gosse/WorkSyncs/Code/bert-for-framenet/output/semeval_scores/$SEMEVAL_FILE.score.partial_span
